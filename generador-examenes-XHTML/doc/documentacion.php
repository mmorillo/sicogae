<?php
/************************************************************************
* Software: ELSAM                                                       *
* Version:  0.9                                                         *
* Date:     2006-08-06                                                  *
* Author:   Miguel Morillo Iruela                                       *
* License:  GPL                                                         *
* This program is distributed under the terms and conditions of the GPL *
* See the LICENSE files for details                                     *
************************************************************************/
require_once "CONFIG/configuracion.php";
require_once "head.php";
?>

<?php

echo "<table border='0' cellpadding='6' cellspacing='6' style='border-collapse: separate' width='100%'>";
echo "<tr>";
echo "<td width='16%' valign='top'  style='border: 0px'>";

include ("menuinicio.php");

echo "</td>";
echo "<td width='82%' valign='top' style='border: hidden'>";

echo "<br><br>";

?>

<center>

<!--
<A href="<?php echo "$pathweb"?>/doc/manual-ELSAM.odt"><IMG SRC="<?php echo "$pathweb"?>/imagenes/consultar.png" NAME="doc" ALIGN=MIDDLE BORDER=0></A><BR><FONT face="sans-serif, Arial, Helvetica, Geneva" size=2><b>Documentación en Openoffice</b></FONT><BR><BR><BR>
-->
<A href="<?php echo "$pathweb"?>/doc/manual-ELSAM.pdf"><IMG SRC="<?php echo "$pathweb"?>/imagenes/acroread2.png" NAME="pdf" ALIGN=MIDDLE BORDER=0 ALT=""></A><BR><FONT face="sans-serif, Arial, Helvetica, Geneva" size=2><b>Documentación en PDF</b></FONT><BR>
</center>

<?php

/* Pie */
require_once "pie.php";

?>
