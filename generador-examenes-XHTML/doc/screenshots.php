<?php
/************************************************************************
* Software: ELSAM                                                       *
* Version:  0.9                                                         *
* Date:     2006-08-06                                                  *
* Author:   Miguel Morillo Iruela                                       *
* License:  GPL                                                         *
* This program is distributed under the terms and conditions of the GPL *
* See the LICENSE files for details                                     *
************************************************************************/

require_once "CONFIG/configuracion.php";
require_once "head.php";
?>


<!-- EFECTOS lightbox2 
<link rel="stylesheet" href="lightbox2/css/lightbox.css" type="text/css" media="screen"> -->
<script src="doc/lightbox2/js/prototype.js" type="text/javascript"></script>
<script src="doc/lightbox2/js/scriptaculous.js?load=effects" type="text/javascript"></script>
<script src="doc/lightbox2/js/lightbox.js" type="text/javascript"></script>


<?php
echo "<table border='0' cellpadding='6' cellspacing='6' style='border-collapse: separate' width='100%'>";
echo "<tr>";
echo "<td width='16%' valign='top' style='border: 0px'>";

include("menuinicio.php");

echo "</td>";
echo "<td width='82%' valign='top' style='border: hidden'>";
echo "<br><br>";

?>



<center>
<FONT size=4>Login de acceso</FONT>
<br><br>
<A href="<?php echo "$pathweb"?>/doc/1.png" rel="lightbox[roadtrip]"><IMG SRC="<?php echo "$pathweb"?>/doc/thumb-1.png" NAME="screenshots" ALIGN=MIDDLE BORDER=2 height=100 width=240 ALT=""></A><BR><BR></center>
<br><br>

<center>
<FONT size=4>Pagina de inicio</FONT>
<br><br>
<A href="<?php echo "$pathweb"?>/doc/2.png" rel="lightbox[roadtrip]"><IMG SRC="<?php echo "$pathweb"?>/doc/thumb-2.png" NAME="screenshots" ALIGN=MIDDLE BORDER=2 height=100 width=240 ALT=""></A><BR><BR></center>
<br><br>

<center>
<FONT size=4>Preferencias</FONT>
<br><br>
<A href="<?php echo "$pathweb"?>/doc/3.png" rel="lightbox[roadtrip]"><IMG SRC="<?php echo "$pathweb"?>/doc/thumb-3.png" NAME="screenshots" ALIGN=MIDDLE BORDER=2 height=100 width=240 ALT=""></A><BR><BR></center>
<br><br>

<center>
<FONT size=4>Cursos</FONT>
<br><br>
<A href="<?php echo "$pathweb"?>/doc/4.png" rel="lightbox[roadtrip]"><IMG SRC="<?php echo "$pathweb"?>/doc/thumb-4.png" NAME="screenshots" ALIGN=MIDDLE BORDER=2 height=100 width=240 ALT=""></A><BR><BR></center>
<br><br>

<center>
<FONT size=4>Asignaturas</FONT>
<br><br>
<A href="<?php echo "$pathweb"?>/doc/5.png" rel="lightbox[roadtrip]"><IMG SRC="<?php echo "$pathweb"?>/doc/thumb-5.png" NAME="screenshots" ALIGN=MIDDLE BORDER=2 height=100 width=240 ALT=""></A><BR><BR></center>
<br><br>

<center>
<FONT size=4>Examenes</FONT>
<br><br>
<A href="<?php echo "$pathweb"?>/doc/6.png" rel="lightbox[roadtrip]"><IMG SRC="<?php echo "$pathweb"?>/doc/thumb-6.png" NAME="screenshots" ALIGN=MIDDLE BORDER=2 height=100 width=240 ALT=""></A><BR><BR></center>
<br><br>

<center>
<FONT size=4>Preguntas</FONT>
<br><br>
<A href="<?php echo "$pathweb"?>/doc/7.png" rel="lightbox[roadtrip]"><IMG SRC="<?php echo "$pathweb"?>/doc/thumb-7.png" NAME="screenshots" ALIGN=MIDDLE BORDER=2 height=100 width=240 ALT=""></A><BR><BR></center>
<br><br>


<?php

/* Pie */
require_once "pie.php";

?>
