<?php
/************************************************************************
* Software: ELSAM                                                       *
* Version:  0.9                                                         *
* Date:     2006-08-06                                                  *
* Author:   Miguel Morillo Iruela                                       *
* License:  GPL                                                         *
* This program is distributed under the terms and conditions of the GPL *
* See the LICENSE files for details                                     *
************************************************************************/

/* Autenticacion */

require 'CHECK/check_login.php';

require 'CHECK/chequealoginadmin.php';

// login OK

?>


<BR><BR>
<center>
<FONT size=5><?php echo "$langprefborrausuario"?></FONT>
</center>
<HR>
<BR><BR>


<FORM ACTION='inicio.php?menu=preferencias&amp;enlace=borrausuariobox' method='post'>

<?php
require "funciones-preferencias.inc.php";
$miconexion = new DB_mysql ;
$miconexion->conectar($BaseDatosNombre, $BaseDatosServidor, $BaseDatosUsuario, $BaseDatosClave);
$miconexion->consulta("SELECT username FROM USUARIOS ORDER BY username");
$miconexion->verconsultaradio();
?>

<BR><BR>

<INPUT TYPE=SUBMIT VALUE="<?php echo "$langprefborrarusuarioboton"?>" class="button">

</FORM>

<?php
/* Pie */
require_once "pie.php";
?>
