<?php
/************************************************************************
* Software: ELSAM                                                       *
* Version:  0.9                                                         *
* Date:     2006-08-06                                                  *
* Author:   Miguel Morillo Iruela                                       *
* License:  GPL                                                         *
* This program is distributed under the terms and conditions of the GPL *
* See the LICENSE files for details                                     *
************************************************************************/

/* Autenticacion */

require 'CHECK/check_login.php';

require 'CHECK/chequealogin.php';

// login OK

?>


<BR>
<center>
<FONT size=5><?php echo "$langpreferencias"?></FONT>
<BR><HR><BR>
</center>

<center>
<FONT size=4><U><?php echo "$langprefusuario"?></U></FONT>
</center>
<BR><BR>

<table style="text-align: center; width: 100%;" border="0" cellpadding="1" cellspacing="1">
  <tbody>
	<tr>
	  <td><A href="inicio.php?menu=preferencias&amp;enlace=usuarios"><IMG SRC="imagenes/usuarios.png" NAME="usuarios" ALIGN=MIDDLE BORDER=0 ALT=""></A></td>
	  <td><A href="inicio.php?menu=preferencias&amp;enlace=registro"><IMG SRC="imagenes/registro.png" NAME="registro" ALIGN=MIDDLE BORDER=0 ALT=""></A></td>
	  <td><A href="inicio.php?menu=preferencias&amp;enlace=actualizausuario" ><IMG SRC="imagenes/actualizar-usuario.png" NAME="actualizar usuario" ALIGN=MIDDLE BORDER=0 ALT=""></A></td>
	  <td><A href="inicio.php?menu=preferencias&amp;enlace=borrausuario" ><IMG SRC="imagenes/borrar-usuario.png" NAME="borrar usuario" ALIGN=MIDDLE BORDER=0 ALT=""></A></td>
    </tr>
    <tr>
          <td><A href="inicio.php?menu=preferencias&amp;enlace=usuarios"><FONT face="sans-serif, Arial, Helvetica, Geneva" size=2><b><?php echo "$langprefdatosusuario"?></b></FONT></A></td>
	  <td><A href="inicio.php?menu=preferencias&amp;enlace=registro"><FONT face="sans-serif, Arial, Helvetica, Geneva" size=2><b><?php echo "$langprefnuevosusuarios"?></b></FONT></A></td>
	  <td><A href="inicio.php?menu=preferencias&amp;enlace=actualizausuario" ><FONT face="sans-serif, Arial, Helvetica, Geneva" size=2><b><?php echo "$langprefactusuarios"?></b></FONT></A></td>
	  <td><A href="inicio.php?menu=preferencias&amp;enlace=borrausuario" ><FONT face="sans-serif, Arial, Helvetica, Geneva" size=2><b><?php echo "$langprefborrarusuarios"?></b></FONT></A></td>
    </tr>
  </tbody>
</table>
	
<BR><BR>
<BR>
<BR><BR>

<center>
<FONT size=4><U><?php echo "$langpreflogo"?></U></FONT>
</center>
<BR><BR><BR>

<table style="text-align: center; width: 100%;" border="0" cellpadding="1" cellspacing="1">
  <tbody>
	<tr>
    		<td><A href="inicio.php?menu=preferencias&amp;enlace=subirlogo" ><IMG SRC="imagenes/subir-logo.png" NAME="subir logo" ALIGN=MIDDLE BORDER=0 ALT=""></A></td>  
		<td><A href="inicio.php?menu=preferencias&amp;enlace=verlogo" ><IMG SRC="imagenes/cambio-logo.png" NAME="ver logo" ALIGN=MIDDLE BORDER=0 ALT=""></A></td>
		<td><A href="inicio.php?menu=preferencias&amp;enlace=borrarlogo"><IMG SRC="imagenes/borrar-usuario.png" NAME="borrar logo" ALIGN=MIDDLE BORDER=0 ALT=""></A></td>
	</tr>
    <tr>
		<td><A href="inicio.php?menu=preferencias&amp;enlace=subirlogo" ><FONT face="sans-serif, Arial, Helvetica, Geneva" size=2><b><?php echo "$langprefnuevologo"?></b></FONT></A></td>
		<td><A href="inicio.php?menu=preferencias&amp;enlace=verlogo" ><FONT face="sans-serif, Arial, Helvetica, Geneva" size=2><b><?php echo "$langprefcambiarlogo"?></b></FONT></A></td>
		<td><A href="inicio.php?menu=preferencias&amp;enlace=borrarlogo"><FONT face="sans-serif, Arial, Helvetica, Geneva" size=2><b><?php echo "$langprefborrarlogo"?></b></FONT></A></td>
    </tr>
  </tbody>
</table>


<BR>
<BR>


<?php
/* Pie */
require_once "pie.php";
?>
