<?php
/************************************************************************
* Software: ELSAM                                                       *
* Version:  0.9                                                         *
* Date:     2006-08-06                                                  *
* Author:   Miguel Morillo Iruela                                       *
* License:  GPL                                                         *
* This program is distributed under the terms and conditions of the GPL *
* See the LICENSE files for details                                     *
************************************************************************/

/* Autenticacion */

require 'CHECK/check_login.php';

require 'CHECK/chequealoginadmin.php';

// login OK

?>


<?
require "funciones-preferencias.inc.php";

if ($_POST[nombre]=='' )
{
	echo("<IMG SRC='imagenes/peligro.png' NAME='Peligro' ALIGN=MIDDLE BORDER=0><br><br>");
	die("$langerrorborrausuario");
}

echo "<br>";
echo "<center>";
echo "<FONT size=5>$langprefborrausuario1 ".$_POST['nombre']."</FONT>";
echo "<br><br><br>";

$miconexion = new DB_mysql ;
$miconexion->conectar($BaseDatosNombre, $BaseDatosServidor, $BaseDatosUsuario, $BaseDatosClave);
$miconexion->consulta("SELECT username as '$langprefusuario', regdate AS '$langpreffecharegistro', email, website, location AS '$langprefdireccion', last_login AS '$langprefultimoacceso' FROM USUARIOS WHERE username='".$_POST['nombre']."'");
$miconexion->verconsultatabla();

echo "</center>";
echo "<br><br><br>";

echo "<FORM METHOD='post' ACTION='inicio.php?menu=preferencias&enlace=borrausuariobox2'>";
echo "<input type=hidden name='usuario' value='".$_POST['nombre']."'>";

echo "<center>";
echo "<input type='submit' class='button' value='$langprefborrarusuarioboton $_POST[nombre]'> ";
echo "</center>";

?>

<?php
/* Pie */
require_once "pie.php";
?>
