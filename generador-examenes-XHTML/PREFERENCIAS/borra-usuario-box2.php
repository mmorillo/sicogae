<?php
/************************************************************************
* Software: ELSAM                                                       *
* Version:  0.9                                                         *
* Date:     2006-08-06                                                  *
* Author:   Miguel Morillo Iruela                                       *
* License:  GPL                                                         *
* This program is distributed under the terms and conditions of the GPL *
* See the LICENSE files for details                                     *
************************************************************************/

/* Autenticacion */

require 'CHECK/check_login.php';

require 'CHECK/chequealoginadmin.php';
// login OK

?>

<?
require "funciones-preferencias.inc.php";

if ($_POST[usuario]=='' )
{
	echo("<IMG SRC='imagenes/peligro.png' NAME='Peligro' ALIGN=MIDDLE BORDER=0><br><br>");
	die("$langerrorborrausuario");
}


$miconexion = new DB_mysql ;
$miconexion->conectar($BaseDatosNombre, $BaseDatosServidor, $BaseDatosUsuario, $BaseDatosClave);
$miconexion->consulta("DELETE FROM USUARIOS WHERE username='".$_POST['usuario']."'");

if ($miconexion->Errno>0 )
{
	echo("<IMG SRC='imagenes/peligro.png' NAME='Peligro' ALIGN=MIDDLE BORDER=0><br><br>");
	echo("$langerrorborrarusuario2");echo("<br><br>");
	die("Error: $miconexion->Error");
}

echo "<br><br><br>";
echo "<FONT size=5>$langprefborrarusuariook ".$_POST['usuario']."</FONT>";

?>

<?php
/* Pie */
require_once "pie.php";
?>
