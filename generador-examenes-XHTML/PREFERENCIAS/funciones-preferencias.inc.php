<?php
/************************************************************************
* Software: ELSAM                                                       *
* Version:  0.9                                                         *
* Date:     2006-08-06                                                  *
* Author:   Miguel Morillo Iruela                                       *
* License:  GPL                                                         *
* This program is distributed under the terms and conditions of the GPL *
* See the LICENSE files for details                                     *
************************************************************************/

require 'SQL/db_connect.php';

class DB_mysql {
 

 /* variables de conexi�n */
 var $BaseDatos;
 var $Servidor;
 var $Usuario;
 var $Clave;

  
 /* identificador de conexi�n y consulta */
 var $Conexion_ID = 0;
 var $Consulta_ID = 0;

   
 /* n�mero de error y texto error */
 var $Errno = 0;
 var $Error = "";

      
 /* M�todo Constructor: Cada vez que creemos una variable
    de esta clase, se ejecutar� esta funci�n */
function DB_mysql($bd = "", $host = "localhost", $user = "nobody", $pass = "") {
   $this->BaseDatos = $bd;
   $this->Servidor = $host;
   $this->Usuario = $user;
   $this->Clave = $pass;
}

   
 /*Conexi�n a la base de datos*/
function conectar($bd, $host, $user, $pass){

	if ($bd != "") $this->BaseDatos = $bd;
	if ($host != "") $this->Servidor = $host;
	if ($user != "") $this->Usuario = $user;
	if ($pass != "") $this->Clave = $pass;
	 

// Conectamos al servidor
	$this->Conexion_ID = mysql_connect($this->Servidor, $this->Usuario, $this->Clave);
    if (!$this->Conexion_ID) {
       $this->Error = "Ha fallado la conexi�n.";
   	   return 0;
    }

//seleccionamos la base de datos
    if (!@mysql_select_db($this->BaseDatos, $this->Conexion_ID)) {
   	  $this->Error = "Imposible abrir ".$this->BaseDatos ;
	  return 0;
	}

 /* Si hemos tenido �xito conectando devuelve 
   el identificador de la conexi�n, sino devuelve 0 */

    return $this->Conexion_ID;
}

	    
/* Ejecuta un consulta */
function consulta($sql = ""){
	     if ($sql == "") {
		     $this->Error = "No ha especificado una consulta SQL";
		     return 0;
	     }

   		//ejecutamos la consulta
        $this->Consulta_ID = @mysql_query($sql, $this->Conexion_ID);
  

       if (!$this->Consulta_ID) {
	       $this->Errno = mysql_errno();
	       $this->Error = mysql_error();
       }
       /* Si hemos tenido �xito en la consulta devuelve 
      el identificador de la conexi�n, sino devuelve 0 */
	  
      return $this->Consulta_ID;
}

	        

/* Devuelve el n�mero de campos de una consulta */
function numcampos() {
	return mysql_num_fields($this->Consulta_ID);
}

		 
/* Devuelve el n�mero de registros de una consulta */
function numregistros(){
	return mysql_num_rows($this->Consulta_ID);
}

	  
/* Devuelve el nombre de un campo de una consulta */
function nombrecampo($numcampo) {
	return mysql_field_name($this->Consulta_ID, $numcampo);
}

		   
function verconsulta0() {
	$row=mysql_fetch_row($this->Consulta_ID);
	return $row[0];
}

function vertabla() {	
	echo "<font size=2>\n";
	echo "<table border=1 CELLPADDING=10 CELLSPACING=0>\n";
	// mostramos los nombres de los campos
	for ($i = 0; $i < $this->numcampos(); $i++){
	   echo "<td><b><center>".$this->nombrecampo($i)."</center></b></td>\n";
	}

	// mostrarmos los registros
	while ($row = mysql_fetch_row($this->Consulta_ID)) {
	   echo "<tr> \n";
	   for ($i = 0; $i < $this->numcampos(); $i++){
	   echo "<td>".$row[$i]."</td>\n";
	   }
    echo "</tr>\n";
	}
	echo "</table>";
}

function verconsultabox() {
	while ($row = mysql_fetch_row($this->Consulta_ID)) {
		echo "<Option>".$row[0]."</Option>\n";

	}
}


function verconsultaradio() {
	while ($row = mysql_fetch_row($this->Consulta_ID)) {
		echo "<input type='radio' name=nombre value='$row[0]'>".$row[0]."<BR>\n";
	}
}

function verconsultatabla() {
    
    echo "<table class='formato' border=0 CELLPADDING=10 CELLSPACING=0>\n";
    echo "<tr>\n";
    for ($i = 0; $i < $this->numcampos(); $i++){
	     echo "<td class='formatotd'><center><b><font size=2>".$this->nombrecampo($i)."</font></b></center></td>\n";
    }
    echo "</tr>\n";
    while ($row = mysql_fetch_row($this->Consulta_ID)) {
	     echo "<tr> \n";
	     for ($i = 0; $i < $this->numcampos(); $i++){
		      echo "<td  class='formatotd'><center><font size=2>".$row[$i]."</font></center></td>\n";
	     }
    echo "</tr>\n";
     }
    echo "</table>";
}



} //fin de la Clse DB_mysql
?>
