<?php
/************************************************************************
* Software: ELSAM                                                       *
* Version:  0.9                                                         *
* Date:     2006-08-06                                                  *
* Author:   Miguel Morillo Iruela                                       *
* License:  GPL                                                         *
* This program is distributed under the terms and conditions of the GPL *
* See the LICENSE files for details                                     *
************************************************************************/

/* Autenticacion */

require 'CHECK/check_login.php';

/* Debemos ser usuario administrador para poder borrar usuarios */
require 'CHECK/chequealoginadmin.php';

// login OK

?>

<br><br><br>

<?php

$nombrearchivo = $_GET["var"]; 
$pathlogos="logo/";


if (!unlink("logo/$nombrearchivo")) {
   echo "$langerrorlogo5 $nombrearchivo...\n";
}

die ("$langpreflogoborrar1 $nombrearchivo $langpreflogoborrar2");

	
?>
 
<?php
/* Pie */
require_once "pie.php";
?>
