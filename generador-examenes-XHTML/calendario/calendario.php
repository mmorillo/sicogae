<?php
/************************************************************************
* Software: ELSAM                                                       *
* Version:  0.9                                                         *
* Date:     2006-08-06                                                  *
* Author:   Miguel Morillo Iruela                                       *
* License:  GPL                                                         *
* This program is distributed under the terms and conditions of the GPL *
* See the LICENSE files for details                                     *
************************************************************************/

/* Autenticacion */

require 'CHECK/check_login.php';

require 'CHECK/chequealogin.php';

// login OK

include "calendario/php_calendar.php";

function leading_zero($number, $num_digits) {		
	return str_repeat("0",($num_digits + -1 - floor(log10($number)))) . $number; 
}

//Recogemos fechas

$time = time();
$today = date('j',$time);
$mes = date('m', $time);
$ano= date('Y', $time);
$anoanterior= $ano-1;
$anosiguiente= $ano+1;
$mes2=date( 'm', strtotime( date( "Y-m" ) . '-01 next month' ) );
$mescalendario=date('n', $time);
$mescalendario2=$mescalendario+1;

$days = array();





// AÑO PASADO

$brake=0;

echo "<center>";
echo "<FONT size=5>$langmenucalendario $anoanterior</FONT>";
echo "</center>";

echo "<table align='center' border='0' cellspacing='0' cellpadding='15'>";
echo "<tr>";

for ($i=9;$i<=12;$i++)
{

$mescorregido= leading_zero($i, 2);
$datosfechaexamenes = array();
$days = array();

$db = mysql_connect("$BaseDatosServidor", "$BaseDatosUsuario","$BaseDatosClave") or die("Imposible conectar a la base de datos");
mysql_select_db("$BaseDatosNombre", $db) or die("Imposible consultar la base de datos");

$fechaexamenes = mysql_query("select FECHA,IDEXAMEN from EXAMEN where FECHA like ('%/$mescorregido/$anoanterior')",$db) or die("Sentencia SQL incorrecta");

while( $row = mysql_fetch_array($fechaexamenes) ){
   $doscifras=substr("$row[FECHA]", 0, 2);
   $cero=substr("$doscifras", 0, 1);
   $unacifra=substr("$doscifras", 1, 1);
   if ($cero==0){
	$cifra=$unacifra;
	}
   else { $cifra=$doscifras; }
   $datosfechaexamenes[0] = $cifra; // Valores de cada fila
   $datosfechaexamenes[1] = $row[IDEXAMEN];
   $days [$datosfechaexamenes[0]]= array("iniciopdf.php?menu=examenes&enlace=visualizarexamenpdf&var=$datosfechaexamenes[1]","linked-day",'<span style="color: blue; font-weight: bold; text-decoration: blink;">'.$datosfechaexamenes[0].'</span>');
}


$calendario=generate_calendar($anoanterior, $i, $days);

$brake=$brake+1;

if ( $brake == 5 ) {
        echo "</tr>\n";
        echo "<tr>\n";
        $brake=1;
}

echo "<td>";
echo "$calendario";
echo "</td>";

}

//echo "</tr>";
echo "</table>";




echo "<br><br>";



// AÑO ACTUAL

$brake=0;

echo "<center>";
echo "<FONT size=5>$langmenucalendario $ano</FONT>";
echo "</center>";

echo "<table align='center' border='0' cellspacing='0' cellpadding='15'>\n";
echo "<tr>\n";

for ($i=1;$i<=12;$i++)
{

$mescorregido= leading_zero($i, 2);
$datosfechaexamenes = array();
$days = array();

$db = mysql_connect("$BaseDatosServidor", "$BaseDatosUsuario","$BaseDatosClave") or die("Imposible conectar a la base de datos");
mysql_select_db("$BaseDatosNombre", $db) or die("Imposible consultar la base de datos");

$fechaexamenes = mysql_query("select FECHA,IDEXAMEN from EXAMEN where FECHA like ('%/$mescorregido/$ano')",$db) or die("Sentencia SQL incorrecta");

while( $row = mysql_fetch_array($fechaexamenes) ){
   $doscifras=substr("$row[FECHA]", 0, 2);
   $cero=substr("$doscifras", 0, 1);
   $unacifra=substr("$doscifras", 1, 1);
   if ($cero==0){
	$cifra=$unacifra;
	}
   else { $cifra=$doscifras; }
   $datosfechaexamenes[0] = $cifra; // Valores de cada fila
   $datosfechaexamenes[1] = $row[IDEXAMEN];
   $days [$datosfechaexamenes[0]]= array("iniciopdf.php?menu=examenes&enlace=visualizarexamenpdf&var=$datosfechaexamenes[1]","linked-day",'<span style="color: blue; font-weight: bold; text-decoration: blink;">'.$datosfechaexamenes[0].'</span>');
}


$calendario=generate_calendar($ano, $i, $days);

$brake=$brake+1;

if ( $brake == 5 ) {
        echo "</tr>\n";
        echo "<tr>\n";
        $brake=1;
}

echo "<td>";
echo "$calendario";
echo "</td>\n";


}

echo "</tr>\n";
echo "</table>\n";




echo "<br><br>";



// AÑO SIGUIENTE

$brake=0;

echo "<center>";
echo "<FONT size=5>$langmenucalendario $anosiguiente</FONT>";
echo "</center>";

echo "<table align='center' border='0' cellspacing='0' cellpadding='15'>";
echo "<tr>";

for ($i=1;$i<=4;$i++)
{

$mescorregido= leading_zero($i, 2);
$datosfechaexamenes = array();
$days = array();

$db = mysql_connect("$BaseDatosServidor", "$BaseDatosUsuario","$BaseDatosClave") or die("Imposible conectar a la base de datos");
mysql_select_db("$BaseDatosNombre", $db) or die("Imposible consultar la base de datos");

$fechaexamenes = mysql_query("select FECHA,IDEXAMEN from EXAMEN where FECHA like ('%/$mescorregido/$anosiguiente')",$db) or die("Sentencia SQL incorrecta");

while( $row = mysql_fetch_array($fechaexamenes) ){
   $doscifras=substr("$row[FECHA]", 0, 2);
   $cero=substr("$doscifras", 0, 1);
   $unacifra=substr("$doscifras", 1, 1);
   if ($cero==0){
	$cifra=$unacifra;
	}
   else { $cifra=$doscifras; }
   $datosfechaexamenes[0] = $cifra; // Valores de cada fila
   $datosfechaexamenes[1] = $row[IDEXAMEN];
   $days [$datosfechaexamenes[0]]= array("iniciopdf.php?menu=examenes&enlace=visualizarexamenpdf&var=$datosfechaexamenes[1]","linked-day",'<span style="color: blue; font-weight: bold; text-decoration: blink;">'.$datosfechaexamenes[0].'</span>');
}


$calendario=generate_calendar($anosiguiente, $i, $days);

$brake=$brake+1;

if ( $brake == 5 ) {
        echo "</tr>\n";
        echo "<tr>\n";
        $brake=1;
}

echo "<td>";
echo "$calendario";
echo "</td>";


}

//echo "</tr>";
echo "</table>";


?>
