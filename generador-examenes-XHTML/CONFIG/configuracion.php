<?php
/************************************************************************
* Software: ELSAM                                                       *
* Version:  0.92                                                         *
* Date:     2006-08-06                                                  *
* Author:   Miguel Morillo Iruela                                       *
* License:  GPL                                                         *
* This program is distributed under the terms and conditions of the GPL *
* See the LICENSE files for details                                     *
************************************************************************/

$BaseDatosServidor	= "localhost";
$BaseDatosNombre	= "examenes";
$BaseDatosUsuario	= "USUARIO";
$BaseDatosClave		= "PASS";

$tituloIndex="Generador examenes ELSAM - Sistema de control, gestión, creación y almacenaje de examenes";
$tituloContacto="";
$PieDePaginaindex=".: Copyleft @desde 2006 Miguel Morillo :."; 
$PieDePaginaContacto="elsam.sicogae@gmail.com"; 
$pathweb="http://localhost/generador-examenes";
$path="/var/www/generador-examenes";


$lang="spanish.php";
require "$path/LANGUAGE/".$lang;


?>
