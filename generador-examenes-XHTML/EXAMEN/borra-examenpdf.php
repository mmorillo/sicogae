<?php
/************************************************************************
* Software: ELSAM                                                       *
* Version:  0.9                                                         *
* Date:     2006-08-06                                                  *
* Author:   Miguel Morillo Iruela                                       *
* License:  GPL                                                         *
* This program is distributed under the terms and conditions of the GPL *
* See the LICENSE files for details                                     *
************************************************************************/

/* Autenticacion */

require 'CHECK/check_login.php';

require 'CHECK/chequealogin.php';
// login OK
?>

<BR><BR>

<center>
<FONT size=5><?php echo "$langexamenborrar1"?></FONT>
</center>
<HR><BR><BR>


<FORM ACTION='inicio.php?menu=examenes&amp;enlace=borraexamenpdf2' method='post'>
<?php
require ("funciones-examenes.inc.php");
$miconexion = new DB_mysql ;
$miconexion->conectar($BaseDatosNombre, $BaseDatosServidor, $BaseDatosUsuario, $BaseDatosClave);
$miconexion->consulta("SELECT NOMBRE FROM PDF ORDER BY NOMBRE");
$miconexion->verconsultaradio();
?>
<BR><BR>
<INPUT TYPE=SUBMIT class='button' VALUE="<?php echo "$langexamenborrarboton"?>">
</FORM>

<?php
/* Pie */
require_once "pie.php";
?>
