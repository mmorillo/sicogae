<?php
/************************************************************************
* Software: ELSAM                                                       *
* Version:  0.9                                                         *
* Date:     2006-08-06                                                  *
* Author:   Miguel Morillo Iruela                                       *
* License:  GPL                                                         *
* This program is distributed under the terms and conditions of the GPL *
* See the LICENSE files for details                                     *
************************************************************************/

/* Autenticacion */

require 'CHECK/check_login.php';

require 'CHECK/chequealogin.php';

// login OK

?>

<BR><BR><BR>


<?php
require ("funciones-examenes.inc.php");

if ($_POST[nombre]=='' )
{
	echo("<IMG SRC='imagenes/peligro.png' NAME='Peligro' ALIGN=MIDDLE BORDER=0 alt=''><br><br>");
	die("$langerrorexamen3");
}


$miconexion = new DB_mysql ;
$miconexion->conectar($BaseDatosNombre, $BaseDatosServidor, $BaseDatosUsuario, $BaseDatosClave);
$miconexion->consulta("SELECT IDASIGNATURA FROM ASIGNATURA WHERE NOMBRE='".$_POST[asignatura]."'");
if ($miconexion->Errno>0 )
{
	echo("<IMG SRC='imagenes/peligro.png' NAME='Peligro' ALIGN=MIDDLE BORDER=0 alt=''><br><br>");
	echo("$langerrorexamen1");echo("<br><br>");
	die("Error: $miconexion->Error");
}

$idasignatura=$miconexion->verconsulta0();


$miconexion = new DB_mysql ;
$miconexion->conectar($BaseDatosNombre, $BaseDatosServidor, $BaseDatosUsuario, $BaseDatosClave);
$miconexion->consulta("UPDATE EXAMEN SET NOMBRE='$_POST[nombre]', COMENTARIO='$_POST[comentario]' , FECHA='$_POST[fecha]' , MODELO='$_POST[modelo]' , EVALUACION='$_POST[evaluacion]', IDASIGNATURA='$idasignatura', CODIGOBARRAS='$_POST[codigobarras]' WHERE IDEXAMEN='$_POST[idexamen]'");

if ($miconexion->Errno>0 )
{
	echo("<IMG SRC='imagenes/peligro.png' NAME='Peligro' ALIGN=MIDDLE BORDER=0 alt=''><br><br>");
	echo("$langerrorexamen4");echo("<br><br>");
	die("Error: $miconexion->Error");
}


echo "<br><br>";
echo "<center>";
echo "<FONT size=5>$langexamenmodificaok</FONT>";
?>

</CENTER>

<?php
/* Pie */
require_once "pie.php";
?>
