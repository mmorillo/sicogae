<?php
/************************************************************************
* Software: ELSAM                                                       *
* Version:  0.9                                                         *
* Date:     2006-08-06                                                  *
* Author:   Miguel Morillo Iruela                                       *
* License:  GPL                                                         *
* This program is distributed under the terms and conditions of the GPL *
* See the LICENSE files for details                                     *
************************************************************************/

/* Autenticacion */

require 'CHECK/check_login.php';

require 'CHECK/chequealogin.php';

// login OK

?>

<div id="cabeceraonline" ALIGN = right>
	<BR><BR><BR><BR><BR><BR><BR>
	<h2><?php echo "$langexamenonlinetitulo"?></h2> 
</div>

<BR><BR><BR><BR><BR><BR><BR><BR><BR><BR><BR><BR>


<CENTER>
<FONT size=4><?php echo "$langexamenonlineexamenes"?> <br> <?php echo "$langexamenonlineexamenes2"?></FONT>
<BR><BR>
</CENTER>

<?php
require ("funciones-examenes.inc.php");

$idasignatura = $_GET["var"];

$miconexion = new DB_mysql ;
$miconexion->conectar($BaseDatosNombre, $BaseDatosServidor, $BaseDatosUsuario, $BaseDatosClave);
$miconexion->consulta("SELECT distinct E.IDEXAMEN FROM EXAMEN E, ASIGNATURA A, PREGUNTA P WHERE P.IDEXAMEN=E.IDEXAMEN AND E.IDASIGNATURA='$idasignatura' AND E.IDASIGNATURA=A.IDASIGNATURA ORDER BY E.NOMBRE");
$miconexion->vertablalinkexamenonline();

?>


<?php
/* Pie */
require_once "pie.php";
?>
