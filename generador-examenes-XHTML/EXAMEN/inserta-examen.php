<?php
/************************************************************************
* Software: ELSAM                                                       *
* Version:  0.9                                                         *
* Date:     2006-08-06                                                  *
* Author:   Miguel Morillo Iruela                                       *
* License:  GPL                                                         *
* This program is distributed under the terms and conditions of the GPL *
* See the LICENSE files for details                                     *
************************************************************************/

/* Autenticacion */

require 'CHECK/check_login.php';

require 'CHECK/chequealogin.php';

// login OK

?>


<p>
<BR><BR>
</p>

<center>
<FONT size=5><?php echo "$langexameninserta1"?></FONT>
<HR><BR><BR>
</center>

<FORM METHOD='post' ACTION="inicio.php?menu=examenes&amp;enlace=formularioinsertaexamen">

<b><?php echo "$langexamenasignatura2"?></b> 

<select name="asignatura">
<?php
require ("funciones-examenes.inc.php");
$miconexion = new DB_mysql ;
$miconexion->conectar($BaseDatosNombre, $BaseDatosServidor, $BaseDatosUsuario, $BaseDatosClave);
$miconexion->consulta("SELECT NOMBRE FROM ASIGNATURA ORDER BY NOMBRE");
$miconexion->verconsultabox();

?>
</select> 

<br><br>

<b><?php echo "$langexamennombre"?>:</b> <input type='text' name='examen' value='' size=50><br><br>
<b><?php echo "$langexamencomentario"?>:</b> <input type='text' name='comentario' value='' size=50><br><br>
<b><?php echo "$langexamenfecha"?> (dd/mm/yyyy):</b> <input type='text' name='date' value='<? echo(date("d/m/Y")); ?>' size=15><br><br>
<b><?php echo "$langexamenevaluacion"?>:</b> <input type='text' name='evaluacion' value='' size=15><br><br>
<b><?php echo "$langexamenmodelo"?>:</b> <input type='text' name='modelo' value='' size=15><br><br>
<b><?php echo "$langexamencodigobarras"?>:</b> <input type='text' name='codigobarras' value='' size=10>
<b><?php echo "$langexamencodigobarras2"?></b><br><br>
<BR><BR>
<center><input type='submit' class='button' value="<?php echo "$langexameninsertaboton"?>"></center>

</FORM>

<?php
/* Pie */
require_once "pie.php";
?>
