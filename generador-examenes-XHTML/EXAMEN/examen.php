<?php
/************************************************************************
* Software: ELSAM                                                       *
* Version:  0.92                                                         *
* Date:     2006-08-06                                                  *
* Author:   Miguel Morillo Iruela                                       *
* License:  GPL                                                         *
* This program is distributed under the terms and conditions of the GPL *
* See the LICENSE files for details                                     *
************************************************************************/

/* Autenticacion */

require 'CHECK/check_login.php';

require 'CHECK/chequealogin.php';

// login OK

?>

<BR><BR><BR><BR>

<table style="text-align: center; width: 100%;" border="0" cellpadding="0" cellspacing="0">
  <tbody>
    <tr>
      <td align="center" valign="middle"><a href="inicio.php?menu=examenes&amp;enlace=verexamenasignatura"><IMG SRC="imagenes/consultar.png" NAME="Consultar" ALIGN=MIDDLE BORDER=0 alt=""></a></td>
      <td align="center" valign="middle"><a href="inicio.php?menu=examenes&amp;enlace=actualizarexamenasignatura"><IMG SRC="imagenes/actualizar.png" NAME="Actualizar" ALIGN=MIDDLE BORDER=0 alt=""></a></td>
      <td align="center" valign="middle"><a href="inicio.php?menu=examenes&amp;enlace=insertaexamen"><IMG SRC="imagenes/insertar.png" NAME="Insertar" ALIGN=MIDDLE BORDER=0 alt=""></a></td>
      <td align="center" valign="middle"><a href="inicio.php?menu=examenes&amp;enlace=borraexamenasignatura"><IMG SRC="imagenes/borrar.png" NAME="Borrar" ALIGN=MIDDLE BORDER=0 alt=""></a></td>
    </tr>
    <tr>
      <td align="center" valign="middle"><a href="inicio.php?menu=examenes&amp;enlace=verexamenasignatura"><FONT face="sans-serif, Arial, Helvetica, Geneva" size=3><b><?php echo "$langexamenconsulta"?></b></FONT></a></td>
      <td align="center" valign="middle"><a href="inicio.php?menu=examenes&amp;enlace=actualizarexamenasignatura"><FONT face="sans-serif, Arial, Helvetica, Geneva" size=3><b><?php echo "$langexamenactualiza"?></b></FONT></a></td>
      <td align="center" valign="middle"><a href="inicio.php?menu=examenes&amp;enlace=insertaexamen"><FONT face="sans-serif, Arial, Helvetica, Geneva" size=3><b><?php echo "$langexameninserta"?></b></FONT></a></td>
      <td align="center" valign="middle"><a href="inicio.php?menu=examenes&amp;enlace=borraexamenasignatura"><FONT face="sans-serif, Arial, Helvetica, Geneva" size=3><b><?php echo "$langexamenborra"?></b></FONT></a></td>
    </tr>
  </tbody>
</table>

<BR><BR>

<table style="text-align: center; width: 100%;" border="0" cellpadding="0" cellspacing="0">
  <tbody>
    <tr>
      <td align="center" valign="middle"><a href="inicio.php?menu=examenes&amp;enlace=verexamenpdf"><IMG SRC="imagenes/copias-pdf-bd.png" NAME="Consultar pdf" ALIGN=MIDDLE BORDER=0 alt=""></a></td>
      <td align="center" valign="middle"><a href="inicio.php?menu=examenes&amp;enlace=borraexamenpdf"><IMG SRC="imagenes/papelera.png" NAME="Borrar pdf" ALIGN=MIDDLE BORDER=0 alt=""></a></td>
      <td align="center" valign="middle"><a href="inicio.php?menu=examenes&amp;enlace=respuestasexamenpdfasignatura"><IMG SRC="imagenes/hojarespuestas.png" NAME="Respuesta pdf" ALIGN=MIDDLE BORDER=0 alt=""></a></td>
      <td align="center" valign="middle"><a href="inicio.php?menu=examenes&amp;enlace=ordenpreguntasexamenasignatura"><IMG SRC="imagenes/orden.png" NAME="Orden preguntas" ALIGN=MIDDLE BORDER=0 alt=""></a></td>
    </tr>
    <tr>
      <td align="center" valign="middle"><a href="inicio.php?menu=examenes&amp;enlace=verexamenpdf"> <FONT face="sans-serif, Arial, Helvetica, Geneva" size=3><b><?php echo "$langexamenconsultabd"?></b></FONT></a></td>
      <td align="center" valign="middle"><a href="inicio.php?menu=examenes&amp;enlace=borraexamenpdf"><FONT face="sans-serif, Arial, Helvetica, Geneva" size=3><b><?php echo "$langexamenborrabd"?></b></FONT></a></td>
      <td align="center" valign="middle"><a href="inicio.php?menu=examenes&amp;enlace=respuestasexamenpdfasignatura"><FONT face="sans-serif, Arial, Helvetica, Geneva" size=3><b><?php echo "$langexamenrespuestas"?></b></FONT></a></td>
      <td align="center" valign="middle"><a href="inicio.php?menu=examenes&amp;enlace=ordenpreguntasexamenasignatura"><FONT face="sans-serif, Arial, Helvetica, Geneva" size=3><b><?php echo "$langexamenorden1"?></b></FONT></a></td>
    </tr>
  </tbody>
</table>

<BR><BR>

<table style="text-align: center; width: 100%;" border="0" cellpadding="0" cellspacing="0">
  <tbody>
    <tr>
      <td align="center" valign="middle"><a href="inicio.php?menu=examenes&amp;enlace=examenrapido"><IMG SRC="imagenes/examenrapido.png" NAME="Examen rapido" ALIGN=MIDDLE BORDER=0 alt=""></a></td>
      <td align="center" valign="middle"><a href="inicio.php?menu=examenes&amp;enlace=examenonline"><IMG SRC="imagenes/examenonline.png" NAME="Examen rapido" ALIGN=MIDDLE BORDER=0 alt=""></a></td>
    </tr>
    <tr>
      <td align="center" valign="middle"><a href="inicio.php?menu=examenes&amp;enlace=examenrapido"> <FONT face="sans-serif, Arial, Helvetica, Geneva" size=3><b><?php echo "$langexamenrapidomenu"?></b></FONT></a></td>
      <td align="center" valign="middle"><a href="inicio.php?menu=examenes&amp;enlace=examenonline"> <FONT face="sans-serif, Arial, Helvetica, Geneva" size=3><b><?php echo "$langexamenonlinemenu"?></b></FONT></a></td>
    </tr>
  </tbody>
</table>


<?php
/* Pie */
require_once "pie.php";
?>
