<?php
/************************************************************************
* Software: ELSAM                                                       *
* Version:  0.9                                                         *
* Date:     2006-08-06                                                  *
* Author:   Miguel Morillo Iruela                                       *
* License:  GPL                                                         *
* This program is distributed under the terms and conditions of the GPL *
* See the LICENSE files for details                                     *
************************************************************************/

/* Autenticacion */

require 'CHECK/check_login.php';

require 'CHECK/chequealogin.php';

// login OK

?>

<BR><BR>

<CENTER>
<FONT size=5><?php echo "$langexamenorden2"?></FONT>
<HR><BR>
</CENTER>

<?php
require ("funciones-examenes.inc.php");
$idexamen = $_GET["var"]; 

$miconexion = new DB_mysql ;
$miconexion->conectar($BaseDatosNombre, $BaseDatosServidor, $BaseDatosUsuario, $BaseDatosClave);
$miconexion->consulta("SELECT IDPREGUNTA FROM PREGUNTA  WHERE IDEXAMEN='$idexamen' ORDER BY IDORDER, IDPREGUNTA");
$miconexion->ordenpreguntasexamen();

?>


<?php
/* Pie */
require_once "pie.php";
?>
