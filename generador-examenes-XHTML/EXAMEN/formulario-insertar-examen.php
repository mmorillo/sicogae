<?php
/************************************************************************
* Software: ELSAM                                                       *
* Version:  0.9                                                         *
* Date:     2006-08-06                                                  *
* Author:   Miguel Morillo Iruela                                       *
* License:  GPL                                                         *
* This program is distributed under the terms and conditions of the GPL *
* See the LICENSE files for details                                     *
************************************************************************/

/* Autenticacion */

require 'CHECK/check_login.php';

require 'CHECK/chequealogin.php';

// login OK

?>


<BR><BR><BR><BR>

<?php
require ("funciones-examenes.inc.php");

if ($_POST[asignatura]=='' )
{
   echo("<IMG SRC='imagenes/peligro.png' NAME='Peligro' ALIGN=MIDDLE BORDER=0 alt=''><br><br>");
   die("$langerrorexamen5");
}

$miconexion = new DB_mysql ;
$miconexion->conectar($BaseDatosNombre, $BaseDatosServidor, $BaseDatosUsuario, $BaseDatosClave);
$miconexion->consulta("SELECT IDASIGNATURA FROM ASIGNATURA WHERE NOMBRE='".$_POST[asignatura]."'");
if ($miconexion->Errno>0 )
{
	echo("<IMG SRC='imagenes/peligro.png' NAME='Peligro' ALIGN=MIDDLE BORDER=0 alt=''><br><br>");
	echo("$langerrorexamen6");echo("<br><br>");
	die("Error: $miconexion->Error");
}

$idasignatura=$miconexion->verconsulta0();


if ($_POST[examen]=='' )
{
   echo("<IMG SRC='imagenes/peligro.png' NAME='Peligro' ALIGN=MIDDLE BORDER=0 alt=''><br><br>");
   die("$langerrorexamen7");
}


echo "<br><br>";

$miconexion = new DB_mysql;
$miconexion->conectar($BaseDatosNombre, $BaseDatosServidor, $BaseDatosUsuario, $BaseDatosClave);

$miconexion->consulta("INSERT INTO EXAMEN (IDASIGNATURA, NOMBRE, COMENTARIO, FECHA, EVALUACION, MODELO, CODIGOBARRAS) VALUES ('$idasignatura', '$_POST[examen]', '$_POST[comentario]', '$_POST[date]', '$_POST[evaluacion]', '$_POST[modelo]', '$_POST[codigobarras]')");

if ($miconexion->Errno>0 )
{
	echo("$langerrorexamen8");echo("<br><br>");
	die("Error: $miconexion->Error");
}

?>

<FONT size=5><?php echo "$langexameninsertaok2"?></FONT>


<?php
/* Pie */
require_once "pie.php";
/************************************************************************
* Software: ELSAM                                                       *
* Version:  0.9                                                         *
* Date:     2006-08-06                                                  *
* Author:   Miguel Morillo Iruela                                       *
* License:  GPL                                                         *
* This program is distributed under the terms and conditions of the GPL *
* See the LICENSE files for details                                     *
************************************************************************/
?>
