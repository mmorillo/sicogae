<?php
/************************************************************************
* Software: ELSAM                                                       *
* Version:  0.9                                                         *
* Date:     2006-08-06                                                  *
* Author:   Miguel Morillo Iruela                                       *
* License:  GPL                                                         *
* This program is distributed under the terms and conditions of the GPL *
* See the LICENSE files for details                                     *
************************************************************************/

/* Autenticacion */

require 'CHECK/check_login.php';

require 'CHECK/chequealogin.php';

// login OK

?>

<BR><BR>

<CENTER>
<FONT size=5><?php echo "$langexamenorden2"?></FONT>
<HR><BR>
</CENTER>

<?php
require ("funciones-examenes.inc.php");

$miconexion = new DB_mysql ;
$miconexion->conectar($BaseDatosNombre, $BaseDatosServidor, $BaseDatosUsuario, $BaseDatosClave);
$miconexion->consulta("UPDATE PREGUNTA SET IDORDER='$_POST[orden]' WHERE IDPREGUNTA='$_POST[idpregunta]'");

if ($miconexion->Errno>0 )
{
	echo("<IMG SRC='imagenes/peligro.png' NAME='Peligro' ALIGN=MIDDLE BORDER=0 alt=''><br><br>");
	echo("$langerrorpregunta5");echo("<br><br>");
	die("Error: $miconexion->Error");
}


echo "<br><br>";
echo "<center>";
echo "<FONT size=5>$langexamenmodificaok</FONT>";
?>


<?php
/* Pie */
require_once "pie.php";
?>
