<?php
/************************************************************************
* Software: ELSAM                                                       *
* Version:  0.9                                                         *
* Date:     2006-08-06                                                  *
* Author:   Miguel Morillo Iruela                                       *
* License:  GPL                                                         *
* This program is distributed under the terms and conditions of the GPL *
* See the LICENSE files for details                                     *
************************************************************************/

require 'SQL/db_connect.php';
include_once("fckeditor/fckeditor.php") ;

class DB_mysql {
 
 /* variables de conexi�n */
 var $BaseDatos;
 var $Servidor;
 var $Usuario;
 var $Clave;

  
 /* identificador de conexi�n y consulta */
 var $Conexion_ID = 0;
 var $Consulta_ID = 0;

   
 /* n�mero de error y texto error */
 var $Errno = 0;
 var $Error = "";

      
 /* M�todo Constructor: Cada vez que creemos una variable
    de esta clase, se ejecutar� esta funci�n */
function DB_mysql($bd = "", $host = "localhost", $user = "nobody", $pass = "") {
   $this->BaseDatos = $bd;
   $this->Servidor = $host;
   $this->Usuario = $user;
   $this->Clave = $pass;
}

   
 /*Conexi�n a la base de datos*/
function conectar($bd, $host, $user, $pass){

	if ($bd != "") $this->BaseDatos = $bd;
	if ($host != "") $this->Servidor = $host;
	if ($user != "") $this->Usuario = $user;
	if ($pass != "") $this->Clave = $pass;
	 

// Conectamos al servidor
	$this->Conexion_ID = mysql_connect($this->Servidor, $this->Usuario, $this->Clave);
    if (!$this->Conexion_ID) {
       $this->Error = "Ha fallado la conexi�n.";
   	   return 0;
    }

//seleccionamos la base de datos
    if (!@mysql_select_db($this->BaseDatos, $this->Conexion_ID)) {
   	  $this->Error = "Imposible abrir ".$this->BaseDatos ;
	  return 0;
	}

 /* Si hemos tenido �xito conectando devuelve 
   el identificador de la conexi�n, sino devuelve 0 */

    return $this->Conexion_ID;
}

	    
/* Ejecuta un consulta */
function consulta($sql = ""){
	     if ($sql == "") {
		     $this->Error = "No ha especificado una consulta SQL";
		     return 0;
	     }

   		//ejecutamos la consulta
        $this->Consulta_ID = @mysql_query($sql, $this->Conexion_ID);
  

       if (!$this->Consulta_ID) {
	       $this->Errno = mysql_errno();
	       $this->Error = mysql_error();
       }
       /* Si hemos tenido �xito en la consulta devuelve 
      el identificador de la conexi�n, sino devuelve 0 */
	  
      return $this->Consulta_ID;
}

	        

/* Devuelve el n�mero de campos de una consulta */
function numcampos() {
	return mysql_num_fields($this->Consulta_ID);
}

		 
/* Devuelve el n�mero de registros de una consulta */
function numregistros(){
	return mysql_num_rows($this->Consulta_ID);
}

	  
/* Devuelve el nombre de un campo de una consulta */
function nombrecampo($numcampo) {
	return mysql_field_name($this->Consulta_ID, $numcampo);
}

		   
function verconsulta0() {
	$row=mysql_fetch_row($this->Consulta_ID);
	return $row[0];
}

function vertabla() {	
	echo "<font size=2>\n";
	echo "<table border=1 CELLPADDING=10 CELLSPACING=0>\n";
	// mostramos los nombres de los campos
	for ($i = 0; $i < $this->numcampos(); $i++){
	   echo "<td><b><center>".$this->nombrecampo($i)."</center></b></td>\n";
	}

	// mostrarmos los registros
	while ($row = mysql_fetch_row($this->Consulta_ID)) {
	   echo "<tr> \n";
	   for ($i = 0; $i < $this->numcampos(); $i++){
	   echo "<td>".$row[$i]."</td>\n";
	   }
    echo "</tr>\n";
	}
	echo "</table>";
}


function vertablalinkpreguntainicio() {
	echo "<ol>\n";

	while ($row = mysql_fetch_row($this->Consulta_ID)) {
	   for ($i = 0; $i < $this->numcampos(); $i++){
	  	 $consultanombrepregunta="SELECT TITULO FROM PREGUNTA WHERE IDPREGUNTA='$row[$i]'";
		 $res=mysql_query($consultanombrepregunta) or die('Error: '.mysql_error()."<BR>Query: $consultanombrepregunta");
		 $nombrepregunta = mysql_fetch_row($res);
	     echo "<li><a href='inicio.php?menu=preguntas&amp;enlace=visualizarpregunta&amp;var=".nl2br($row[$i])."'>".nl2br($nombrepregunta[0])."</a>&nbsp; &nbsp; &nbsp; &nbsp;<a href='iniciopdf.php?menu=preguntas&amp;enlace=visualizarpreguntapdf&amp;var=".$row[$i]."'><IMG SRC='imagenes/pdf.png' NAME='Generar Examen PDF' ALIGN=MIDDLE BORDER=0 alt=''></a>\n";
	     echo "<br><br></li>";
	   }
	}
	echo "</ol>\n";
}


function vertablalinkexameninicio() {
	    	
	//echo "<font size=2>\n";
	echo "<ul>";
	while ($row = mysql_fetch_row($this->Consulta_ID)) {
	   for ($i = 0; $i < $this->numcampos(); $i++){
   		//$consultanombreasignatura="SELECT NOMBRE FROM ASIGNATURA WHERE IDASIGNATURA='$row[$i]'";
		$consultanombreexamen="SELECT NOMBRE FROM EXAMEN WHERE IDASIGNATURA='$row[$i]'";
		$res=mysql_query($consultanombreexamen) or die('Error: '.mysql_error()."<BR>Query: $consultanombreexamen");
		$nombreexamen = mysql_fetch_row($res);
		$consultaidexamen="SELECT distinct E.IDEXAMEN FROM EXAMEN E, PREGUNTA P WHERE E.IDEXAMEN=P.IDEXAMEN AND IDASIGNATURA='$row[$i]'";
		$res4=mysql_query($consultaidexamen) or die('Error: '.mysql_error()."<BR>Query: $consultaidexamen");

		while ($asignaturaexamen = mysql_fetch_row($res4)){
			for ($j = 0; $j < $this->numcampos(); $j++){
				//echo "$asignaturaexamen[$j]\n";
				$consultapregunta="SELECT P.TITULO FROM PREGUNTA P, EXAMEN E WHERE E.IDEXAMEN=P.IDEXAMEN AND E.IDEXAMEN=$asignaturaexamen[$j] AND E.IDASIGNATURA='$row[$i]'";
				$res3=mysql_query($consultapregunta) or die('Error: '.mysql_error()."<BR>Query: $consultapregunta");
				$consultanumeropreguntas="SELECT P.IDPREGUNTA FROM PREGUNTA P, EXAMEN E WHERE  E.IDEXAMEN=$asignaturaexamen[$j] AND E.IDEXAMEN=P.IDEXAMEN AND E.IDASIGNATURA='$row[$i]'";
				$res2=mysql_query($consultanumeropreguntas) or die('Error: '.mysql_error()."<BR>Query: $consultanumeropreguntas");
		                $numeropreguntas = mysql_num_rows($res2);

				echo "<br><li><a href='inicio.php?menu=preguntas&amp;enlace=verpregunta&amp;var=".$asignaturaexamen[$j]."'>".$nombreexamen[0]." ($numeropreguntas)"."</a><br>\n";

				while ($consultapregunta = mysql_fetch_row($res3)){
					for ($j = 0; $j < $this->numcampos(); $j++){
						echo "$consultapregunta[$j]\n";
					}
				echo "<br>\n";
				}
			}
		}

	   }
	}
	echo "</ul>\n";
	
}

function vertablalinkexameninicio2() {
	    	

	//echo "<font size=2>\n";
	echo "<ul>";
	while ($row = mysql_fetch_row($this->Consulta_ID)) {
	   for ($i = 0; $i < $this->numcampos(); $i++){
   		//$consultanombreasignatura="SELECT NOMBRE FROM ASIGNATURA WHERE IDASIGNATURA='$row[$i]'";
		$consultanombreexamen="SELECT NOMBRE FROM EXAMEN WHERE IDASIGNATURA='$row[$i]'";
		$res=mysql_query($consultanombreexamen) or die('Error: '.mysql_error()."<BR>Query: $consultanombreexamen");
		$nombreexamen = mysql_fetch_row($res);
		$consultaidexamen="SELECT distinct E.IDEXAMEN FROM EXAMEN E, PREGUNTA P WHERE E.IDEXAMEN=P.IDEXAMEN AND IDASIGNATURA='$row[$i]'";
		$res4=mysql_query($consultaidexamen) or die('Error: '.mysql_error()."<BR>Query: $consultaidexamen");

		while ($asignaturaexamen = mysql_fetch_row($res4)){
			for ($j = 0; $j < $this->numcampos(); $j++){
				//echo "$asignaturaexamen[$j]\n";
				$consultapregunta="SELECT P.TITULO FROM PREGUNTA P, EXAMEN E WHERE E.IDEXAMEN=P.IDEXAMEN AND E.IDEXAMEN=$asignaturaexamen[$j] AND E.IDASIGNATURA='$row[$i]'";
				$res3=mysql_query($consultapregunta) or die('Error: '.mysql_error()."<BR>Query: $consultapregunta");
				$consultanumeropreguntas="SELECT P.IDPREGUNTA FROM PREGUNTA P, EXAMEN E WHERE  E.IDEXAMEN=$asignaturaexamen[$j] AND E.IDEXAMEN=P.IDEXAMEN AND E.IDASIGNATURA='$row[$i]'";
				$res2=mysql_query($consultanumeropreguntas) or die('Error: '.mysql_error()."<BR>Query: $consultanumeropreguntas");
		                $numeropreguntas = mysql_num_rows($res2);

				echo "<br><li><a href='inicio.php?menu=preguntas&amp;enlace=actualizarpregunta&amp;var=".$asignaturaexamen[$j]."'>".$nombreexamen[0]." ($numeropreguntas)"."</a><br>\n";

				while ($consultapregunta = mysql_fetch_row($res3)){
					for ($j = 0; $j < $this->numcampos(); $j++){
						echo "$consultapregunta[$j]\n";
					}
				echo "<br>\n";
				}
			}
		}

	   }
	}

}

function vertablalinkexameninicio3() {
	    	
	//echo "<font size=2>\n";
	echo "<ul>";
	while ($row = mysql_fetch_row($this->Consulta_ID)) {
	   for ($i = 0; $i < $this->numcampos(); $i++){
   		//$consultanombreasignatura="SELECT NOMBRE FROM ASIGNATURA WHERE IDASIGNATURA='$row[$i]'";
		$consultanombreexamen="SELECT NOMBRE FROM EXAMEN WHERE IDASIGNATURA='$row[$i]'";
		$res=mysql_query($consultanombreexamen) or die('Error: '.mysql_error()."<BR>Query: $consultanombreexamen");
		$nombreexamen = mysql_fetch_row($res);
		$consultaidexamen="SELECT distinct E.IDEXAMEN FROM EXAMEN E, PREGUNTA P WHERE E.IDEXAMEN=P.IDEXAMEN AND IDASIGNATURA='$row[$i]'";
		$res4=mysql_query($consultaidexamen) or die('Error: '.mysql_error()."<BR>Query: $consultaidexamen");

		while ($asignaturaexamen = mysql_fetch_row($res4)){
			for ($j = 0; $j < $this->numcampos(); $j++){
				//echo "$asignaturaexamen[$j]\n";
				$consultapregunta="SELECT P.TITULO FROM PREGUNTA P, EXAMEN E WHERE E.IDEXAMEN=P.IDEXAMEN AND E.IDEXAMEN=$asignaturaexamen[$j] AND E.IDASIGNATURA='$row[$i]'";
				$res3=mysql_query($consultapregunta) or die('Error: '.mysql_error()."<BR>Query: $consultapregunta");
				$consultanumeropreguntas="SELECT P.IDPREGUNTA FROM PREGUNTA P, EXAMEN E WHERE  E.IDEXAMEN=$asignaturaexamen[$j] AND E.IDEXAMEN=P.IDEXAMEN AND E.IDASIGNATURA='$row[$i]'";
				$res2=mysql_query($consultanumeropreguntas) or die('Error: '.mysql_error()."<BR>Query: $consultanumeropreguntas");
		                $numeropreguntas = mysql_num_rows($res2);

				echo "<br><li><a href='inicio.php?menu=preguntas&amp;enlace=borrarpregunta&amp;var=".$asignaturaexamen[$j]."'>".$nombreexamen[0]." ($numeropreguntas)"."</a><br>\n";

				while ($consultapregunta = mysql_fetch_row($res3)){
					for ($j = 0; $j < $this->numcampos(); $j++){
						echo "$consultapregunta[$j]\n";
					}
				echo "<br>\n";
				}
			}
		}

	   }
	}
	echo "</ul>\n";

}


function verconsultabox() {
	while ($row = mysql_fetch_row($this->Consulta_ID)) {
		echo "<Option>".$row[0]."</Option>\n";

	}
}

function verconsultaboxasignatura() {
	
	while ($row = mysql_fetch_row($this->Consulta_ID)) {
		$consultanombreasignatura="SELECT A.NOMBRE FROM ASIGNATURA A, EXAMEN E WHERE E.NOMBRE='$row[0]' AND E.IDASIGNATURA=A.IDASIGNATURA";
		$res=mysql_query($consultanombreasignatura) or die('Error: '.mysql_error()."<BR>Query: $consultanombreasignatura");
		$nombreasignatura = mysql_fetch_row($res);
		echo "<Option value='$row[0]'>$row[0] ($nombreasignatura[0])</Option>\n";
	}
}

function verconsultaradio() {
	while ($row = mysql_fetch_row($this->Consulta_ID)) {
		echo "<input type='radio' name=nombre value='$row[0]'>".$row[0]."<BR>\n";
	}
}

function verconsultatabla() {
    echo "<table class='formato' border=0 CELLPADDING=10 CELLSPACING=0>\n";
    echo "<font size=2>\n";
    for ($i = 0; $i < $this->numcampos(); $i++){
	     echo "<td class='formatotd'><center><b>".$this->nombrecampo($i)."</b></center></td>\n";
    }
    echo "</tr>\n";
    while ($row = mysql_fetch_row($this->Consulta_ID)) {
	     echo "<tr> \n";
	     for ($i = 0; $i < $this->numcampos(); $i++){
		      echo "<td class='formatotd'><center>".$row[$i]."</center></td>\n";
	     }
	     echo "</tr>\n";
     }
	 echo "</table>";
}


function verconsultapregunta() {
    //echo "<font size=3>\n";
    while ($row = mysql_fetch_row($this->Consulta_ID)) {
	     for ($i = 0; $i < $this->numcampos(); $i++){
			  echo "<b>".$this->nombrecampo($i).": </b>";
			  echo " ".nl2br($row[$i])."";
			  echo "<br>";
	     }
     }

}

function verconsultapreguntas() {    
    //echo "<font size=3>\n";

    while ($row = mysql_fetch_row($this->Consulta_ID)) {
	     for ($i = 0; $i < $this->numcampos(); $i++){
			  echo "<b>".$this->nombrecampo($i)." </b><br>";
			  //echo " ".nl2br($row[$i])."";
			  //echo "<br>";
			  echo "$row[$i]";
	     }
     }
}

function updatepregunta() {
require "CONFIG/configuracion.php";

    $reg=array("IDPREGUNTA: ", "IDEXAMEN: ",  "PREGUNTA: ",  "TEXTO: ", "SOLUCION: ", "COMENTARIOS: ", "FECHA: ", "PUNTUACION: ", "CODIGOBARRAS: " );
    $m=0;
    while ($row = mysql_fetch_row($this->Consulta_ID)) {
		for ($i = 0; $i < $this->numcampos(); $i++){
			$reg[$m]=$row[$i];
			$m++;
		}
	}
	
	echo "<BR><BR>";
	
	echo "<FORM METHOD='post' ACTION='inicio.php?menu=preguntas&amp;enlace=updatepregunta'>";
	echo "<b>$langpreguntaexamen </b>";
	echo "<select name='examen'>";
	
	$consultanombreexamen="SELECT NOMBRE FROM EXAMEN WHERE IDEXAMEN='$reg[1]'";
	$res=mysql_query($consultanombreexamen) or die('Error: '.mysql_error()."<BR>Query: $consultanombreexamen");
	$nombreexamen = mysql_fetch_row($res);

        $consultanombreasignatura="SELECT A.NOMBRE FROM ASIGNATURA A, EXAMEN E WHERE E.IDEXAMEN='$reg[1]' AND E.IDASIGNATURA=A.IDASIGNATURA";
        $res=mysql_query($consultanombreasignatura) or die('Error: '.mysql_error()."<BR>Query: $consultanombreasignatura");
        $nombreasignatura = mysql_fetch_row($res);

	
	$consultaexamen="SELECT NOMBRE FROM EXAMEN ORDER BY NOMBRE";
	$res=mysql_query($consultaexamen) or die('Error: '.mysql_error()."<BR>Query: $consultaexamen");
		while ($rowexamen = mysql_fetch_row($res)) {
			if ($rowexamen[0]==$nombreexamen[0]) {
				echo "<Option selected='selected' value='$rowexamen[0]'>$rowexamen[0] ($nombreasignatura[0])</Option>\n";
			}
			else{
				echo "<Option value='$rowexamen[0]'>$rowexamen[0] ($nombreasignatura[0])</Option>\n";
			}	
		}
		
	echo "</select>";
	echo "<BR><BR>";
	echo "<input type=hidden name='idpregunta' value='$reg[0]'><br>";
	echo "<input type=hidden name='idexamen' value='$reg[1]'><br>";
	echo "<b>$langpreguntapregunta:</b> <input type='text' name='pregunta' value='$reg[2]' size=65><br><br><br>";

	echo "<b>$langpreguntatexto:</b> <br>";
	//echo "<textarea rows=15 cols=75 name='texto' value='$reg[3]'>$reg[3]</textarea><br><br>";

	$oFCKeditor = new FCKeditor('texto') ;
	$oFCKeditor->BasePath = 'fckeditor/' ;
	//$oFCKeditor->Value = "".nl2br($reg[3])."";
	$oFCKeditor->Value = "$reg[3]";
	$oFCKeditor->ToolbarSet = 'Elsam';
	$oFCKeditor->Width='97%';
	$oFCKeditor->Height='260';
	$oFCKeditor->Create() ;

	echo "<BR><BR>";
	echo "<b>$langpreguntasolucion:</b> <br>";
	//echo "<textarea rows=15 cols=75 name='solucion' value='$reg[4]'>$reg[4]</textarea><br><br>";

	$oFCKeditor = new FCKeditor('solucion') ;
	$oFCKeditor->BasePath = 'fckeditor/' ;
	//$oFCKeditor->Value = "".nl2br($reg[4])."";
	$oFCKeditor->Value = "$reg[4]";
	$oFCKeditor->ToolbarSet = 'Elsam';
	$oFCKeditor->Width='97%';
	$oFCKeditor->Height='260';
	$oFCKeditor->Create() ;

	echo "<BR><BR>";
	echo "<b>$langpreguntacomentario:</b><input type='text' name='comentario' value='$reg[5]' size=50><br><br><br>";
	echo "<b>$langpreguntafecha (dd/mm/yyyy):</b> <input type='text' name='fecha' value='$reg[6]' size=12><br><br><br>";
	echo "<b>$langpreguntapuntuacion:</b> <input type='text' name='puntuacion' value='$reg[7]' size=10><br><br><br>";
	echo "<b>$langpreguntacodigobarras:</b> <input type='text' name='codigobarras' value='$reg[8]' size=10>";
	echo "<b>$langpreguntacodigobarras2</b> <br><br><br>";
	echo "<BR><BR><BR>";
	echo "<center><input type='submit' class='button' value='$langpreguntaactualizaok2 $reg[2]'></center>";
	echo "<BR><BR>";
	echo "</FORM> ";
		    
}

} //fin de la Clse DB_mysql
?>
