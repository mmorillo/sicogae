<?php
/************************************************************************
* Software: ELSAM                                                       *
* Version:  0.9                                                         *
* Date:     2006-08-06                                                  *
* Author:   Miguel Morillo Iruela                                       *
* License:  GPL                                                         *
* This program is distributed under the terms and conditions of the GPL *
* See the LICENSE files for details                                     *
************************************************************************/

/* Autenticacion */

require 'CHECK/check_login.php';

require 'CHECK/chequealogin.php';

// login OK

?>

<BR><BR><BR><BR>

<?php
require ("funciones-preguntas.inc.php");


if ($_POST[examen]=='' )
{
   echo("<IMG SRC='imagenes/peligro.png' NAME='Peligro' ALIGN=MIDDLE BORDER=0><br><br>");
   die("$langerrorpregunta1");
}

$miconexion = new DB_mysql ;
$miconexion->conectar($BaseDatosNombre, $BaseDatosServidor, $BaseDatosUsuario, $BaseDatosClave);
$miconexion->consulta("SELECT IDEXAMEN FROM EXAMEN WHERE NOMBRE='".$_POST[examen]."'");
if ($miconexion->Errno>0 )
{
	echo("<IMG SRC='imagenes/peligro.png' NAME='Peligro' ALIGN=MIDDLE BORDER=0><br><br>");
	echo("$langerrorpregunta2");echo("<br><br>");
	die("Error: $miconexion->Error");
}

$idexamen=$miconexion->verconsulta0();

//Comprobamos si se rellenan los campos requeridos
if ($_POST[pregunta]=='' )
{
   echo("<IMG SRC='imagenes/peligro.png' NAME='Peligro' ALIGN=MIDDLE BORDER=0><br><br>");
   die("$langerrorpregunta3");
}


//$textofckeditor = stripslashes( $_POST['textofckeditor'] ) ;
//$solucionfckeditor= stripslashes( $_POST['solucionfckeditor'] ) ;
$textofckeditor2 = $_POST['textofckeditor'] ;
$solucionfckeditor= $_POST['solucionfckeditor'] ;
$cadenaseliminar=array("\r","\n");
$textofckeditor = str_replace($cadenaseliminar, "", "$textofckeditor2");

/*FCKEDITOR*/
$miconexion = new DB_mysql;
$miconexion->conectar($BaseDatosNombre, $BaseDatosServidor, $BaseDatosUsuario, $BaseDatosClave);
/*NORMAL*/
//$miconexion->consulta("INSERT INTO PREGUNTA (IDEXAMEN, TITULO, SOLUCION, TEXTO, COMENTARIO, FECHA, PUNTUACION, CODIGOBARRAS) VALUES ('$idexamen', '$_POST[pregunta]', '$_POST[solucion]', '$_POST[texto]', '$_POST[comentario]', '$_POST[date]', '$_POST[puntuacion]','$_POST[codigobarras]')");

/*FKEDITOR*/
$miconexion->consulta("INSERT INTO PREGUNTA (IDEXAMEN, TITULO, SOLUCION, TEXTO, COMENTARIO, FECHA, PUNTUACION, CODIGOBARRAS) VALUES ('$idexamen', '$_POST[pregunta]', '$solucionfckeditor', '$textofckeditor', '$_POST[comentario]', '$_POST[date]', '$_POST[puntuacion]','$_POST[codigobarras]')");

if ($miconexion->Errno>0 )
{
	echo("$langerrorpregunta4");echo("<br><br>");
	die("Error: $miconexion->Error");
}


echo "<FONT size=5>$langpreguntainsertaok</FONT>";


?>


<?php
/* Pie */
require_once "pie.php";
?>
