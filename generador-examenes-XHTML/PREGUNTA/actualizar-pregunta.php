<?php
/************************************************************************
* Software: ELSAM                                                       *
* Version:  0.9                                                         *
* Date:     2006-08-06                                                  *
* Author:   Miguel Morillo Iruela                                       *
* License:  GPL                                                         *
* This program is distributed under the terms and conditions of the GPL *
* See the LICENSE files for details                                     *
************************************************************************/

/* Autenticacion */

require 'CHECK/check_login.php';

require 'CHECK/chequealogin.php';

// login OK

?>

<BR><BR><BR>

<CENTER>
<FONT size=5><?php echo "$langpreguntaactualiza1"?></FONT>
<BR><BR><BR>

<form action='inicio.php?menu=preguntas&amp;enlace=modificapregunta' method='post'> 
<select name="pregunta"> 
<?
require ("funciones-preguntas.inc.php");

$idexamen = $_GET["var"];

$miconexion = new DB_mysql ;
$miconexion->conectar($BaseDatosNombre, $BaseDatosServidor, $BaseDatosUsuario, $BaseDatosClave);
$miconexion->consulta("SELECT P.TITULO FROM PREGUNTA P, EXAMEN E WHERE E.IDEXAMEN=P.IDEXAMEN AND E.IDEXAMEN='$idexamen' ORDER BY P.IDPREGUNTA, P.TITULO");
$miconexion->verconsultabox();

?>
</select>
<input type="submit" class="button" value="<?php echo "$langpreguntaactualizaboton"?>"> 
</form>
</CENTER>


<?php
/* Pie */
require_once "pie.php";
?>
