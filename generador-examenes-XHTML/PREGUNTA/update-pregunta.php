<?php
/************************************************************************
* Software: ELSAM                                                       *
* Version:  0.9                                                         *
* Date:     2006-08-06                                                  *
* Author:   Miguel Morillo Iruela                                       *
* License:  GPL                                                         *
* This program is distributed under the terms and conditions of the GPL *
* See the LICENSE files for details                                     *
************************************************************************/

/* Autenticacion */

require 'CHECK/check_login.php';

require 'CHECK/chequealogin.php';

// login OK

?>

<BR><BR><BR>

<?php
require ("funciones-preguntas.inc.php");

if ($_POST[pregunta]=='' )
{
	echo("<IMG SRC='imagenes/peligro.png' NAME='Peligro' ALIGN=MIDDLE BORDER=0 alt=''><br><br>");
	die("$langerrorpregunta6");
}


$miconexion = new DB_mysql ;
$miconexion->conectar($BaseDatosNombre, $BaseDatosServidor, $BaseDatosUsuario, $BaseDatosClave);
$miconexion->consulta("SELECT IDEXAMEN FROM EXAMEN WHERE NOMBRE='".$_POST[examen]."'");
if ($miconexion->Errno>0 )
{
	echo("<IMG SRC='imagenes/peligro.png' NAME='Peligro' ALIGN=MIDDLE BORDER=0 alt=''><br><br>");
	echo("$langerrorpregunta5");echo("<br><br>");
	die("Error: $miconexion->Error");
}

$idexamen=$miconexion->verconsulta0();


$textofckeditor2 = $_POST['texto'] ;
$cadenaseliminar=array("\r","\n");
$textofckeditor3 = str_replace($cadenaseliminar, "", "$textofckeditor2");
$textofckeditor=trim($textofckeditor3);

$miconexion = new DB_mysql ;
$miconexion->conectar($BaseDatosNombre, $BaseDatosServidor, $BaseDatosUsuario, $BaseDatosClave);
$miconexion->consulta("UPDATE PREGUNTA SET TITULO='$_POST[pregunta]', SOLUCION='$_POST[solucion]', TEXTO='$textofckeditor', COMENTARIO='$_POST[comentario]' , FECHA='$_POST[fecha]' , PUNTUACION='$_POST[puntuacion]', CODIGOBARRAS='$_POST[codigobarras]', IDEXAMEN='$idexamen' WHERE IDPREGUNTA='$_POST[idpregunta]'");


echo "<br><br>";
echo "<center>";
echo "<FONT size=5>$langpreguntaactualizaok</FONT>";
echo "<br><br>";

?>

</CENTER>

<?php
/* Pie */
require_once "pie.php";
?>
