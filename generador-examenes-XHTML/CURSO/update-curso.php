<?php
/************************************************************************
* Software: ELSAM                                                       *
* Version:  0.9                                                         *
* Date:     2006-08-06                                                  *
* Author:   Miguel Morillo Iruela                                       *
* License:  GPL                                                         *
* This program is distributed under the terms and conditions of the GPL *
* See the LICENSE files for details                                     *
************************************************************************/

/* Autenticacion */

require 'CHECK/check_login.php';

require 'CHECK/chequealogin.php';

// login OK
?>

<BR><BR><BR>

<?php
require ("funciones-cursos.inc.php");


if ($_POST[nombre]=='' )
{
	echo("<IMG SRC='imagenes/peligro.png' NAME='Peligro' ALIGN=MIDDLE BORDER=0 alt=''><br><br>");
	die('Debes rellenar el campo curso correctamente.');
}

$miconexion = new DB_mysql ;
$miconexion->conectar($BaseDatosNombre, $BaseDatosServidor, $BaseDatosUsuario, $BaseDatosClave);
$miconexion->consulta("UPDATE CURSO SET NOMBRE='$_POST[nombre]', COMENTARIO='$_POST[comentario]' WHERE IDCURSO='$_POST[idcurso]'");

if ($miconexion->Errno>0 )
{
	echo("<IMG SRC='imagenes/peligro.png' NAME='Peligro' ALIGN=MIDDLE BORDER=0 alt=''><br><br>");
	echo("$langerrorcurso1");echo("<br><br>");
	die("Error: $miconexion->Error");
}

echo "<br><br>";
echo "<center>";
echo "<FONT size=5>$langcursook</FONT>";
?>

</CENTER>

<?php
/* Pie */
require_once "pie.php";
?>
