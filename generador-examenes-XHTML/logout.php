<?php
/************************************************************************
* Software: ELSAM                                                       *
* Version:  0.9                                                         *
* Date:     2006-08-06                                                  *
* Author:   Miguel Morillo Iruela                                       *
* License:  GPL                                                         *
* This program is distributed under the terms and conditions of the GPL *
* See the LICENSE files for details                                     *
************************************************************************/

    ob_start();
    require 'SQL/db_connect.php';    // conexion a BD
    require 'CHECK/check_login.php';
    if ($logged_in == 0)
    {
        die('You are not logged in so you cannot log out. <p> <a href="index.php">Login</a>');
    }
    setcookie("uname", "", time() - 60);
    setcookie("passwd", "", time() - 60);
    unset($_SESSION['username']);
    unset($_SESSION['password']);
    /* Matamos sesiones anteriores */
    $_SESSION = array(); // resetear sesiones del array
    session_destroy();   // destruir sesion
    header("Location: index.php"); // redireccion salida
    ob_end_flush();
?>
