<?php
/************************************************************************
* Software: ELSAM                                                       *
* Version:  0.92                                                         *
* Date:     2006-08-06                                                  *
* Author:   Miguel Morillo Iruela                                       *
* License:  GPL                                                         *
* This program is distributed under the terms and conditions of the GPL *
* See the LICENSE files for details                                     *
************************************************************************/

/* Autenticacion */

require 'CHECK/check_login.php';

require 'CHECK/chequealogin.php';

// login OK

require ("funciones-asignaturas.inc.php");
?>

<BR><BR>

<FORM METHOD="post" ACTION="inicio.php?menu=asignaturas&amp;enlace=formularioinsertaasignatura">

<center>
<FONT size=5><?php echo "$langasignaturainsertar"?></FONT>
<HR><BR><BR>
</center>

<b>Curso: </b>

<select name="curso">
<?php
$miconexion2 = new DB_mysql ;
$miconexion2->conectar($BaseDatosNombre, $BaseDatosServidor, $BaseDatosUsuario, $BaseDatosClave);
$miconexion2->consulta("SELECT NOMBRE FROM CURSO ORDER BY NOMBRE");
$miconexion2->verconsultabox();
?>

</select> 

<br><br>

<b>Nombre asignatura:</b> <input type='text' name='asignatura' value='' size=40><br><br>
<b>Grupo:</b> <input type='text' name='grupo' value='' size=51><br><br>
<b>Comentario:</b> <BR><textarea rows=10 cols=65 name='comentario'></textarea><br><br>
<BR><BR>
<center>
<input type="submit" class="button" value="<?php echo "$langasignaturainsertaboton"?>">
</center>
</FORM>

<?php
/* Pie */
require_once "pie.php";
?>
