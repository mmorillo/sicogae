<?php
/************************************************************************
* Software: ELSAM                                                       *
* Version:  0.92                                                         *
* Date:     2006-08-06                                                  *
* Author:   Miguel Morillo Iruela                                       *
* License:  GPL                                                         *
* This program is distributed under the terms and conditions of the GPL *
* See the LICENSE files for details                                     *
************************************************************************/

/* Autenticacion */

require 'CHECK/check_login.php';

require 'CHECK/chequealogin.php';

// login OK
?>


<BR><BR>

<?php
require ("funciones-asignaturas.inc.php");

if ($_POST[nombre]=='' )
{
	echo("<IMG SRC='imagenes/peligro.png' NAME='Peligro' ALIGN=MIDDLE BORDER=0><br><br>");
	die("$langerrorasignatura1");
}


$miconexion = new DB_mysql ;
$miconexion->conectar($BaseDatosNombre, $BaseDatosServidor, $BaseDatosUsuario, $BaseDatosClave);
$miconexion->consulta("SELECT C.IDCURSO FROM CURSO C WHERE C.NOMBRE='".$_POST[curso]."'");
if ($miconexion->Errno>0 )
{
	echo("<IMG SRC='imagenes/peligro.png' NAME='Peligro' ALIGN=MIDDLE BORDER=0 alt=''><br><br>");
	echo("$langerrorasignatura2");echo("<br><br>");
	die("Error: $miconexion->Error");
}

$idcurso=$miconexion->verconsulta0();

if ($idcurso == ''){
	echo("<IMG SRC='imagenes/peligro.png' NAME='Peligro' ALIGN=MIDDLE BORDER=0 alt=''><br><br>");
	echo("$langerrorasignatura3");echo("<br><br>");
	die("$langerrorasignatura4");
}


$miconexion = new DB_mysql ;
$miconexion->conectar($BaseDatosNombre, $BaseDatosServidor, $BaseDatosUsuario, $BaseDatosClave);
$miconexion->consulta("UPDATE ASIGNATURA SET NOMBRE='$_POST[nombre]', GRUPO='$_POST[grupo]', COMENTARIO='$_POST[comentario]', IDCURSO='$idcurso' WHERE IDASIGNATURA='$_POST[idasignatura]'");

if ($miconexion->Errno>0 )
{
	echo("<IMG SRC='imagenes/peligro.png' NAME='Peligro' ALIGN=MIDDLE BORDER=0 alt=''><br><br>");
	echo("$langerrorasignatura5");echo("<br><br>");
	die("Error: $miconexion->Error");
}


echo "<br><br>";
echo "<center>";
echo "<FONT size=5>$langasignaturamodificaok</FONT>";
?>

</CENTER>

<?php
/* Pie */
require_once "pie.php";
?>
