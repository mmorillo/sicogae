<?php
/************************************************************************
* Software: ELSAM                                                       *
* Version:  0.92                                                         *
* Date:     2006-08-06                                                  *
* Author:   Miguel Morillo Iruela                                       *
* License:  GPL                                                         *
* This program is distributed under the terms and conditions of the GPL *
* See the LICENSE files for details                                     *
************************************************************************/

/* Autenticacion */

require 'CHECK/check_login.php';

require 'CHECK/chequealogin.php';

// login OK
?>

<BR><BR>
<CENTER>
<FONT size=5><?php echo "$langasignaturatitulo1"?></FONT>
<HR>
<BR>
</CENTER>

<?php
require ("funciones-asignaturas.inc.php");

$idcurso = $_GET["var"];

$miconexion = new DB_mysql ;
$miconexion->conectar($BaseDatosNombre, $BaseDatosServidor, $BaseDatosUsuario, $BaseDatosClave);
$miconexion->consulta("SELECT IDASIGNATURA FROM ASIGNATURA WHERE IDCURSO='$idcurso' ORDER BY GRUPO");
$miconexion->vertablalinkasignaturainicio();

?>

<?php
/* Pie */
require_once "pie.php";
?>
