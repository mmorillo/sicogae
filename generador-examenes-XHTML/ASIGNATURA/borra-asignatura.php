<?php
/************************************************************************
* Software: ELSAM                                                       *
* Version:  0.92                                                         *
* Date:     2006-08-06                                                  *
* Author:   Miguel Morillo Iruela                                       *
* License:  GPL                                                         *
* This program is distributed under the terms and conditions of the GPL *
* See the LICENSE files for details                                     *
************************************************************************/

/* Autenticacion */

require 'CHECK/check_login.php';

require 'CHECK/chequealogin.php';

// login OK
?>

<BR><BR>
<center>
<FONT size=5><?php echo "$langasignaturaborra1"?></FONT>
</center>
<HR><BR><BR>

<FORM ACTION='inicio.php?menu=asignaturas&amp;enlace=borrarasignatura2' method='post'>
<?php
require ("funciones-asignaturas.inc.php");

$idcurso = $_GET["var"];

$miconexion = new DB_mysql ;
$miconexion->conectar($BaseDatosNombre, $BaseDatosServidor, $BaseDatosUsuario, $BaseDatosClave);
$miconexion->consulta("SELECT NOMBRE FROM ASIGNATURA WHERE IDCURSO='$idcurso' ORDER BY NOMBRE");
$miconexion->verconsultaradio();
?>


<BR><BR>
<INPUT TYPE=SUBMIT class="button" VALUE="<?php echo "$langasignaturaborraboton"?>">
</FORM>

<?php
/* Pie */
require_once "pie.php";
?>
