<?php
/************************************************************************
* Software: ELSAM                                                       *
* Version:  0.92                                                         *
* Date:     2006-08-06                                                  *
* Author:   Miguel Morillo Iruela                                       *
* License:  GPL                                                         *
* This program is distributed under the terms and conditions of the GPL *
* See the LICENSE files for details                                     *
************************************************************************/

/* Autenticacion */

require 'CHECK/check_login.php';

require 'CHECK/chequealogin.php';

// login OK
?>


<BR><BR>
<?
require ("funciones-asignaturas.inc.php");

$idasignatura = $_GET["var"]; 

echo "<center>";
echo "<FONT size=5>$langasignaturatitulo2</FONT>";
echo "<hr><br><br>";
echo "</center>";


$miconexion = new DB_mysql ;
$miconexion->conectar($BaseDatosNombre, $BaseDatosServidor, $BaseDatosUsuario, $BaseDatosClave);
$miconexion->consulta("SELECT NOMBRE AS '$langasignatura1',GRUPO AS '$langasignaturagrupo' FROM ASIGNATURA WHERE IDASIGNATURA='$idasignatura'");
$miconexion->verconsultaasignatura();

echo "<br>";

$miconexion = new DB_mysql ;
$miconexion->conectar($BaseDatosNombre, $BaseDatosServidor, $BaseDatosUsuario, $BaseDatosClave);
$miconexion->consulta("SELECT COMENTARIO AS '$langasignaturacomentario' FROM ASIGNATURA WHERE IDASIGNATURA='$idasignatura'");
$miconexion->verconsultaasignaturas();

echo "<br>";

$miconexion = new DB_mysql ;
$miconexion->conectar($BaseDatosNombre, $BaseDatosServidor, $BaseDatosUsuario, $BaseDatosClave);
$miconexion->consulta("SELECT C.NOMBRE AS '$langasignaturacurso' FROM ASIGNATURA A, CURSO C WHERE A.IDASIGNATURA='$idasignatura' AND A.IDCURSO=C.IDCURSO");
$miconexion->verconsultaasignatura();

echo "<br>";

$miconexion = new DB_mysql ;
$miconexion->conectar($BaseDatosNombre, $BaseDatosServidor, $BaseDatosUsuario, $BaseDatosClave);
$miconexion->consulta("SELECT E.IDEXAMEN AS '$langasignaturaexamenes' FROM ASIGNATURA A, EXAMEN E WHERE A.IDASIGNATURA='$idasignatura' AND E.IDASIGNATURA=A.IDASIGNATURA");
$miconexion->verconsultaexamenes();


/* Pie */
require_once "pie.php";
?>
