<?php
/************************************************************************
* Software: ELSAM                                                       *
* Version:  0.92                                                         *
* Date:     2006-08-06                                                  *
* Author:   Miguel Morillo Iruela                                       *
* License:  GPL                                                         *
* This program is distributed under the terms and conditions of the GPL *
* See the LICENSE files for details                                     *
************************************************************************/

require 'SQL/db_connect.php';

class DB_mysql {
 
 /* variables de conexi�n */
 var $BaseDatos;
 var $Servidor;
 var $Usuario;
 var $Clave;

  
 /* identificador de conexi�n y consulta */
 var $Conexion_ID = 0;
 var $Consulta_ID = 0;

   

 /* n�mero de error y texto error */
 var $Errno = 0;
 var $Error = "";

      
 /* M�todo Constructor: Cada vez que creemos una variable
    de esta clase, se ejecutar� esta funci�n */
function DB_mysql($bd = "", $host = "localhost", $user = "nobody", $pass = "") {
   $this->BaseDatos = $bd;
   $this->Servidor = $host;
   $this->Usuario = $user;
   $this->Clave = $pass;
}

   
 /*Conexi�n a la base de datos*/
function conectar($bd, $host, $user, $pass){

	if ($bd != "") $this->BaseDatos = $bd;
	if ($host != "") $this->Servidor = $host;
	if ($user != "") $this->Usuario = $user;
	if ($pass != "") $this->Clave = $pass;
	 

// Conectamos al servidor
	$this->Conexion_ID = mysql_connect($this->Servidor, $this->Usuario, $this->Clave);
    if (!$this->Conexion_ID) {
       $this->Error = "Ha fallado la conexi�n.";
   	   return 0;
    }

//seleccionamos la base de datos
    if (!@mysql_select_db($this->BaseDatos, $this->Conexion_ID)) {
   	  $this->Error = "Imposible abrir ".$this->BaseDatos ;
	  return 0;
	}

 /* Si hemos tenido �xito conectando devuelve 
   el identificador de la conexi�n, sino devuelve 0 */
    return $this->Conexion_ID;
}

	    
/* Ejecuta un consulta */
function consulta($sql = ""){
	     if ($sql == "") {
		     $this->Error = "No ha especificado una consulta SQL";
		     return 0;
	     }

   //ejecutamos la consulta
         $this->Consulta_ID = @mysql_query($sql, $this->Conexion_ID);
  

       if (!$this->Consulta_ID) {
	       $this->Errno = mysql_errno();
	       $this->Error = mysql_error();
       }
       /* Si hemos tenido �xito en la consulta devuelve 
      el identificador de la conexi�n, sino devuelve 0 */
	  
      return $this->Consulta_ID;
}

	        

/* Devuelve el n�mero de campos de una consulta */
function numcampos() {
	return mysql_num_fields($this->Consulta_ID);
}

		 
/* Devuelve el n�mero de registros de una consulta */
function numregistros(){
	return mysql_num_rows($this->Consulta_ID);
}

	  
/* Devuelve el nombre de un campo de una consulta */
function nombrecampo($numcampo) {
	return mysql_field_name($this->Consulta_ID, $numcampo);
}

		   
function verconsulta0() {
	$row=mysql_fetch_row($this->Consulta_ID);
	return $row[0];
}


function vertablalinkasignaturainicio() {
	    	
	//echo "<font size=2>\n";
	echo "<ol>\n";
	while ($row = mysql_fetch_row($this->Consulta_ID)) {
	   for ($i = 0; $i < $this->numcampos(); $i++){
	   		$consultanombreasignatura="SELECT NOMBRE FROM ASIGNATURA WHERE IDASIGNATURA='$row[$i]'";
			$res=mysql_query($consultanombreasignatura) or die('Error: '.mysql_error()."<BR>Query: $consultanombreasignatura");
			$nombreasignatura = mysql_fetch_row($res);
	   		echo "<li><a href='inicio.php?menu=asignaturas&amp;enlace=detalleasignatura&amp;var=".nl2br($row[$i])."'>".nl2br($nombreasignatura[0])."</a>\n";
	   }
	}
	echo "</ol>\n";
	
}


function vertablalinkasignaturacursoinicio() {
	    	
	echo "<ul>\n";
	while ($row = mysql_fetch_row($this->Consulta_ID)) {
	   for ($i = 0; $i < $this->numcampos(); $i++){

			$consultanombrecurso="SELECT NOMBRE FROM CURSO WHERE IDCURSO='$row[$i]'";
			$res=mysql_query($consultanombrecurso) or die('Error: '.mysql_error()."<BR>Query: $consultanombrecurso");
			$nombrecurso = mysql_fetch_row($res);

	   		$consultanombreasignatura="SELECT A.NOMBRE FROM ASIGNATURA A, CURSO C WHERE C.IDCURSO='$row[$i]' AND A.IDCURSO=C.IDCURSO";
                        $res2=mysql_query($consultanombreasignatura) or die('Error: '.mysql_error()."<BR>Query: $consultanombreasignatura");
			$consultaidasignatura="SELECT A.IDASIGNATURA FROM ASIGNATURA A, CURSO C WHERE C.IDCURSO='$row[$i]' AND A.IDCURSO=C.IDCURSO";
                        $res3=mysql_query($consultanombreasignatura) or die('Error: '.mysql_error()."<BR>Query: $consultanombreasignatura");

	   		echo "<br><li><a href='inicio.php?menu=asignaturas&amp;enlace=verasignatura&amp;var=".nl2br($row[$i])."'>".nl2br($nombrecurso[0])."</a><br>\n";

			while ($asignaturacurso = mysql_fetch_row($res2)){
				for ($j = 0; $j < $this->numcampos(); $j++){
					echo "$asignaturacurso[$j]\n";
				}
			echo "<br>\n";
			}
	   }
	}
	echo "</ul>\n";
	
}


function vertablalinkasignaturacursoinicio2() {
	    	
	echo "<ul>\n";
	while ($row = mysql_fetch_row($this->Consulta_ID)) {
	   for ($i = 0; $i < $this->numcampos(); $i++){

			$consultanombrecurso="SELECT NOMBRE FROM CURSO WHERE IDCURSO='$row[$i]'";
			$res=mysql_query($consultanombrecurso) or die('Error: '.mysql_error()."<BR>Query: $consultanombrecurso");
			$nombrecurso = mysql_fetch_row($res);

	   		$consultanombreasignatura="SELECT A.NOMBRE FROM ASIGNATURA A, CURSO C WHERE C.IDCURSO='$row[$i]' AND A.IDCURSO=C.IDCURSO";
                        $res2=mysql_query($consultanombreasignatura) or die('Error: '.mysql_error()."<BR>Query: $consultanombreasignatura");
			$consultaidasignatura="SELECT A.IDASIGNATURA FROM ASIGNATURA A, CURSO C WHERE C.IDCURSO='$row[$i]' AND A.IDCURSO=C.IDCURSO";
                        $res3=mysql_query($consultanombreasignatura) or die('Error: '.mysql_error()."<BR>Query: $consultanombreasignatura");

	   		echo "<br><li><a href='inicio.php?menu=asignaturas&amp;enlace=actualizaasignatura&amp;var=".nl2br($row[$i])."'>".nl2br($nombrecurso[0])."</a><br>\n";

			while ($asignaturacurso = mysql_fetch_row($res2)){
				for ($j = 0; $j < $this->numcampos(); $j++){
					echo "$asignaturacurso[$j]\n";
				}
			echo "<br>\n";
			}
	   }
	}
	echo "</ul>\n";
	
}



function vertablalinkasignaturacursoinicio3() {
	    	
	echo "<ul>\n";
	while ($row = mysql_fetch_row($this->Consulta_ID)) {
	   for ($i = 0; $i < $this->numcampos(); $i++){

			$consultanombrecurso="SELECT NOMBRE FROM CURSO WHERE IDCURSO='$row[$i]'";
			$res=mysql_query($consultanombrecurso) or die('Error: '.mysql_error()."<BR>Query: $consultanombrecurso");
			$nombrecurso = mysql_fetch_row($res);

	   		$consultanombreasignatura="SELECT A.NOMBRE FROM ASIGNATURA A, CURSO C WHERE C.IDCURSO='$row[$i]' AND A.IDCURSO=C.IDCURSO";
                        $res2=mysql_query($consultanombreasignatura) or die('Error: '.mysql_error()."<BR>Query: $consultanombreasignatura");
			$consultaidasignatura="SELECT A.IDASIGNATURA FROM ASIGNATURA A, CURSO C WHERE C.IDCURSO='$row[$i]' AND A.IDCURSO=C.IDCURSO";
                        $res3=mysql_query($consultanombreasignatura) or die('Error: '.mysql_error()."<BR>Query: $consultanombreasignatura");

	   		echo "<br><li><a href='inicio.php?menu=asignaturas&amp;enlace=borrarasignatura&amp;var=".nl2br($row[$i])."'>".nl2br($nombrecurso[0])."</a><br>\n";

			while ($asignaturacurso = mysql_fetch_row($res2)){
				for ($j = 0; $j < $this->numcampos(); $j++){
					echo "$asignaturacurso[$j]\n";
				}
			echo "<br>\n";
			}
	   }
	}
	echo "</ul>\n";
	
}


function verconsultatabla() {
		    
    echo "<table class='formato' border=0 CELLPADDING=10 CELLSPACING=0>\n";
    echo "<font size=2>\n";
    for ($i = 0; $i < $this->numcampos(); $i++){
	     echo "<td class='formatotd'><center><b>".$this->nombrecampo($i)."</b></center></td>\n";
    }
    while ($row = mysql_fetch_row($this->Consulta_ID)) {
	     echo "<tr> \n";
	     for ($i = 0; $i < $this->numcampos(); $i++){
		      echo "<td  class='formatotd'>".$row[$i]."</td>\n";
	     }
	     echo "</tr>\n";
     }
	 echo "</table>";
}


function verconsultabox() {
	while ($row = mysql_fetch_row($this->Consulta_ID)) {
		echo "<Option>".$row[0]."</Option>\n";
	}
}

function verconsultaboxcurso() {
	while ($row = mysql_fetch_row($this->Consulta_ID)) {
		$consultanombrecurso="SELECT C.NOMBRE FROM ASIGNATURA A, CURSO C WHERE A.NOMBRE='$row[0]' AND A.IDCURSO=C.IDCURSO";
		$res=mysql_query($consultanombrecurso) or die('Error: '.mysql_error()."<BR>Query: $consultanombrecurso");
		$nombrecurso = mysql_fetch_row($res);
		echo "<Option value='$row[0]'>$row[0] ($nombrecurso[0])</Option>\n";
	}
}


function verconsultaradio() {
	while ($row = mysql_fetch_row($this->Consulta_ID)) {
		echo "<input type='radio' name=nombre value='$row[0]'>".$row[0]."<BR>\n";
	}
}


function verconsultaasignatura() {    
    //echo "<font size=3>\n";
    while ($row = mysql_fetch_row($this->Consulta_ID)) {
	     for ($i = 0; $i < $this->numcampos(); $i++){
			  echo "<b>".$this->nombrecampo($i).": </b>";
			  echo " ".nl2br($row[$i])."";
			  echo "<br><br>";
	     }
     }
}

function verconsultaasignaturas() {    
    //echo "<font size=3>\n";
    while ($row = mysql_fetch_row($this->Consulta_ID)) {
	     for ($i = 0; $i < $this->numcampos(); $i++){
			  echo "<b>".$this->nombrecampo($i)." </b><br>";
			  echo " ".nl2br($row[$i])."";
			  echo "<br>";
	     }
     }
}

function verconsultaexamenes2() {    
    //echo "<font size=3>\n";
	echo "<b>".$this->nombrecampo($i)." </b><br>";
    while ($row = mysql_fetch_row($this->Consulta_ID)) {
	     for ($i = 0; $i < $this->numcampos(); $i++){
			  echo " ".nl2br($row[$i])."";
			  echo "<br>";
	     }
     }
}

function verconsultaexamenes() {    
    //echo "<font size=3>\n";
    echo "<b>".$this->nombrecampo($i)." </b><br><br>";
    echo "<ul>";
    while ($row = mysql_fetch_row($this->Consulta_ID)) {
	     for ($i = 0; $i < $this->numcampos(); $i++){
	   	   	  $consultanombreexamen="SELECT NOMBRE FROM EXAMEN WHERE IDEXAMEN='$row[$i]'";
			  $res=mysql_query($consultanombreexamen) or die('Error: '.mysql_error()."<BR>Query: $consultanombreexamen");
			  $nombreexamen = mysql_fetch_row($res);
			  echo "<li><a href='inicio.php?menu=asignaturas&amp;enlace=visualizarexamen&amp;var=".nl2br($row[$i])."'>".nl2br($nombreexamen[0])."</a> &nbsp; &nbsp; &nbsp; &nbsp;<a href='iniciopdf.php?menu=asignaturas&amp;enlace=visualizarexamenpdf&amp;var=".nl2br($row[$i])."'><IMG SRC='imagenes/pdf.png' NAME='Generar Examen PDF' ALIGN=MIDDLE BORDER=0 alt=''></a> &nbsp; &nbsp; &nbsp; &nbsp;<a href='../PDF/guarda-examen-pdf.php?var=".nl2br($row[$i])."'></a>\n";
			  echo "</li>";
	     }
     }
    echo "</ul>";
}


function updateasignatura() {
require 'CONFIG/configuracion.php';

    $reg=array("IDASIGNATURA: ", "IDCURSO: ",  "ASIGNATURA: ", "GRUPO: ","COMENTARIOS: ");
    $m=0;
    while ($row = mysql_fetch_row($this->Consulta_ID)) {
		for ($i = 0; $i < $this->numcampos(); $i++){
			$reg[$m]=$row[$i];
			$m++;
		}
	}

	echo "<FORM METHOD='post' ACTION='inicio.php?menu=asignaturas&amp;enlace=updateasignatura'>";
	
	echo "<b>$langasignaturacurso:</b> ";
	echo "<select name='curso'>";
	
	$consultanombrecurso="SELECT NOMBRE FROM CURSO WHERE IDCURSO='$reg[1]'";
	$res=mysql_query($consultanombrecurso) or die('Error: '.mysql_error()."<BR>Query: $consultanombrecurso");
	$nombrecurso = mysql_fetch_row($res);
		
	$consultacurso="SELECT NOMBRE FROM CURSO ORDER BY NOMBRE";
	$res=mysql_query($consultacurso) or die('Error: '.mysql_error()."<BR>Query: $consultacurso");
		while ($rowcurso = mysql_fetch_row($res)) {
			if ($rowcurso[0]==$nombrecurso[0]) {
				echo "<Option selected='selected'>".$rowcurso[0]."</Option>\n";
			}
			else{
				echo "<Option>".$rowcurso[0]."</Option>\n";
			}				
		}
	
	echo "</select><br>";
	
	echo "<input type=hidden name='idasignatura' value='$reg[0]'><br>";
	echo "<input type=hidden name='idcurso' value='$reg[1]'><br>";
	echo "<b>$langasignatura1:</b> <input type='text' name='nombre' value='$reg[2]' size=46><br><br>";
	echo "<b>$langasignaturagrupo:</b> <input type='text' name='grupo' value='$reg[3]' size=50><br><br>";
	echo "<b>$langasignaturacomentario:</b><br>";
	echo "<textarea rows=10 cols=65 name='comentario'>$reg[4]</textarea><br>";
	echo "<BR><BR>";
	echo "<center>";
	echo "<input type='submit' class='button' value='$langasignaturamodificaboton2 $reg[2]'>";
	echo "</center>";
	echo "</FORM> ";

		    
}

} //fin de la Clse DB_mysql
?>
