<?php
/************************************************************************
* Software: ELSAM                                                       *
* Version:  0.92                                                         *
* Date:     2006-08-06                                                  *
* Author:   Miguel Morillo Iruela                                       *
* License:  GPL                                                         *
* This program is distributed under the terms and conditions of the GPL *
* See the LICENSE files for details                                     *
************************************************************************/

/* Autenticacion */

require 'CHECK/check_login.php';

require 'CHECK/chequealogin.php';

// login OK
?>

<BR><BR><BR>
<CENTER>
<FONT size=5><?php echo "$langasignaturamodifica"?></FONT>

<BR><BR><BR>

<form action='inicio.php?menu=asignaturas&amp;enlace=modificaasignatura' method='post'> 
<select name="asignatura"> 
<?
require ("funciones-asignaturas.inc.php");

$idcurso = $_GET["var"];

$miconexion = new DB_mysql ;
$miconexion->conectar($BaseDatosNombre, $BaseDatosServidor, $BaseDatosUsuario, $BaseDatosClave);
$miconexion->consulta("SELECT NOMBRE FROM ASIGNATURA WHERE IDCURSO='$idcurso' ORDER BY GRUPO, NOMBRE");
$miconexion->verconsultaboxcurso();
?>
</select>
<input type="submit" class="button" value="<?php echo "$langasignaturamodificaboton"?>"> 

</form>

</CENTER>


<?php
/* Pie */
require_once "pie.php";
?>
