<?php
/************************************************************************
* Software: ELSAM                                                       *
* Version:  0.9                                                         *
* Date:     2006-08-06                                                  *
* Author:   Miguel Morillo Iruela                                       *
* License:  GPL                                                         *
* This program is distributed under the terms and conditions of the GPL *
* See the LICENSE files for details                                     *
************************************************************************/

include_once ( '../CONFIG/configuracion.php' );

$db = mysql_connect("$BaseDatosServidor", "$BaseDatosUsuario","$BaseDatosClave") or die("Imposible conectar a la base de datos");

mysql_select_db("$BaseDatosNombre", $db) or die("Imposible consultar la base de datos");


// generate some random data
srand((double)microtime()*1000000);


$data_1 = array();
$data_2 = array();


//EXAMENES

// Realizamos la consulta
$examenesenero = mysql_query("select count(E.NOMBRE) from EXAMEN E where E.FECHA like ('%/01/%')",$db) or die("Sentencia SQL incorrecta");
$examenesfebrero = mysql_query("select count(E.NOMBRE) from EXAMEN E where E.FECHA like ('%/02/%')",$db) or die("Sentencia SQL incorrecta");
$examenesmarzo = mysql_query("select count(E.NOMBRE) from EXAMEN E where E.FECHA like ('%/03/%')",$db) or die("Sentencia SQL incorrecta");
$examenesabril = mysql_query("select count(E.NOMBRE) from EXAMEN E where E.FECHA like ('%/04/%')",$db) or die("Sentencia SQL incorrecta");
$examenesmayo = mysql_query("select count(E.NOMBRE) from EXAMEN E where E.FECHA like ('%/05/%')",$db) or die("Sentencia SQL incorrecta");
$examenesjunio = mysql_query("select count(E.NOMBRE) from EXAMEN E where E.FECHA like ('%/06/%')",$db) or die("Sentencia SQL incorrecta");
$examenesjulio = mysql_query("select count(E.NOMBRE) from EXAMEN E where E.FECHA like ('%/07/%')",$db) or die("Sentencia SQL incorrecta");
$examenesagosto = mysql_query("select count(E.NOMBRE) from EXAMEN E where E.FECHA like ('%/08/%')",$db) or die("Sentencia SQL incorrecta");
$examenesseptiembre = mysql_query("select count(E.NOMBRE) from EXAMEN E where E.FECHA like ('%/09/%')",$db) or die("Sentencia SQL incorrecta");
$examenesoctubre = mysql_query("select count(E.NOMBRE) from EXAMEN E where E.FECHA like ('%/10/%')",$db) or die("Sentencia SQL incorrecta");
$examenesnoviembre = mysql_query("select count(E.NOMBRE) from EXAMEN E where E.FECHA like ('%/11/%')",$db) or die("Sentencia SQL incorrecta");
$examenesdiciembre = mysql_query("select count(E.NOMBRE) from EXAMEN E where E.FECHA like ('%/12/%')",$db) or die("Sentencia SQL incorrecta");

// Itera sobre el resultado de la consulta para obtener los campos que necesitamos para generar la gr�fica
while( $row = mysql_fetch_array($examenesenero) ){
   $enero[] =$row[0]; // Valores de cada fila que se insertan en el array de datos que le pasaremos a Open Flash Chart
}

while( $row = mysql_fetch_array($examenesfebrero) ){
   $febrero[] =$row[0]; // Valores de cada fila que se insertan en el array de datos que le pasaremos a Open Flash Chart
}

while( $row = mysql_fetch_array($examenesmarzo) ){
   $marzo[] =$row[0]; // Valores de cada fila que se insertan en el array de datos que le pasaremos a Open Flash Chart
}

while( $row = mysql_fetch_array($examenesabril) ){
   $abril[] =$row[0]; // Valores de cada fila que se insertan en el array de datos que le pasaremos a Open Flash Chart
}

while( $row = mysql_fetch_array($examenesmayo) ){
   $mayo[] =$row[0]; // Valores de cada fila que se insertan en el array de datos que le pasaremos a Open Flash Chart
}

while( $row = mysql_fetch_array($examenesjunio) ){
   $junio[] =$row[0]; // Valores de cada fila que se insertan en el array de datos que le pasaremos a Open Flash Chart
}

while( $row = mysql_fetch_array($examenesjulio) ){
   $julio[] =$row[0]; // Valores de cada fila que se insertan en el array de datos que le pasaremos a Open Flash Chart
}
while( $row = mysql_fetch_array($examenesagosto) ){
   $agosto[] =$row[0]; // Valores de cada fila que se insertan en el array de datos que le pasaremos a Open Flash Chart
}

while( $row = mysql_fetch_array($examenesseptiembre) ){
   $septiembre[] =$row[0]; // Valores de cada fila que se insertan en el array de datos que le pasaremos a Open Flash Chart
}

while( $row = mysql_fetch_array($examenesoctubre) ){
   $octubre[] =$row[0]; // Valores de cada fila que se insertan en el array de datos que le pasaremos a Open Flash Chart
}

while( $row = mysql_fetch_array($examenesnoviembre) ){
   $noviembre[] =$row[0]; // Valores de cada fila que se insertan en el array de datos que le pasaremos a Open Flash Chart
}

while( $row = mysql_fetch_array($examenesdiciembre) ){
   $diciembre[] =$row[0]; // Valores de cada fila que se insertan en el array de datos que le pasaremos a Open Flash Chart
}



$data_1[0] = $enero[0];
$data_1[1] = $febrero[0];
$data_1[2] = $marzo[0];
$data_1[3] = $abril[0];
$data_1[4] = $mayo[0];
$data_1[5] = $junio[0];
$data_1[6] = $julio[0];
$data_1[7] = $agosto[0];
$data_1[8] = $septiembre[0];
$data_1[9] = $octubre[0];
$data_1[10] = $noviembre[0];
$data_1[11] = $diciembre[0];



//PREGUNTAS

// Realizamos la consulta
$preguntasenero = mysql_query("select count(TITULO) from PREGUNTA where FECHA like ('%/01/%')",$db) or die("Sentencia SQL incorrecta");
$preguntasfebrero = mysql_query("select count(TITULO) from PREGUNTA where FECHA like ('%/02/%')",$db) or die("Sentencia SQL incorrecta");
$preguntasmarzo = mysql_query("select count(TITULO) from PREGUNTA where FECHA like ('%/03/%')",$db) or die("Sentencia SQL incorrecta");
$preguntasabril = mysql_query("select count(TITULO) from PREGUNTA where FECHA like ('%/04/%')",$db) or die("Sentencia SQL incorrecta");
$preguntasmayo = mysql_query("select count(TITULO) from PREGUNTA where FECHA like ('%/05/%')",$db) or die("Sentencia SQL incorrecta");
$preguntasjunio = mysql_query("select count(TITULO) from PREGUNTA where FECHA like ('%/06/%')",$db) or die("Sentencia SQL incorrecta");
$preguntasjulio = mysql_query("select count(TITULO) from PREGUNTA where FECHA like ('%/07/%')",$db) or die("Sentencia SQL incorrecta");
$preguntasagosto = mysql_query("select count(TITULO) from PREGUNTA where FECHA like ('%/08/%')",$db) or die("Sentencia SQL incorrecta");
$preguntasseptiembre = mysql_query("select count(TITULO) from PREGUNTA where FECHA like ('%/09/%')",$db) or die("Sentencia SQL incorrecta");
$preguntasoctubre = mysql_query("select count(TITULO) from PREGUNTA where FECHA like ('%/10/%')",$db) or die("Sentencia SQL incorrecta");
$preguntasnoviembre = mysql_query("select count(TITULO) from PREGUNTA where FECHA like ('%/11/%')",$db) or die("Sentencia SQL incorrecta");
$preguntasdiciembre = mysql_query("select count(TITULO) from PREGUNTA where FECHA like ('%/12/%')",$db) or die("Sentencia SQL incorrecta");

// Itera sobre el resultado de la consulta para obtener los campos que necesitamos para generar la gr�fica
while( $row = mysql_fetch_array($preguntasenero) ){
   $penero[] =$row[0]; // Valores de cada fila que se insertan en el array de datos que le pasaremos a Open Flash Chart
}

while( $row = mysql_fetch_array($preguntasfebrero) ){
   $pfebrero[] =$row[0]; // Valores de cada fila que se insertan en el array de datos que le pasaremos a Open Flash Chart
}

while( $row = mysql_fetch_array($preguntasmarzo) ){
   $pmarzo[] =$row[0]; // Valores de cada fila que se insertan en el array de datos que le pasaremos a Open Flash Chart
}

while( $row = mysql_fetch_array($preguntasabril) ){
   $pabril[] =$row[0]; // Valores de cada fila que se insertan en el array de datos que le pasaremos a Open Flash Chart
}

while( $row = mysql_fetch_array($preguntasmayo) ){
   $pmayo[] =$row[0]; // Valores de cada fila que se insertan en el array de datos que le pasaremos a Open Flash Chart
}

while( $row = mysql_fetch_array($preguntasjunio) ){
   $pjunio[] =$row[0]; // Valores de cada fila que se insertan en el array de datos que le pasaremos a Open Flash Chart
}

while( $row = mysql_fetch_array($preguntasjulio) ){
   $pjulio[] =$row[0]; // Valores de cada fila que se insertan en el array de datos que le pasaremos a Open Flash Chart
}
while( $row = mysql_fetch_array($preguntasagosto) ){
   $pagosto[] =$row[0]; // Valores de cada fila que se insertan en el array de datos que le pasaremos a Open Flash Chart
}

while( $row = mysql_fetch_array($preguntasseptiembre) ){
   $pseptiembre[] =$row[0]; // Valores de cada fila que se insertan en el array de datos que le pasaremos a Open Flash Chart
}

while( $row = mysql_fetch_array($preguntasoctubre) ){
   $poctubre[] =$row[0]; // Valores de cada fila que se insertan en el array de datos que le pasaremos a Open Flash Chart
}

while( $row = mysql_fetch_array($preguntasnoviembre) ){
   $pnoviembre[] =$row[0]; // Valores de cada fila que se insertan en el array de datos que le pasaremos a Open Flash Chart
}

while( $row = mysql_fetch_array($preguntasdiciembre) ){
   $pdiciembre[] =$row[0]; // Valores de cada fila que se insertan en el array de datos que le pasaremos a Open Flash Chart
}



$data_2[0] = $penero[0];
$data_2[1] = $pfebrero[0];
$data_2[2] = $pmarzo[0];
$data_2[3] = $pabril[0];
$data_2[4] = $pmayo[0];
$data_2[5] = $pjunio[0];
$data_2[6] = $pjulio[0];
$data_2[7] = $pagosto[0];
$data_2[8] = $pseptiembre[0];
$data_2[9] = $poctubre[0];
$data_2[10] = $pnoviembre[0];
$data_2[11] = $pdiciembre[0];



mysql_close($db); // Cerramos la conexi�n con la base de datos



include_once( 'open-flash-chart.php' );
$g = new graph();
$g->title( "$langflashfechaexamenes", '{font-size: 20px; color: #736AFF}' );

// we add 3 sets of data:
$g->set_data( $data_1 );
$g->set_data( $data_2 );


// we add the 3 line types and key labels
$g->line_dot( 3, 5, '0xCC3399', "$langflashexamenes", 10);    // <-- 3px thick + dots
$g->line_hollow( 2, 4, '0x80a033', "$langflashpreguntas", 10 );

$g->set_x_labels( array( "$langflashenero","$langflashfebrero","$langflashmarzo","$langflashabril","$langflashmayo","$langflashjunio","$langflashjulio","$langflashagosto","$langflashsep","$langflashoct", "$langflashnov", "$langflashdic" ) );

$g->set_x_label_style( 10, '0x000000', 0, 2 );


$numeromax = max($enero[0], $febrero[0], $marzo[0], $abril[0], $mayo[0], $junio[0], $julio[0], $agosto[0], $septiembre[0], $octubre[0], $noviembre[0], $diciembre[0], $penero[0], $pfebrero[0], $pmarzo[0], $pabril[0], $pmayo[0], $pjunio[0], $pjulio[0], $pagosto[0], $pseptiembre[0], $poctubre[0], $pnoviembre[0], $pdiciembre[0]);


$g->set_y_max( $numeromax + 2 );
$g->y_label_steps( 4 );
$g->set_y_legend( "$langflashnumeroexamenestotal", 12, '#736AFF' );

$g->bg_colour = '#FDFDFD';

echo $g->render();
?>