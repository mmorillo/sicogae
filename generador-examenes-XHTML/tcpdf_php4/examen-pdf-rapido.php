<?

/* Autenticacion */

require 'CHECK/check_login_pdf.php';

require 'CHECK/chequealogin.php';

require('SQL/db_connect-pdf.php');

// Include de la Libreria TCPDF
require_once('config/lang/eng.php');
require_once('tcpdf.php');

// Incluimos la Clase class.crearpdf.php
include('class.crearexamenpdf.php');

//Inicializamos variables

$examen = "Examen";
$asignatura = "";
$grupo = "";
$fecha = "";
$modelo= "";
$evaluacion= "";
$comentario= "";
$curso= "";
$ids="";
$idcheckbox="";
$query="";
$codigobarras="";

/* 
Aqui recogemos los datos de la Base de Datos para mostrarlos
*/

$query="select max(IDPREGUNTA) from PREGUNTA";
$res=mysql_query($query) or die('Error: '.mysql_error()."<BR>Query: $query");
$resultado = mysql_fetch_row($res);

// Recogemos los idpreguntas seleccionados
for ($i = 0; $i <= $resultado[0]; $i++){
	$nombre="checkbox$i";
	$idcheckbox = $_POST["$nombre"];
	if ($idcheckbox != ''){
		if ($ids == ''){
			$ids="$idcheckbox";
		}
		else {
			$ids="$ids,$idcheckbox";
		}
	}
}

$asignatura = $_POST["asignatura"];
$grupo = $_POST["grupo"];
$fecha = $_POST["date"];
$modelo= $_POST["modelo"];
$evaluacion= $_POST["evaluacion"];
$comentario= $_POST["comentario"];
$curso= $_POST["curso"];
$codigobarras=$_POST["codigobarras"];

if ($ids != ''){

	$query="SELECT count(P.TITULO) AS PREGUNTAS FROM PREGUNTA P, EXAMEN E WHERE E.IDEXAMEN=P.IDEXAMEN AND IDPREGUNTA in ($ids)";
	$res=mysql_query($query) or die('Error: '.mysql_error()."<BR>Query: $query");
	$resultado = mysql_fetch_row($res);
	//$numeropreguntas = utf8_decode($resultado[0]);
	$numeropreguntas = $resultado[0];
	
	//Guardamos el titulo de las preguntas para luego procesarlas en un array
	$query="SELECT P.TITULO FROM PREGUNTA P, EXAMEN E WHERE E.IDEXAMEN=P.IDEXAMEN AND IDPREGUNTA in ($ids) ORDER BY P.IDORDER, P.IDPREGUNTA";
	$resultadotitulopreguntas=mysql_query($query) or die('Error: '.mysql_error()."<BR>Query: $query");
	
	//Guardamos el texto de las preguntas para luego procesarlas en un array
	$query="SELECT P.TEXTO FROM PREGUNTA P, EXAMEN E WHERE E.IDEXAMEN=P.IDEXAMEN AND IDPREGUNTA in ($ids) ORDER BY P.IDORDER, P.IDPREGUNTA";
	$resultadopreguntas=mysql_query($query) or die('Error: '.mysql_error()."<BR>Query: $query");
	
	//Guardamos imagen de las preguntas para luego procesarlas en un array
	$query="SELECT P.IMAGEN FROM PREGUNTA P, EXAMEN E WHERE E.IDEXAMEN=P.IDEXAMEN AND IDPREGUNTA in ($ids) ORDER BY P.IDORDER, P.IDPREGUNTA";
	$resultadoimagenes=mysql_query($query) or die('Error: '.mysql_error()."<BR>Query: $query");
}
else {
	$numeropreguntas="";
	$resultadotitulopreguntas="";
	$resultadopreguntas="";
	$resultadoimagenes="";
}

/* 
Fin SQL
*/



// Usamos La Clase class.crearexamenpdf.php
$pdf = new crearpdf("examen.pdf",1);
$pdf->examen($examen,$asignatura,$grupo,$fecha,$modelo,$evaluacion,$comentario,$curso,$numeropreguntas,$codigobarras,$resultadotitulopreguntas,$resultadopreguntas,$resultadoimagenes);

?>
