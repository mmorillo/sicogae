<?php
/************************************************************************
* Software: ELSAM                                                       *
* Version:  0.92                                                         *
* Date:     2006-08-06                                                  *
* Author:   Miguel Morillo Iruela                                       *
* License:  GPL                                                         *
* This program is distributed under the terms and conditions of the GPL *
* See the LICENSE files for details                                     *
************************************************************************/

/* Autenticacion */

require 'CHECK/check_login.php';

require 'CHECK/chequealogin.php';

// login OK

/* Iniciacion variables y constantes de configuracion */
/* Incluimos la cabecera y el menu */

require_once "CONFIG/configuracion.php";

$opcion = $_GET["menu"]; 
$submenu = $_GET["enlace"]; 

/* Pantalla de inicio */

if ($opcion=="inicio")
{
	include "inicio.html";
}

/* Logout */
else if ($opcion=="logout" ){
		include "logout.php";
}

/* Asignaturas */
else if ($opcion=="asignaturas" ){
$idasignatura = $_GET["var"]; 

		if ($submenu=="menu"){
			include "ASIGNATURA/asignaturas.php";
		}
		else if ($submenu=="visualizarexamenpdf"){
			$_REQUEST['var'] = $idexamen;
			include "tcpdf/examen-pdf.php";
		}
}

/* Examenes */
else if ($opcion=="examenes" ){
$idexamen = $_GET["var"]; 

		if ($submenu=="menu"){
			include "EXAMEN/examen.php";
		}
		else if ($submenu=="visualizarexamenpdf"){
			$_REQUEST['var'] = $idexamen;
			include "tcpdf/examen-pdf.php";
		}
		else if ($submenu=="guardarexamenpdf"){
			$_REQUEST['var'] = $idexamen;
			include "tcpdf/guarda-examen-pdf.php";
		}
		else if ($submenu=="verexamenpdf2"){
			include "EXAMEN/visualizar-examenpdf.php";
		}
		else if ($submenu=="respuestaexamenpdf"){
			include "tcpdf/respuesta-examen-pdf.php";
		}
                else if ($submenu=="examenpdfrapido"){
                        include "tcpdf/examen-pdf-rapido.php";
                }
                else if ($submenu=="examenpdfonline"){
                        include "tcpdf/examen-pdf-online.php";
                }

}

/* Preguntas */
else if ($opcion=="preguntas" ){
$idpregunta = $_GET["var"]; 

		if ($submenu=="menu"){
			include "PREGUNTA/pregunta.php";
		}
		else if ($submenu=="visualizarpreguntapdf"){
			$_REQUEST['var'] = $idpregunta;
			//include "tcpdf/mipdf.php";
			//include "PDF/pregunta-pdf.php";
			include "tcpdf/pregunta-pdf.php";
		}
}


/* Vacio */
else{
	include_once "inicio.html";
}


/* Pie */
require_once "pie.php";
?>
