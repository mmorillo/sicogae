<?php
/************************************************************************
* Software: ELSAM                                                       *
* Version:  0.92                                                         *
* Date:     2006-08-06                                                  *
* Author:   Miguel Morillo Iruela                                       *
* License:  GPL                                                         *
* This program is distributed under the terms and conditions of the GPL *
* See the LICENSE files for details                                     *
************************************************************************/

/* Autenticacion */

require 'CHECK/check_login.php';

require 'CHECK/chequealogin.php';

// login OK

/* Iniciacion variables y constantes de configuracion */
/* Incluimos la cabecera y el menu */

require_once "CONFIG/configuracion.php";
require "cabecera.php";


$opcion = $_GET["menu"]; 
$submenu = $_GET["enlace"]; 

/* Pantalla de inicio */

if ($opcion=="inicio")
{
	include "inicio.html";
	include "calendario/php_calendar.php";
	

//Recogemos fechas de examenes MES ACTUAL

$time = time();
$today = date('j',$time);
$mes = date('m', $time);
$ano= date('Y', $time);
$mes2=date( 'm', strtotime( date( "Y-m" ) . '-01 next month' ) );
$ano2= date('Y', $time);

$days = array();

$db = mysql_connect("$BaseDatosServidor", "$BaseDatosUsuario","$BaseDatosClave") or die("Imposible conectar a la base de datos");
mysql_select_db("$BaseDatosNombre", $db) or die("Imposible consultar la base de datos");


$fechaexamenes = mysql_query("select FECHA,IDEXAMEN from EXAMEN where FECHA like ('%/$mes/$ano')",$db) or die("Sentencia SQL incorrecta");
$datosfechaexamenes = array();

while( $row = mysql_fetch_array($fechaexamenes) ){
   $doscifras=substr("$row[FECHA]", 0, 2);
   $cero=substr("$doscifras", 0, 1);
   $unacifra=substr("$doscifras", 1, 1);
   if ($cero==0){
	$cifra=$unacifra;
	}
   else { $cifra=$doscifras; }
   $datosfechaexamenes[0] = $cifra; // Valores de cada fila
   $datosfechaexamenes[1] = $row[IDEXAMEN];
   $days [$datosfechaexamenes[0]]= array("iniciopdf.php?menu=examenes&enlace=visualizarexamenpdf&var=$datosfechaexamenes[1]","linked-day",'<span style="color: blue; font-weight: bold; ">'.$datosfechaexamenes[0].'</span>');
}

$days[$today]=array (NULL,NULL,'<span style="color: red; font-weight: bold;">'.$today.'</span>');


//Recogemos fechas de examenes MES SIGUIENTE


$db = mysql_connect("$BaseDatosServidor", "$BaseDatosUsuario","$BaseDatosClave") or die("Imposible conectar a la base de datos");
mysql_select_db("$BaseDatosNombre", $db) or die("Imposible consultar la base de datos");
$fechaexamenes2 = mysql_query("select FECHA,IDEXAMEN from EXAMEN where FECHA like ('%/$mes2/$ano')",$db) or die("Sentencia SQL incorrecta");

$datosfechaexamenes2 = array();
$days2 = array();

while( $row2 = mysql_fetch_array($fechaexamenes2) ){
   $doscifras2=substr("$row2[FECHA]", 0, 2);
   $cero2=substr("$doscifras2", 0, 1);
   $unacifra2=substr("$doscifras2", 1, 1);
   if ($cero2==0){
	$cifra2=$unacifra2;
	}
   else { $cifra2=$doscifras2; }
   $datosfechaexamenes2[0] = $cifra2; // Valores de cada fila
   $datosfechaexamenes2[1] = $row2[IDEXAMEN];
   $days2 [$datosfechaexamenes2[0]]= array("iniciopdf.php?menu=examenes&enlace=visualizarexamenpdf&var=$datosfechaexamenes2[1]","linked-day",'<span style="color: blue; font-weight: bold;">'.$datosfechaexamenes2[0].'</span>');
}



// IMPRIMIMOS CALENDARIOS

$mescalendario=date('n', $time);
$mescalendario2=$mescalendario+1;
$calendarioano=generate_calendar(date('Y', $time), $mescalendario, $days);
$calendarioano2=generate_calendar(date('Y', $time), $mescalendario2, $days2);

	echo "<center><BR>";

	echo "<table align='center' border='0' cellspacing='1' cellpadding='10'>";
	echo "<tr>";
	echo "<td>$calendarioano</td>";
	echo "<td>$calendarioano2</td>";
	echo "</tr>";
	echo "</table>";

	echo "<BR>";
	
        echo "<APPLET CODE = 'jscicalc/CalcButtonApplet.class' ARCHIVE = '$pathweb/calculadora/jscicalc-2.1.6.jar' WIDTH = '150' HEIGHT = '30'> Calculadora </APPLET>";

	echo "</center><BR>";
	
	echo "<center><BR><BR>";
	include_once "ofc-library/chart.php";
	echo "</center><BR>";
}

/* Logout */
else if ($opcion=="logout" ){
		include "/logout.php";
}

/* Preferencias */
else if ($opcion=="preferencias" ){
		if ($submenu=="menu"){
			include "PREFERENCIAS/preferencias.php";
		}
		else if ($submenu=="usuarios"){
			include "PREFERENCIAS/usuarios.php";
		}
		else if ($submenu=="registro"){
			include "PREFERENCIAS/registro.php";
		}
		else if ($submenu=="registronuevousuario"){
			include "PREFERENCIAS/registro.php";
		}
		else if ($submenu=="actualizausuario"){
			include "PREFERENCIAS/actualizar-usuario.php";
		}
		else if ($submenu=="borrausuario"){
			include "PREFERENCIAS/borrar-usuario.php";
		}
		else if ($submenu=="borrausuariobox"){
			include "PREFERENCIAS/borra-usuario-box.php";
		}
		else if ($submenu=="borrausuariobox2"){
			include "PREFERENCIAS/borra-usuario-box2.php";
		}
		else if ($submenu=="subirlogo"){
			include "PREFERENCIAS/subirlogo.php";
		}
		else if ($submenu=="uploader"){
			include "PREFERENCIAS/uploader.php";
		}
		else if ($submenu=="verlogo"){
			include "PREFERENCIAS/verlogo.php";
		}
		else if ($submenu=="actualizalogo"){
			$_REQUEST['var'] = $archivo;
			include "PREFERENCIAS/actualiza-logo.php";
		}
		else if ($submenu=="borrarlogo"){
			include "PREFERENCIAS/borrar-logo.php";
		}
		else if ($submenu=="borrarlogo2"){
			$_REQUEST['var'] = $archivo;
			include "PREFERENCIAS/borrar-logo2.php";
		}
}


/* Cursos */
else if ($opcion=="cursos" ){
$curso = $_GET["var"]; 

		if ($submenu=="menu"){
			include "CURSO/cursos.php";
		}
		else if ($submenu=="vercurso"){
			include "CURSO/ver-cursos.php";
		}
		else if ($submenu=="detallecurso"){
			$_REQUEST['var'] = $curso;
			include "CURSO/detalle-cursos.php";
		}
		else if ($submenu=="detalleasignatura"){
			$_REQUEST['var'] = $idasignatura;
			include "ASIGNATURA/visualizar-asignatura.php";
		}
		else if ($submenu=="actualizacurso"){
			include "CURSO/actualizar-curso.php";
		}
		else if ($submenu=="modificacurso"){
			include "CURSO/modifica-curso.php";
		}
		else if ($submenu=="updatecurso"){
			include "CURSO/update-curso.php";
		}
		else if ($submenu=="insertacurso"){
			include "CURSO/insertar-curso.php";
		}
		else if ($submenu=="formularioinsertacurso"){
			include "CURSO/formulario-inserta-curso.php";
		}
		else if ($submenu=="borrarcurso"){
			include "CURSO/borrar-curso.php";
		}
		else if ($submenu=="borrarcursobox"){
			include "CURSO/borra-curso-box.php";
		}
		else if ($submenu=="borrarcursobox2"){
			include "CURSO/borra-curso-box2.php";
		}
}

/* Asignaturas */
else if ($opcion=="asignaturas" ){
$asignatura = $_GET["var"]; 

		if ($submenu=="menu"){
			include "ASIGNATURA/asignaturas.php";
		}
		else if ($submenu=="verasignatura"){
			include "ASIGNATURA/verasignaturas.php";
		}
                else if ($submenu=="verasignaturacurso"){
                        include "ASIGNATURA/verasignaturascurso.php";
                }
		else if ($submenu=="detalleasignatura"){
			$_REQUEST['var'] = $idasignatura;
			include "ASIGNATURA/visualizar-asignatura.php";
		}
		else if ($submenu=="visualizarexamen"){
			$_REQUEST['var'] = $examen;
			include "EXAMEN/visualizar-examen.php";
		}
		else if ($submenu=="actualizaasignatura"){
			include "ASIGNATURA/actualizar-asignatura.php";
		}
                else if ($submenu=="actualizaasignaturacurso"){
                        include "ASIGNATURA/actualizar-asignatura-curso.php";
                }
		else if ($submenu=="modificaasignatura"){
			include "ASIGNATURA/modifica-asignatura.php";
		}
		else if ($submenu=="updateasignatura"){
			include "ASIGNATURA/update-asignatura.php";
		}
		else if ($submenu=="insertaasignatura"){
			include "ASIGNATURA/inserta-asignatura.php";
		}
		else if ($submenu=="formularioinsertaasignatura"){
			include "ASIGNATURA/formulario-inserta-asignatura.php";
		}
		else if ($submenu=="borrarasignatura"){
			include "ASIGNATURA/borra-asignatura.php";
		}
		else if ($submenu=="borrarasignaturacurso"){
                        include "ASIGNATURA/borra-asignatura-curso.php";
                }
		else if ($submenu=="borrarasignatura2"){
			include "ASIGNATURA/borra-asignatura2.php";
		}
		else if ($submenu=="borrarasignatura3"){
			include "ASIGNATURA/borra-asignatura3.php";
		}
}

/* Examenes */
else if ($opcion=="examenes" ){
$idexamen = $_GET["var"]; 

		if ($submenu=="menu"){
			include "EXAMEN/examen.php";
		}
		else if ($submenu=="verexamen"){
			include "EXAMEN/verexamen.php";
		}
		else if ($submenu=="verexamenasignatura"){
			include "EXAMEN/verexamenasignatura.php";
		}
		else if ($submenu=="visualizarexamen"){
			$_REQUEST['var'] = $idexamen;
			include "EXAMEN/visualizar-examen.php";
		}
		else if ($submenu=="verexamenpdf"){
			include "EXAMEN/verexamenpdf.php";
		}
		else if ($submenu=="actualizarexamenasignatura"){
			include "EXAMEN/actualizaexamenasignatura.php";
		}
		else if ($submenu=="actualizarexamen"){
			include "EXAMEN/actualizar-examen.php";
		}
		else if ($submenu=="modificaexamen"){
			include "EXAMEN/modifica-examen.php";
		}
		else if ($submenu=="updateexamen"){
			include "EXAMEN/update-examen.php";
		}
		else if ($submenu=="insertaexamen"){
			include "EXAMEN/inserta-examen.php";
		}
		else if ($submenu=="formularioinsertaexamen"){
			include "EXAMEN/formulario-insertar-examen.php";
		}
		else if ($submenu=="borraexamenasignatura"){
			include "EXAMEN/borraexamenasignatura.php";
		}
		else if ($submenu=="borraexamen"){
			include "EXAMEN/borra-examen.php";
		}
		else if ($submenu=="borraexamen2"){
			include "EXAMEN/borra-examen2.php";
		}
		else if ($submenu=="borraexamen3"){
			include "EXAMEN/borra-examen3.php";
		}
		else if ($submenu=="borraexamenpdf"){
			include "EXAMEN/borra-examenpdf.php";
		}
		else if ($submenu=="borraexamenpdf2"){
			include "EXAMEN/borra-examenpdf2.php";
		}
		else if ($submenu=="borraexamenpdf3"){
			include "EXAMEN/borra-examenpdf3.php";
		}
		else if ($submenu=="respuestasexamenpdfasignatura"){
			include "EXAMEN/respuestaexamenasignatura.php";
		}
		else if ($submenu=="respuestasexamenpdf"){
			include "EXAMEN/respuestasexamenpdf.php";
		}
		else if ($submenu=="ordenpreguntasexamenasignatura"){
			include "EXAMEN/ordenexamenasignatura.php";
		}
		else if ($submenu=="ordenpreguntasexamen"){
			include "EXAMEN/ordenpreguntasexamen1.php";
		}
		else if ($submenu=="ordenpreguntasexamen2"){
			include "EXAMEN/ordenpreguntasexamen2.php";
		}
		else if ($submenu=="updateordenpreguntasexamen"){
			include "EXAMEN/ordenpreguntasexamen3.php";
		}
                else if ($submenu=="examenrapido"){
                        include "EXAMEN/verexamenasignaturarapido.php";
                }
                else if ($submenu=="preguntasexamenrapido"){
                        include "EXAMEN/preguntasexamenrapido.php";
                }
                else if ($submenu=="verexamenonline"){
                        include "EXAMEN/verexamenonline.php";
                }
                else if ($submenu=="examenonline"){
                        include "EXAMEN/verexamenasignaturaonline.php";
                }
}

/* Preguntas */
else if ($opcion=="preguntas" ){
$idpregunta = $_GET["var"]; 

		if ($submenu=="menu"){
			include "PREGUNTA/pregunta.php";
		}
		else if ($submenu=="verpreguntaasignatura"){
			include "PREGUNTA/verpreguntaasignatura.php";
		}
		else if ($submenu=="verpregunta"){
			include "PREGUNTA/verpregunta.php";
		}
		else if ($submenu=="visualizarpregunta"){
			$_REQUEST['var'] = $idpregunta;
			include "PREGUNTA/visualizar-pregunta.php";
		}
		else if ($submenu=="actualizarpregunta"){
			include "PREGUNTA/actualizar-pregunta.php";
		}
		else if ($submenu=="actualizarpreguntaasignatura"){
			include "PREGUNTA/actualizapreguntaasignatura.php";
		}
		else if ($submenu=="modificapregunta"){
			include "PREGUNTA/modifica-pregunta.php";
		}
		else if ($submenu=="updatepregunta"){
			include "PREGUNTA/update-pregunta.php";
		}
		else if ($submenu=="insertapregunta"){
			include "PREGUNTA/inserta-pregunta.php";
		}
		else if ($submenu=="formularioinsertapregunta"){
			include "PREGUNTA/formulario-inserta-pregunta.php";
		}
		else if ($submenu=="borrarpreguntaasignatura"){
			include "PREGUNTA/borrapreguntaasignatura.php";
		}
		else if ($submenu=="borrarpregunta"){
			include "PREGUNTA/borra-pregunta.php";
		}
		else if ($submenu=="borrarpregunta2"){
			include "PREGUNTA/borra-pregunta2.php";
		}
		else if ($submenu=="borrarpregunta3"){
			include "PREGUNTA/borra-pregunta3.php";
		}
}

/* Calculadora */
else if ($opcion=="calculadora" ){
		if ($submenu=="ver"){
				include "calculadora/calculadora.php";
		}
}

/* Calendario */
else if ($opcion=="calendario" ){
		if ($submenu=="ver"){
				include "calendario/calendario.php";
		}
}

/* Vacio */
else{
	include_once "inicio.html";


}



/* Pie */
require_once "pie.php";
?>
