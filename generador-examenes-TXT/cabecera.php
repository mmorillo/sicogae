<?php
require("gpl.php");

/************************************************************************
* Software: ELSAM                                                       *
* Version:  0.92                                                         *
* Date:     2006-08-06                                                  *
* Author:   Miguel Morillo Iruela                                       *
* License:  GPL                                                         *
* This program is distributed under the terms and conditions of the GPL *
* See the LICENSE files for details                                     *
************************************************************************/

/************************************************************************
*   Copyright (C) 2006 by Miguel Morillo Iruela                         *
*   elsam.sicogae@gmail.com 				                            *
*                                                                       *
*   This program is free software; you can redistribute it and/or modify*
*   it under the terms of the GNU General Public License as published by*
*   the Free Software Foundation; either version 2 of the License, or   *
*   (at your option) any later version.                                 *
*                                                                       *
*   This program is distributed in the hope that it will be useful,     *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of      *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       *
*   GNU General Public License for more details.                        *
*                                                                       *
*   You should have received a copy of the GNU General Public License   *
*   along with this program; if not, write to the                       *
*   Free Software Foundation, Inc.,                                     *
*   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.           *
*************************************************************************/

require_once "CONFIG/configuracion.php";

require_once("head.php");

echo "<table border='0' cellpadding='6' cellspacing='6' style='border-collapse: separate' width='100%'>";
echo "<tr>";
echo "<td width=\"14%\" valign=\"top\" style='border: 0px'>";

include("menu.php");

echo "</td>";
echo "<td width=\"86%\" valign=\"top\"  style='border: 0px'>";

?>
