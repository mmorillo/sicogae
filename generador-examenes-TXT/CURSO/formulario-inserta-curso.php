<?php
/************************************************************************
* Software: ELSAM                                                       *
* Version:  0.92                                                         *
* Date:     2006-08-06                                                  *
* Author:   Miguel Morillo Iruela                                       *
* License:  GPL                                                         *
* This program is distributed under the terms and conditions of the GPL *
* See the LICENSE files for details                                     *
************************************************************************/

/* Autenticacion */

require 'CHECK/check_login.php';

require 'CHECK/chequealogin.php';

// login OK
?>

<BR><BR><BR><BR><BR>

<?php
require ("funciones-cursos.inc.php");

if ($_POST[curso]=='' )
{
	echo("<IMG SRC='imagenes/peligro.png' NAME='Peligro' ALIGN=MIDDLE BORDER=0><br><br>");
	die("$langerrorcurso2");
}


$miconexion = new DB_mysql;
$miconexion->conectar($BaseDatosNombre, $BaseDatosServidor, $BaseDatosUsuario, $BaseDatosClave);
$miconexion->consulta("INSERT INTO CURSO(NOMBRE,COMENTARIO) VALUES ('$_POST[curso]', '$_POST[comentario]')");

if ($miconexion->Errno>0 )
{
	echo("<IMG SRC='imagenes/peligro.png' NAME='Peligro' ALIGN=MIDDLE BORDER=0><br><br>");
	echo("$langerrorcurso3");echo("<br><br>");
	die("Error: $miconexion->Error");
}

?>

<FONT size=5><?php echo "$langcursoinsertaok"?></FONT>

<?php
/* Pie */
require_once "pie.php";
?>
