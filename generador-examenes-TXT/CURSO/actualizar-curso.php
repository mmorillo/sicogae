<?php
/************************************************************************
* Software: ELSAM                                                       *
* Version:  0.92                                                         *
* Date:     2006-08-06                                                  *
* Author:   Miguel Morillo Iruela                                       *
* License:  GPL                                                         *
* This program is distributed under the terms and conditions of the GPL *
* See the LICENSE files for details                                     *
************************************************************************/

/* Autenticacion */

require 'CHECK/check_login.php';

require 'CHECK/chequealogin.php';

// login OK

?>

<BR><BR><BR>

<center>

<FONT size=5><?php echo "$langcursotitulo3"?></FONT>

<BR><BR><BR>


<form action='inicio.php?menu=cursos&amp;enlace=modificacurso' method='post'>
<select name="curso">
<?
require ("funciones-cursos.inc.php");
$miconexion = new DB_mysql ;
$miconexion->conectar($BaseDatosNombre, $BaseDatosServidor, $BaseDatosUsuario, $BaseDatosClave);
$miconexion->consulta("SELECT NOMBRE FROM CURSO ORDER BY IDCURSO, NOMBRE");
$miconexion->verconsultabox();
?>
</select>

<input type="submit" value="<?php echo "$langcursoactualizaboton"?>" class="button"> 


</form>

</center>

<?php
/* Pie */
require_once "pie.php";
?>
