<?php
/************************************************************************
* Software: ELSAM                                                       *
* Version:  0.92                                                         *
* Date:     2006-08-06                                                  *
* Author:   Miguel Morillo Iruela                                       *
* License:  GPL                                                         *
* This program is distributed under the terms and conditions of the GPL *
* See the LICENSE files for details                                     *
************************************************************************/

/* Autenticacion */

require 'CHECK/check_login.php';

require 'CHECK/chequealogin.php';

// login OK

?>

<BR><BR>

<FORM METHOD="post" ACTION="inicio.php?menu=cursos&amp;enlace=formularioinsertacurso">

<center>
<FONT size=5><?php echo "$langcursotitulo5"?></FONT>
<HR><BR><BR>
</center>

<?php echo "<b>$langcursotitulo6</b>"?>  <input type='text' name='curso' value='' size=55><br><br><br>
<?php echo "<b>$langcursocomentario</b>"?> <BR><textarea rows=10 cols=70 name='comentario'></textarea><br><br>

<BR><BR>
<center>
<input type="submit" class="button" value="<?php echo "$langcursoinsertaboton"?>">
</center>
</FORM>

<?php
/* Pie */
require_once "pie.php";
?>
