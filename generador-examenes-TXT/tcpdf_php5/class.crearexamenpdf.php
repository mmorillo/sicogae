<?
/* Autenticacion */
/*
require 'CHECK/check_login_pdf.php';

require 'CHECK/chequealogin.php';

require('SQL/db_connect-pdf.php');
*/


class crearpdf
{
	var $pdf;	
	var $nombre_archivo;
	var $descargar = FALSE;
	var $patch = K_PATH_MAIN; // Patch Default del TCPDF
	
	// Funciones
	
	// Constructor
	function crearpdf($nombre,$descargar)
	{	
	 	// Instanciamos el Objeto
		$this->pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true);
		// Damos nombre al Archivo
		$this->nombre_archivo=$nombre;
		/*  $descargar TRUE Descargar Archivo
			True  Descargar Archivo
			False Crear Archivo
		*/
		if ($descargar)
		{
			$this->descargar=TRUE;
		}
		else
		{
			$this->descargar=FALSE;
		}
					
	}
	
	// Cambiar Ruta del Archivo
	function ruta($ruta)
	{	
	 	if ( is_dir($ruta) )
	 	{
			$this->patch=$ruta;
			return TRUE;	
		}
		else
		{
			return FALSE;
		}
	}
	
	// Crear el Archivo PDF
	function examen($examen,$asignatura,$grupo,$fecha,$modelo,$evaluacion,$comentario,$curso,$numeropreguntas,$codigobarras,$resultadotitulopreguntas,$resultadopreguntas,$resultadoimagenes)
	{

	// Para poder cargar los ficheros de idiomas y variables
	require 'CONFIG/configuracion.php';


		// $examen debe ser un String
		if ( is_string ($examen) )
		{

			// Info del Documento, estos valores los podemos cambiar en config/tcpdf_config.php
			$this->pdf->SetCreator(PDF_CREATOR);
			$this->pdf->SetAuthor(PDF_AUTHOR);
				$doc_title="$langexamennombre2 $examen";
			$this->pdf->SetTitle($doc_title);
			$this->pdf->SetSubject($doc_subject);
			$this->pdf->SetKeywords($doc_keywords);
			//$this->pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);
			//$this->pdf->SetHeaderData( PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);
			// Seteamos Margenes
			$this->pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
			$this->pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
			$this->pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
			$this->pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
			$this->pdf->setImageScale(PDF_IMAGE_SCALE_RATIO); 
			$this->pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
			$this->pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
			$this->pdf->setLanguageArray($l);

			$this->pdf->setPrintHeader(false);

			// Inciciamos Documento 
			$this->pdf->AliasNbPages();
			$this->pdf->AddPage();


	/* Comienzo de la recuperacion de los datos pasados para formar el archivo PDF */


				//Logo
				$this->pdf->Image("$path/imagenes/logo.jpg",21,17,30);
				$this->pdf->Cell(40,46,"",1);
				
				//Primera linea gris
				$this->pdf->SetX(56);
				$this->pdf->SetFillColor(223);
				$this->pdf->Cell(130,3,"",1,0,L,1);
				$this->pdf->Ln();
					
				//Asignatura
				$this->pdf->SetX(56);
				$this->pdf->SetFont('freesans','B',12);
				$this->pdf->Cell(27,10,"$langpdfasignatura",1,0,R,1);
				$this->pdf->SetFont('freesans','',11);
				$this->pdf->Cell(103,10,"$asignatura",1,0,L,0);
				$this->pdf->Ln();
				
				
				//Curso
				$this->pdf->SetX(56);
				$this->pdf->SetFont('freesans','B',12);
				$this->pdf->Cell(27,10,"$langpdfcurso",1,0,R,1);
				$this->pdf->SetFont('freesans','',11);
				$this->pdf->Cell(67,10,"$curso",1,0,L,0);
				
				//Grupo
				$this->pdf->SetX(150);
				$this->pdf->SetFont('freesans','B',12);
				$this->pdf->Cell(16,10,"$langpdfgrupo",1,0,R,1);
				$this->pdf->SetFont('freesans','',11);
				$this->pdf->Cell(20,10,"$grupo",1,0,L,0);
				$this->pdf->Ln();
				
				
				//Evaluacion
				$this->pdf->SetX(56);
				$this->pdf->SetFont('freesans','B',12);
				$langpdfevaluacionutf8=utf8_decode($langpdfevaluacion);
				//$this->pdf->Cell(27,10,"$langpdfevaluacionutf8",1,0,R,1);
				$this->pdf->Cell(27,10,"$langpdfevaluacion",1,0,R,1);
				$this->pdf->SetFont('freesans','',11);
				$this->pdf->Cell(20,10,"$evaluacion",1,0,L,0);
				
				//Comentario
				$this->pdf->SetX(103);
				$this->pdf->SetFont('freesans','B',12);
				$this->pdf->Cell(27,10,"$langpdfcomentario",1,0,R,1);
				$this->pdf->SetFont('freesans','',11);
				$this->pdf->Cell(56,10,"$comentario",1,0,L,0);
				$this->pdf->Ln();
				
				//Fecha
				$this->pdf->SetX(56);
				$this->pdf->SetFont('freesans','B',12);
				$this->pdf->Cell(27,10,"$langpdffecha",1,0,R,1);
				$this->pdf->SetFont('freesans','',11);
				$this->pdf->Cell(67,10,"$fecha",1,0,L,0);
				
				//Modelo
				$this->pdf->SetX(150);
				$this->pdf->SetFont('freesans','B',12);
				$this->pdf->Cell(18,10,"$langpdfmodelo",1,0,R,1);
				$this->pdf->SetFont('freesans','',11);
				$this->pdf->Cell(18,10,"$modelo",1,0,L,0);
				$this->pdf->Ln();
				
				
				//Segunda linea gris
				//$cursoactual=date("Y");
				//$cursoanterior=$cursoactual-1;
				$this->pdf->SetFont('freesans','',12);
				$this->pdf->SetFillColor(223);
				//$pdf->Cell(170,5," Curso: $cursoanterior/$cursoactual",1,0,L,1);
				$this->pdf->Cell(168,6,"",1,0,L,1);
				$this->pdf->SetFont('freesans','B',11);
				$this->pdf->SetX(148);
				$this->pdf->Cell(38,6,"$langexamencalificacion",1,0,C,0);
				$this->pdf->Ln();
				
				
				//Alumno
				$this->pdf->SetFont('freesans','B',12);
				$this->pdf->Cell(38,10,"$langpdfalumno",1,0,R,0);
				$this->pdf->Cell(92,10,"",1,0,L,0);
				$this->pdf->Cell(38,10,"",1,0,L,0);
				$this->pdf->Ln();
				
				//Tercera linea gris
				//$this->pdf->SetFillColor(223);
				//$this->pdf->Cell(168,2,"",1,0,L,1);
				//$this->pdf->Ln();

			// Codigo de Barra para el Pie de pagina
			if ($codigobarras==null){
				$this->pdf->SetBarcode(date("Y-m-d H:i:s", time()));
			}
			else {
				$this->pdf->SetBarcode("$codigobarras");
			}



			// Escribimos el archivo

			if ($resultadopreguntas != ''){
				//Preguntas
				$this->pdf->Ln(3);
				$this->pdf->SetFont('freesans','BU',10);
				//$this->pdf->Cell(170,10,"$langpdfpreguntas",0,0,L,0);
				$this->pdf->WriteHTML("$langpdfpreguntas", true, 0, true, 0);
				$this->pdf->Ln();
				$i=0;
				$n=1;
				
				
	
				while ($preguntas = mysql_fetch_row($resultadopreguntas) and $titulopreguntas = mysql_fetch_row($resultadotitulopreguntas) and $imagenes= mysql_fetch_row($resultadoimagenes)) {
					$this->pdf->SetFont('freesans','B',10);
					//$titulopregunta=utf8_decode($titulopreguntas[0]);
					$titulopregunta=$titulopreguntas[0];
					//$this->pdf->Cell(170,6,"$langpdfpregunta3 $n.- $titulopregunta",0,0,L,2);
					$this->pdf->WriteHTML("$langpdfpregunta3 $n.- $titulopregunta", true, 0, true, 0);
					$n++;
					$this->pdf->Ln(2);
					
					$this->pdf->SetFont('freesans','',10);
					//$pregunta=utf8_decode($preguntas[0]);
					$pregunta=$preguntas[0];
					if ($pregunta !=''){
						$this->pdf->MultiCell(0,15,"$pregunta",0,L);
						//$this->pdf->WriteHTML($pregunta, true, 0);
						//$this->pdf->WriteHTML($pregunta, true, 0, true, 0);
					}
				$this->pdf->Ln(5);
				}

			
			
				//$this->pdf->Ln();
				$this->pdf->SetFillColor(255);
				//$this->pdf->Cell(170,0,"",1,0,L,1);
				//$this->pdf->WriteHTML("<HR>", true, 0);
				//$this->pdf->Ln();
				//$this->pdf->Cell(170,10,"$langpdfrespuestas",0,0,L,0);
				$this->pdf->WriteHTML("<HR> $langpdfrespuestas", true, 0, true, 0);
			
			}

			
			/* Recogemos el contenido del PDF para insertarlo en BD */
			
			$contenido=$this->pdf->Output('',S);
			$contenido = addslashes($contenido);
			$query="UPDATE EXAMEN SET PDF='$contenido' WHERE NOMBRE='$examen';";
			$res=mysql_query($query) or die("$langerrorpdf ".mysql_error()."<BR>Query: $query");


			// � Creamos o Descargamos ?
			if ($this->descargar)
			{
				$this->pdf->lastPage();
				$this->pdf->Output($this->nombre_archivo);
			}
			else
			{
				$this->pdf->Output($this->nombre_archivo,$this->$patch);
			}
		
			
		}
	}
	


	function examenonline($examen,$asignatura,$grupo,$fecha,$modelo,$evaluacion,$comentario,$curso,$numeropreguntas,$codigobarras,$resultadotitulopreguntas,$resultadopreguntas,$resultadoimagenes)
	{

	// Para poder cargar los ficheros de idiomas y variables
	require 'CONFIG/configuracion.php';


		// $examen debe ser un String
		if ( is_string ($examen) )
		{

			// Info del Documento, estos valores los podemos cambiar en config/tcpdf_config.php
			$this->pdf->SetCreator(PDF_CREATOR);
			$this->pdf->SetAuthor(PDF_AUTHOR);
				$doc_title="$langexamennombre2 $examen";
			$this->pdf->SetTitle($doc_title);
			$this->pdf->SetSubject($doc_subject);
			$this->pdf->SetKeywords($doc_keywords);
			//$this->pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);
			//$this->pdf->SetHeaderData( PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);
			// Seteamos Margenes
			$this->pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
			$this->pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
			$this->pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
			$this->pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
			$this->pdf->setImageScale(PDF_IMAGE_SCALE_RATIO); 
			$this->pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
			$this->pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
			$this->pdf->setLanguageArray($l);

			$this->pdf->setPrintHeader(false);

			// Inciciamos Documento 
			$this->pdf->AliasNbPages();
			$this->pdf->AddPage();


	/* Comienzo de la recuperacion de los datos pasados para formar el archivo PDF */


				//Logo
				$this->pdf->Image("$path/imagenes/logo.jpg",21,17,30);
				$this->pdf->Cell(40,46,"",1);
				
				//Primera linea gris
				$this->pdf->SetX(56);
				$this->pdf->SetFillColor(223);
				$this->pdf->Cell(130,3,"",1,0,L,1);
				$this->pdf->Ln();
					
				//Asignatura
				$this->pdf->SetX(56);
				$this->pdf->SetFont('freesans','B',12);
				$this->pdf->Cell(27,10,"$langpdfasignatura",1,0,R,1);
				$this->pdf->SetFont('freesans','',11);
				$this->pdf->Cell(103,10,"$asignatura",1,0,L,0);
				$this->pdf->Ln();
				
				
				//Curso
				$this->pdf->SetX(56);
				$this->pdf->SetFont('freesans','B',12);
				$this->pdf->Cell(27,10,"$langpdfcurso",1,0,R,1);
				$this->pdf->SetFont('freesans','',11);
				$this->pdf->Cell(67,10,"$curso",1,0,L,0);
				
				//Grupo
				$this->pdf->SetX(150);
				$this->pdf->SetFont('freesans','B',12);
				$this->pdf->Cell(16,10,"$langpdfgrupo",1,0,R,1);
				$this->pdf->SetFont('freesans','',11);
				$this->pdf->Cell(20,10,"$grupo",1,0,L,0);
				$this->pdf->Ln();
				
				
				//Evaluacion
				$this->pdf->SetX(56);
				$this->pdf->SetFont('freesans','B',12);
				$langpdfevaluacionutf8=utf8_decode($langpdfevaluacion);
				//$this->pdf->Cell(27,10,"$langpdfevaluacionutf8",1,0,R,1);
				$this->pdf->Cell(27,10,"$langpdfevaluacion",1,0,R,1);
				$this->pdf->SetFont('freesans','',11);
				$this->pdf->Cell(20,10,"$evaluacion",1,0,L,0);
				
				//Comentario
				$this->pdf->SetX(103);
				$this->pdf->SetFont('freesans','B',12);
				$this->pdf->Cell(27,10,"$langpdfcomentario",1,0,R,1);
				$this->pdf->SetFont('freesans','',11);
				$this->pdf->Cell(56,10,"$comentario",1,0,L,0);
				$this->pdf->Ln();
				
				//Fecha
				$this->pdf->SetX(56);
				$this->pdf->SetFont('freesans','B',12);
				$this->pdf->Cell(27,10,"$langpdffecha",1,0,R,1);
				$this->pdf->SetFont('freesans','',11);
				$this->pdf->Cell(67,10,"$fecha",1,0,L,0);
				
				//Modelo
				$this->pdf->SetX(150);
				$this->pdf->SetFont('freesans','B',12);
				$this->pdf->Cell(18,10,"$langpdfmodelo",1,0,R,1);
				$this->pdf->SetFont('freesans','',11);
				$this->pdf->Cell(18,10,"$modelo",1,0,L,0);
				$this->pdf->Ln();
				
				
				//Segunda linea gris
				//$cursoactual=date("Y");
				//$cursoanterior=$cursoactual-1;
				$this->pdf->SetFont('freesans','',12);
				$this->pdf->SetFillColor(223);
				//$pdf->Cell(170,5," Curso: $cursoanterior/$cursoactual",1,0,L,1);
				$this->pdf->Cell(168,6,"",1,0,L,1);
				$this->pdf->SetFont('freesans','B',11);
				$this->pdf->SetX(148);
				$this->pdf->Cell(38,6,"$langexamencalificacion",1,0,C,0);
				$this->pdf->Ln();
				
				
				//Alumno
				$this->pdf->SetFont('freesans','B',12);
				$this->pdf->Cell(38,10,"$langpdfalumno",1,0,R,0);
				//$this->pdf->Cell(92,10,"",1,0,L,0);
				$this->pdf->TextField('firstname',92,10,array('strokeColor'=>'ltGray'));
				$this->pdf->Cell(38,10,"",1,0,L,0);
				$this->pdf->Ln();
				
				//Tercera linea gris
				//$this->pdf->SetFillColor(223);
				//$this->pdf->Cell(168,2,"",1,0,L,1);
				//$this->pdf->Ln();

			// Codigo de Barra para el Pie de pagina
			if ($codigobarras==null){
				$this->pdf->SetBarcode(date("Y-m-d H:i:s", time()));
			}
			else {
				$this->pdf->SetBarcode("$codigobarras");
			}



			// Escribimos el archivo

			if ($resultadopreguntas != ''){
				//Preguntas
				$this->pdf->Ln(3);
				$this->pdf->SetFont('freesans','BU',10);
				//$this->pdf->Cell(170,10,"$langpdfpreguntas",0,0,L,0);
				$this->pdf->WriteHTML("$langpdfpreguntas", true, 0, true, 0);
				$this->pdf->Ln();
				$i=0;
				
				
	
				while ($preguntas = mysql_fetch_row($resultadopreguntas) and $titulopreguntas = mysql_fetch_row($resultadotitulopreguntas) and $imagenes= mysql_fetch_row($resultadoimagenes)) {
					$this->pdf->SetFont('freesans','B',10);
					//$titulopregunta=utf8_decode($titulopreguntas[0]);
					$titulopregunta=$titulopreguntas[0];
					//$this->pdf->Cell(170,6,"$langpdfpregunta3 $n.- $titulopregunta",0,0,L,2);
					$this->pdf->WriteHTML("$langpdfpregunta3 $n.- $titulopregunta", true, 0, true, 0);
					$this->pdf->Ln(2);
					
					$this->pdf->SetFont('freesans','',10);
					//$pregunta=utf8_decode($preguntas[0]);
					$pregunta=$preguntas[0];
					if ($pregunta !=''){
						$this->pdf->MultiCell(0,15,"$pregunta",0,L);
						//$this->pdf->WriteHTML($pregunta, true, 0);
						//$this->pdf->WriteHTML($pregunta, true, 0, true, 0);
					}
				$this->pdf->Ln(5);
				}

				//$this->pdf->Ln();
				$this->pdf->SetFillColor(255);
				//$this->pdf->Cell(170,0,"",1,0,L,1);
				//$this->pdf->WriteHTML("<HR>", true, 0);
				//$this->pdf->Ln();
				//$this->pdf->Cell(170,10,"$langpdfrespuestas",0,0,L,0);
				//$this->pdf->WriteHTML("<HR> $langpdfrespuestas", true, 0, true, 0);
			
			}
			

			$this->pdf->setUserRights(); // Posibilidad de insertar javascripts y formularios 
			/* Añadimos 3 paginas de respuestas */
			$this->pdf->AddPage();
			$this->pdf->WriteHTML("HOJA DE $langpdfrespuestas 1", true, 0, true, 0);
			$this->pdf->Ln();
			$this->pdf->TextField("hojarespuestas1",170,255,array('multiline'=>true,'strokeColor'=>'ltGray', 'richText'=>true, 'richValue'=>true, 'doNotScroll'=>false));
			
			$this->pdf->AddPage();
			$this->pdf->WriteHTML("HOJA DE $langpdfrespuestas 2", true, 0, true, 0);
			$this->pdf->Ln();
			$this->pdf->TextField("hojarespuestas2",170,255,array('multiline'=>true,'strokeColor'=>'ltGray', 'richText'=>true, 'richValue'=>true, 'doNotScroll'=>false));
			
			$this->pdf->AddPage();
			$this->pdf->WriteHTML("HOJA DE $langpdfrespuestas 3", true, 0, true, 0);
			$this->pdf->Ln();
			$this->pdf->TextField("hojarespuestas3",170,255,array('multiline'=>true,'strokeColor'=>'ltGray', 'richText'=>true, 'richValue'=>true, 'doNotScroll'=>false));

			
			/* Recogemos el contenido del PDF para insertarlo en BD */
			
			$contenido=$this->pdf->Output('',S);
			$contenido = addslashes($contenido);
			$query="UPDATE EXAMEN SET PDF='$contenido' WHERE NOMBRE='$examen';";
			$res=mysql_query($query) or die("$langerrorpdf ".mysql_error()."<BR>Query: $query");


			// � Creamos o Descargamos ?
			if ($this->descargar)
			{
				$this->pdf->lastPage();
				$this->pdf->Output($this->nombre_archivo);
			}
			else
			{
				$this->pdf->Output($this->nombre_archivo,$this->$patch);
			}
		
			
		}
	}


	
// Crear el Archivo PDF
	function respuestaexamen($examen,$asignatura,$grupo,$fecha,$modelo,$evaluacion,$comentario,$curso,$numeropreguntas,$codigobarras,$resultadotitulopreguntas,$resultadopreguntas,$resultadoimagenes)
	{

	// Para poder cargar los ficheros de idiomas y variables
	require 'CONFIG/configuracion.php';


		// $examen debe ser un String
		if ( is_string ($examen) )
		{

			// Info del Documento, estos valores los podemos cambiar en config/tcpdf_config.php
			$this->pdf->SetCreator(PDF_CREATOR);
			$this->pdf->SetAuthor(PDF_AUTHOR);
				$doc_title="$langexamenrespuestas3 $examen";
			$this->pdf->SetTitle($doc_title);
			$this->pdf->SetSubject($doc_subject);
			$this->pdf->SetKeywords($doc_keywords);
			//$this->pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);
			//$this->pdf->SetHeaderData( PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);
			// Seteamos Margenes
			$this->pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
			$this->pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
			$this->pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
			$this->pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
			$this->pdf->setImageScale(PDF_IMAGE_SCALE_RATIO); 
			$this->pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
			$this->pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
			$this->pdf->setLanguageArray($l);

			$this->pdf->setPrintHeader(false);

			// Inciciamos Documento 
			$this->pdf->AliasNbPages();
			$this->pdf->AddPage();

				//Primera linea gris
				//$this->pdf->SetX(20);
				//$this->pdf->SetFillColor(223);
				//$this->pdf->Cell(167,3,"",1,0,L,1);
				//$this->pdf->Ln();
					
				//Asignatura
				$this->pdf->SetX(20);
				$this->pdf->SetFont('freesans','B',12);
				$this->pdf->SetFillColor(223);
				$this->pdf->Cell(47,7,"$langexamenrespuestas3",1,0,C,1);
				$this->pdf->SetFont('freesans','',11);
				$this->pdf->Cell(120,7,"$examen",1,0,L,0);
				$this->pdf->Ln();

			// Codigo de Barra para el Pie de pagina
			if ($codigobarras==null){
				$this->pdf->SetBarcode(date("Y-m-d H:i:s", time()));
			}
			else {
				$this->pdf->SetBarcode("$codigobarras");
			}



			// � Creamos o Descargamos ?
			if ($this->descargar)
			{
				$this->pdf->lastPage();
				$this->pdf->Output($this->nombre_archivo);
			}
			else
			{
				$this->pdf->Output($this->nombre_archivo,$this->$patch);
			}
		
			
		}
	}
	
	
	
	
}



class guardarpdf
{
	var $pdf;	
	var $nombre_archivo;
	var $descargar = FALSE;
	var $patch = K_PATH_MAIN; // Patch Default del TCPDF
	
	// Funciones
	
	// Constructor
	function guardarpdf($nombre,$descargar)
	{	
	 	// Instanciamos el Objeto
		$this->pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true);
		// Damos nombre al Archivo
		$this->nombre_archivo=$nombre;
		/*  $descargar TRUE Descargar Archivo
			True  Descargar Archivo
			False Crear Archivo
		*/
		if ($descargar)
		{
			$this->descargar=TRUE;
		}
		else
		{
			$this->descargar=FALSE;
		}
					
	}
	
	// Cambiar Ruta del Archivo
	function ruta($ruta)
	{	
	 	if ( is_dir($ruta) )
	 	{
			$this->patch=$ruta;
			return TRUE;	
		}
		else
		{
			return FALSE;
		}
	}
	
	
	function guardarexamen($examen,$asignatura,$grupo,$fecha,$modelo,$evaluacion,$comentario,$curso,$numeropreguntas,$codigobarras,$resultadotitulopreguntas,$resultadopreguntas,$resultadoimagenes)
	{

	// Para poder cargar los ficheros de idiomas y variables
	require 'CONFIG/configuracion.php';


		// $string debe ser un String
		if ( is_string ($examen) )
		{

			// Info del Documento, estos valores los podemos cambiar en config/tcpdf_config.php
			$this->pdf->SetCreator(PDF_CREATOR);
			$this->pdf->SetAuthor(PDF_AUTHOR);
				$doc_title="$langexamennombre2 $examen";
			$this->pdf->SetTitle($doc_title);
			$this->pdf->SetSubject($doc_subject);
			$this->pdf->SetKeywords($doc_keywords);
			//$this->pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);
			//$this->pdf->SetHeaderData( PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);
			// Seteamos Margenes
			$this->pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
			$this->pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
			$this->pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
			$this->pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
			$this->pdf->setImageScale(PDF_IMAGE_SCALE_RATIO); 
			$this->pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
			$this->pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
			$this->pdf->setLanguageArray($l);

			$this->pdf->setPrintHeader(false);

			// Inciciamos Documento 
			$this->pdf->AliasNbPages();
			$this->pdf->AddPage();


	/* Comienzo de la recuperacion de los datos pasados para formar el archivo PDF */


				//Logo
				$this->pdf->Image("$path/imagenes/logo.jpg",21,17,30);
				$this->pdf->Cell(40,46,"",1);
				
				//Primera linea gris
				$this->pdf->SetX(56);
				$this->pdf->SetFillColor(223);
				$this->pdf->Cell(130,3,"",1,0,L,1);
				$this->pdf->Ln();
					
				//Asignatura
				$this->pdf->SetX(56);
				$this->pdf->SetFont('freesans','B',12);
				$this->pdf->Cell(27,10,"$langpdfasignatura",1,0,R,1);
				$this->pdf->SetFont('freesans','',11);
				$this->pdf->Cell(103,10,"$asignatura",1,0,L,0);
				$this->pdf->Ln();
				
				
				//Curso
				$this->pdf->SetX(56);
				$this->pdf->SetFont('freesans','B',12);
				$this->pdf->Cell(27,10,"$langpdfcurso",1,0,R,1);
				$this->pdf->SetFont('freesans','',11);
				$this->pdf->Cell(67,10,"$curso",1,0,L,0);
				
				//Grupo
				$this->pdf->SetX(150);
				$this->pdf->SetFont('freesans','B',12);
				$this->pdf->Cell(16,10,"$langpdfgrupo",1,0,R,1);
				$this->pdf->SetFont('freesans','',11);
				$this->pdf->Cell(20,10,"$grupo",1,0,L,0);
				$this->pdf->Ln();
				
				
				//Evaluacion
				$this->pdf->SetX(56);
				$this->pdf->SetFont('freesans','B',12);
				$langpdfevaluacionutf8=utf8_decode($langpdfevaluacion);
				//$this->pdf->Cell(27,10,"$langpdfevaluacionutf8",1,0,R,1);
				$this->pdf->Cell(27,10,"$langpdfevaluacion",1,0,R,1);
				$this->pdf->SetFont('freesans','',11);
				$this->pdf->Cell(20,10,"$evaluacion",1,0,L,0);
				
				//Comentario
				$this->pdf->SetX(103);
				$this->pdf->SetFont('freesans','B',12);
				$this->pdf->Cell(27,10,"$langpdfcomentario",1,0,R,1);
				$this->pdf->SetFont('freesans','',11);
				$this->pdf->Cell(56,10,"$comentario",1,0,L,0);
				$this->pdf->Ln();
				
				//Fecha
				$this->pdf->SetX(56);
				$this->pdf->SetFont('freesans','B',12);
				$this->pdf->Cell(27,10,"$langpdffecha",1,0,R,1);
				$this->pdf->SetFont('freesans','',11);
				$this->pdf->Cell(67,10,"$fecha",1,0,L,0);
				
				//Modelo
				$this->pdf->SetX(150);
				$this->pdf->SetFont('freesans','B',12);
				$this->pdf->Cell(18,10,"$langpdfmodelo",1,0,R,1);
				$this->pdf->SetFont('freesans','',11);
				$this->pdf->Cell(18,10,"$modelo",1,0,L,0);
				$this->pdf->Ln();
				
				
				//Segunda linea gris
				//$cursoactual=date("Y");
				//$cursoanterior=$cursoactual-1;
				$this->pdf->SetFont('freesans','',12);
				$this->pdf->SetFillColor(223);
				//$pdf->Cell(170,5," Curso: $cursoanterior/$cursoactual",1,0,L,1);
				$this->pdf->Cell(168,6,"",1,0,L,1);
				$this->pdf->SetFont('freesans','B',11);
				$this->pdf->SetX(148);
				$this->pdf->Cell(38,6,"$langexamencalificacion",1,0,C,0);
				$this->pdf->Ln();
				
				
				//Alumno
				$this->pdf->SetFont('freesans','B',12);
				$this->pdf->Cell(38,10,"$langpdfalumno",1,0,R,0);
				$this->pdf->Cell(92,10,"",1,0,L,0);
				$this->pdf->Cell(38,10,"",1,0,L,0);
				$this->pdf->Ln();
				
				//Tercera linea gris
				//$this->pdf->SetFillColor(223);
				//$this->pdf->Cell(168,2,"",1,0,L,1);
				//$this->pdf->Ln();


			// Codigo de Barra para el Pie de pagina
			if ($codigobarras==null){
				$this->pdf->SetBarcode(date("Y-m-d H:i:s", time()));
			}
			else {
				$this->pdf->SetBarcode("$codigobarras");
			}




			// Escribimos el archivo

			if ($resultadopreguntas != ''){

				//Preguntas
				$this->pdf->Ln(3);
				$this->pdf->SetFont('freesans','BU',10);
				//$this->pdf->Cell(170,10,"$langpdfpreguntas",0,0,L,0);
				$this->pdf->WriteHTML("$langpdfpreguntas", true, 0, true, 0);
				$this->pdf->Ln();
				$i=0;
				$n=1;
				
				while ($preguntas = mysql_fetch_row($resultadopreguntas) and $titulopreguntas = mysql_fetch_row($resultadotitulopreguntas) and $imagenes= mysql_fetch_row($resultadoimagenes)) {
					$this->pdf->SetFont('freesans','B',10);
					//$titulopregunta=utf8_decode($titulopreguntas[0]);
					$titulopregunta=$titulopreguntas[0];
					//$this->pdf->Cell(170,6,"$langpdfpregunta3 $n.- $titulopregunta",0,0,L,2);
					$this->pdf->WriteHTML("$langpdfpregunta3 $n.- $titulopregunta", true, 0, true, 0);
					$n++;
					$this->pdf->Ln(2);
					
					$this->pdf->SetFont('freesans','',10);
					//$pregunta=utf8_decode($preguntas[0]);
					$pregunta=$preguntas[0];
					if ($pregunta !=''){
						$this->pdf->MultiCell(0,15,"$pregunta",0,L);
						//$this->pdf->WriteHTML($pregunta, true, 0);
						//$this->pdf->WriteHTML($pregunta, true, 0, true, 0);
					}
				$this->pdf->Ln(5);
				}


				//$this->pdf->Ln();
				$this->pdf->SetFillColor(255);
				//$this->pdf->Cell(170,0,"",1,0,L,1);
				//$this->pdf->WriteHTML("<HR>", true, 0);
				//$this->pdf->Ln();
				//$this->pdf->Cell(170,10,"$langpdfrespuestas",0,0,L,0);
				$this->pdf->WriteHTML("<HR> $langpdfrespuestas", true, 0, true, 0);
				
			}
			
			
			
			
			

			/* Recogemos el contenido del PDF para insertarlo en BD */
			
			$fechapdf=(date("d/m/Y H:i:s"));
			$contenido=$this->pdf->Output('',S);
			$contenido = addslashes($contenido);
			$query="INSERT INTO PDF(NOMBRE,PDF,sysdate) VALUES('$examen-$fechapdf','$contenido',now())";
			$res=mysql_query($query) or die("$langerrorpdf ".mysql_error()."<BR>Query: $query");


/*
			// � Creamos o Descargamos ?
			if ($this->descargar)
			{
				$this->pdf->lastPage();
				$this->pdf->Output($this->nombre_archivo);
			}
			else
			{
				$this->pdf->Output($this->nombre_archivo,$this->$patch);
			}
*/		



			
		}
	}

}
	








?>
