<?php
/************************************************************************
* Software: ELSAM                                                       *
* Version:  0.92                                                         *
* Date:     2006-08-06                                                  *
* Author:   Miguel Morillo Iruela                                       *
* License:  GPL                                                         *
* This program is distributed under the terms and conditions of the GPL *
* See the LICENSE files for details                                     *
************************************************************************/

/* Autenticacion */

require 'CHECK/check_login.php';

require 'CHECK/chequealogin.php';

// login OK

?>

<BR><BR>

<?php
require ("funciones-asignaturas.inc.php");

if ($_POST[curso]=='' )
{
   echo("<IMG SRC='imagenes/peligro.png' NAME='Peligro' ALIGN=MIDDLE BORDER=0 alt=''><br><br>");
   die("$langerrorasignatura9");
}

$miconexion = new DB_mysql ;
$miconexion->conectar($BaseDatosNombre, $BaseDatosServidor, $BaseDatosUsuario, $BaseDatosClave);
$miconexion->consulta("SELECT C.IDCURSO FROM CURSO C WHERE C.NOMBRE='".$_POST[curso]."'");
if ($miconexion->Errno>0 )
{
	echo("<IMG SRC='imagenes/peligro.png' NAME='Peligro' ALIGN=MIDDLE BORDER=0 alt=''><br><br>");
	echo("$langerrorasignatura10");echo("<br><br>");
	die("Error: $miconexion->Error");
}

$idcurso=$miconexion->verconsulta0();

if ($idcurso == ''){
	echo("<IMG SRC='imagenes/peligro.png' NAME='Peligro' ALIGN=MIDDLE BORDER=0 alt=''><br><br>");
	echo("$langerrorasignatura11");echo("<br><br>");
	die("$langerrorasignatura12");
}


if ($_POST[asignatura]=='')
{
	echo("<IMG SRC='imagenes/peligro.png' NAME='Peligro' ALIGN=MIDDLE BORDER=0 alt=''><br><br>");
	die("$langerrorasignatura13");
}


echo "<br><br>";

$miconexion = new DB_mysql;
$miconexion->conectar($BaseDatosNombre, $BaseDatosServidor, $BaseDatosUsuario, $BaseDatosClave);

$miconexion->consulta("INSERT INTO ASIGNATURA (IDCURSO,NOMBRE,GRUPO,COMENTARIO) VALUES ('$idcurso', '$_POST[asignatura]',  '$_POST[grupo]', '$_POST[comentario]')");
if ($miconexion->Errno>0 )
{
	echo("<IMG SRC='imagenes/peligro.png' NAME='Peligro' ALIGN=MIDDLE BORDER=0><br><br>");
	echo("$langerrorasignatura14");echo("<br><br>");
	die("Error: $miconexion->Error");
}

?>

<FONT size=5><?php echo "$langasignaturainsertaok"?></FONT>

<?php
/* Pie */
require_once "pie.php";
?>
