<?
/************************************************************************
* Software: ELSAM                                                       *
* Version:  0.92                                                         *
* Date:     2006-08-06                                                  *
* Author:   Miguel Morillo Iruela                                       *
* License:  GPL                                                         *
* This program is distributed under the terms and conditions of the GPL *
* See the LICENSE files for details                                     *
************************************************************************/

/* Autenticacion */

require 'CHECK/check_login.php';

require 'CHECK/chequealogin.php';

// login OK

require ("funciones-examenes.inc.php");
$examen = $_GET["var"]; 

$miconexion = new DB_mysql ;
$miconexion->conectar($BaseDatosNombre, $BaseDatosServidor, $BaseDatosUsuario, $BaseDatosClave);
$miconexion->consulta("SELECT PDF FROM PDF WHERE IDPDF='$examen'");
if ($miconexion->Errno>0 )
{
	echo("<IMG SRC='imagenes/peligro.png' NAME='Peligro' ALIGN=MIDDLE BORDER=0><br><br>");
	echo("$langerrorexamen2");echo("<br><br>");
	die("Error: $miconexion->Error");
}

$pdf=$miconexion->verconsulta0();

header('Content-Type: application/pdf');
header('Content-disposition: inline; filename="examen.pdf"');
echo $pdf; 
?>
