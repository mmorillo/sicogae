<?php
/************************************************************************
* Software: ELSAM                                                       *
* Version:  0.92                                                         *
* Date:     2006-08-06                                                  *
* Author:   Miguel Morillo Iruela                                       *
* License:  GPL                                                         *
* This program is distributed under the terms and conditions of the GPL *
* See the LICENSE files for details                                     *
************************************************************************/

require 'SQL/db_connect.php';

class DB_mysql {
 
 /* variables de conexi�n */
 var $BaseDatos;
 var $Servidor;
 var $Usuario;
 var $Clave;

  
 /* identificador de conexi�n y consulta */
 var $Conexion_ID = 0;
 var $Consulta_ID = 0;

   

 /* n�mero de error y texto error */
 var $Errno = 0;
 var $Error = "";

      
 /* M�todo Constructor: Cada vez que creemos una variable
    de esta clase, se ejecutar� esta funci�n */

function DB_mysql($bd = "", $host = "localhost", $user = "nobody", $pass = "") {
   $this->BaseDatos = $bd;
   $this->Servidor = $host;
   $this->Usuario = $user;
   $this->Clave = $pass;
}

   
 /*Conexi�n a la base de datos*/
function conectar($bd, $host, $user, $pass){

	if ($bd != "") $this->BaseDatos = $bd;
	if ($host != "") $this->Servidor = $host;
	if ($user != "") $this->Usuario = $user;
	if ($pass != "") $this->Clave = $pass;
	 

// Conectamos al servidor
	$this->Conexion_ID = mysql_connect($this->Servidor, $this->Usuario, $this->Clave);
    if (!$this->Conexion_ID) {
       $this->Error = "Ha fallado la conexi�n.";
   	   return 0;
    }

//seleccionamos la base de datos
    if (!@mysql_select_db($this->BaseDatos, $this->Conexion_ID)) {
   	  $this->Error = "Imposible abrir ".$this->BaseDatos ;
	  return 0;
	}

 /* Si hemos tenido �xito conectando devuelve 
   el identificador de la conexi�n, sino devuelve 0 */

    return $this->Conexion_ID;
}

	    
/* Ejecuta un consulta */
function consulta($sql = ""){
	     if ($sql == "") {
		     $this->Error = "No ha especificado una consulta SQL";
		     return 0;
	     }

   //ejecutamos la consulta
         $this->Consulta_ID = @mysql_query($sql, $this->Conexion_ID);
  

       if (!$this->Consulta_ID) {
	       $this->Errno = mysql_errno();
	       $this->Error = mysql_error();
       }
       /* Si hemos tenido �xito en la consulta devuelve 
      el identificador de la conexi�n, sino devuelve 0 */
	  
      return $this->Consulta_ID;
}

	        

/* Devuelve el n�mero de campos de una consulta */
function numcampos() {
	return mysql_num_fields($this->Consulta_ID);
}

		 
/* Devuelve el n�mero de registros de una consulta */
function numregistros(){
	return mysql_num_rows($this->Consulta_ID);
}

	  
/* Devuelve el nombre de un campo de una consulta */
function nombrecampo($numcampo) {
	return mysql_field_name($this->Consulta_ID, $numcampo);
}

		   
function verconsulta0() {
	$row=mysql_fetch_row($this->Consulta_ID);
	return $row[0];
}

function vertabla() {	
	echo "<font size=2>\n";
	echo "<table border=1 CELLPADDING=10 CELLSPACING=0>\n";
	// mostramos los nombres de los campos
	for ($i = 0; $i < $this->numcampos(); $i++){
	   echo "<td><b><center>".$this->nombrecampo($i)."</center></b></td>\n";
	}

	// mostrarmos los registros
	while ($row = mysql_fetch_row($this->Consulta_ID)) {
	   echo "<tr> \n";
	   for ($i = 0; $i < $this->numcampos(); $i++){
	   echo "<td>".$row[$i]."</td>\n";
	   }
    echo "</tr>\n";
	}
	echo "</table>";
}



function vertablalinkasignaturainicio() {
	    	
	//echo "<font size=2>\n";
	echo "<ul>";
	while ($row = mysql_fetch_row($this->Consulta_ID)) {
	   for ($i = 0; $i < $this->numcampos(); $i++){
	   		$consultanombreasignatura="SELECT NOMBRE FROM ASIGNATURA WHERE IDASIGNATURA='$row[$i]'";
			$consultanumeroexamenes="SELECT IDEXAMEN FROM EXAMEN WHERE IDASIGNATURA='$row[$i]'";
			$res=mysql_query($consultanombreasignatura) or die('Error: '.mysql_error()."<BR>Query: $consultanombreasignatura");
			$nombreasignatura = mysql_fetch_row($res);
			
			$res2=mysql_query($consultanumeroexamenes) or die('Error: '.mysql_error()."<BR>Query: $consultanumeroexamenes");
			$numeroexamenes = mysql_num_rows($res2);

			$consultaexamen="SELECT NOMBRE FROM EXAMEN WHERE IDASIGNATURA='$row[$i]'";
			$res3=mysql_query($consultaexamen) or die('Error: '.mysql_error()."<BR>Query: $consultaexamen");

	   		echo "<br><li><a href='inicio.php?menu=examenes&amp;enlace=verexamen&amp;var=".nl2br($row[$i])."'>".nl2br($nombreasignatura[0])." ($numeroexamenes)"."</a><br>\n";

			while ($asignaturaexamen = mysql_fetch_row($res3)){
				for ($j = 0; $j < $this->numcampos(); $j++){
					echo "$asignaturaexamen[$j]\n";
				}
			echo "<br>\n";
			}
	   }
	}
	echo "</ul>\n";
	
}



function vertablalinkasignaturainicio2() {
	    	
	//echo "<font size=2>\n";
	echo "<ul>\n";
	while ($row = mysql_fetch_row($this->Consulta_ID)) {
	   for ($i = 0; $i < $this->numcampos(); $i++){
	   		$consultanombreasignatura="SELECT NOMBRE FROM ASIGNATURA WHERE IDASIGNATURA='$row[$i]'";
			$consultanumeroexamenes="SELECT IDEXAMEN FROM EXAMEN WHERE IDASIGNATURA='$row[$i]'";
			$res=mysql_query($consultanombreasignatura) or die('Error: '.mysql_error()."<BR>Query: $consultanombreasignatura");
			$nombreasignatura = mysql_fetch_row($res);
			
			$res2=mysql_query($consultanumeroexamenes) or die('Error: '.mysql_error()."<BR>Query: $consultanumeroexamenes");
			$numeroexamenes = mysql_num_rows($res2);

			$consultaexamen="SELECT NOMBRE FROM EXAMEN WHERE IDASIGNATURA='$row[$i]'";
			$res3=mysql_query($consultaexamen) or die('Error: '.mysql_error()."<BR>Query: $consultaexamen");

	   		echo "<br><li><a href='inicio.php?menu=examenes&amp;enlace=actualizarexamen&amp;var=".nl2br($row[$i])."'>".nl2br($nombreasignatura[0])." ($numeroexamenes)"."</a><br>\n";

			while ($asignaturaexamen = mysql_fetch_row($res3)){
				for ($j = 0; $j < $this->numcampos(); $j++){
					echo "$asignaturaexamen[$j]\n";
				}
			echo "<br>\n";
			}

	   }
	}
	echo "</ul>\n";
	
}

function vertablalinkasignaturainicio3() {
	    	
	//echo "<font size=2>\n";
	echo "<ul>\n";
	while ($row = mysql_fetch_row($this->Consulta_ID)) {
	   for ($i = 0; $i < $this->numcampos(); $i++){
	   		$consultanombreasignatura="SELECT NOMBRE FROM ASIGNATURA WHERE IDASIGNATURA='$row[$i]'";
			$consultanumeroexamenes="SELECT IDEXAMEN FROM EXAMEN WHERE IDASIGNATURA='$row[$i]'";
			$res=mysql_query($consultanombreasignatura) or die('Error: '.mysql_error()."<BR>Query: $consultanombreasignatura");
			$nombreasignatura = mysql_fetch_row($res);
			
			$res2=mysql_query($consultanumeroexamenes) or die('Error: '.mysql_error()."<BR>Query: $consultanumeroexamenes");
			$numeroexamenes = mysql_num_rows($res2);

			$consultaexamen="SELECT NOMBRE FROM EXAMEN WHERE IDASIGNATURA='$row[$i]'";
			$res3=mysql_query($consultaexamen) or die('Error: '.mysql_error()."<BR>Query: $consultaexamen");

	   		echo "<br><li><a href='inicio.php?menu=examenes&amp;enlace=borraexamen&amp;var=".nl2br($row[$i])."'>".nl2br($nombreasignatura[0])." ($numeroexamenes)"."</a><br>\n";

			while ($asignaturaexamen = mysql_fetch_row($res3)){
				for ($j = 0; $j < $this->numcampos(); $j++){
					echo "$asignaturaexamen[$j]\n";
				}
			echo "<br>\n";
			}

	   }
	}
	echo "</ul>\n";
	
}

function vertablalinkasignaturainicio4() {
	    	
	//echo "<font size=2>\n";
	echo "<ul>\n";
	while ($row = mysql_fetch_row($this->Consulta_ID)) {
	   for ($i = 0; $i < $this->numcampos(); $i++){
	   		$consultanombreasignatura="SELECT NOMBRE FROM ASIGNATURA WHERE IDASIGNATURA='$row[$i]'";
			$consultanumeroexamenes="SELECT IDEXAMEN FROM EXAMEN WHERE IDASIGNATURA='$row[$i]'";
			$res=mysql_query($consultanombreasignatura) or die('Error: '.mysql_error()."<BR>Query: $consultanombreasignatura");
			$nombreasignatura = mysql_fetch_row($res);
			
			$res2=mysql_query($consultanumeroexamenes) or die('Error: '.mysql_error()."<BR>Query: $consultanumeroexamenes");
			$numeroexamenes = mysql_num_rows($res2);

			$consultaexamen="SELECT NOMBRE FROM EXAMEN WHERE IDASIGNATURA='$row[$i]'";
			$res3=mysql_query($consultaexamen) or die('Error: '.mysql_error()."<BR>Query: $consultaexamen");

	   		echo "<br><li><a href='inicio.php?menu=examenes&amp;enlace=respuestasexamenpdf&amp;var=".nl2br($row[$i])."'>".nl2br($nombreasignatura[0])." ($numeroexamenes)"."</a><br>\n";

			while ($asignaturaexamen = mysql_fetch_row($res3)){
				for ($j = 0; $j < $this->numcampos(); $j++){
					echo "$asignaturaexamen[$j]\n";
				}
			echo "<br>\n";
			}

	   }
	}
	echo "</ul>\n";
	
}

function vertablalinkasignaturainicio5() {
	    	
	//echo "<font size=2>\n";
	echo "<ul>\n";
	while ($row = mysql_fetch_row($this->Consulta_ID)) {
	   for ($i = 0; $i < $this->numcampos(); $i++){
	   		$consultanombreasignatura="SELECT NOMBRE FROM ASIGNATURA WHERE IDASIGNATURA='$row[$i]'";
			$consultanumeroexamenes="SELECT IDEXAMEN FROM EXAMEN WHERE IDASIGNATURA='$row[$i]'";
			$res=mysql_query($consultanombreasignatura) or die('Error: '.mysql_error()."<BR>Query: $consultanombreasignatura");
			$nombreasignatura = mysql_fetch_row($res);
			
			$res2=mysql_query($consultanumeroexamenes) or die('Error: '.mysql_error()."<BR>Query: $consultanumeroexamenes");
			$numeroexamenes = mysql_num_rows($res2);

			$consultaexamen="SELECT NOMBRE FROM EXAMEN WHERE IDASIGNATURA='$row[$i]'";
			$res3=mysql_query($consultaexamen) or die('Error: '.mysql_error()."<BR>Query: $consultaexamen");

	   		echo "<br><li><a href='inicio.php?menu=examenes&amp;enlace=ordenpreguntasexamen&amp;var=".nl2br($row[$i])."'>".nl2br($nombreasignatura[0])." ($numeroexamenes)"."</a><br>\n";

			while ($asignaturaexamen = mysql_fetch_row($res3)){
				for ($j = 0; $j < $this->numcampos(); $j++){
					echo "$asignaturaexamen[$j]\n";
				}
			echo "<br>\n";
			}

	   }
	}
	echo "</ul>\n";
	
}



function vertablalinkasignaturainiciorapido() {

       echo "<ul>\n";
	while ($row = mysql_fetch_row($this->Consulta_ID)) {
	   for ($i = 0; $i < $this->numcampos(); $i++){
	   		$consultanombreasignatura="SELECT NOMBRE FROM ASIGNATURA WHERE IDASIGNATURA='$row[$i]'";
			$consultanumeroexamenes="SELECT IDEXAMEN FROM EXAMEN WHERE IDASIGNATURA='$row[$i]'";
			$res=mysql_query($consultanombreasignatura) or die('Error: '.mysql_error()."<BR>Query: $consultanombreasignatura");
			$nombreasignatura = mysql_fetch_row($res);
			
			$res2=mysql_query($consultanumeroexamenes) or die('Error: '.mysql_error()."<BR>Query: $consultanumeroexamenes");
			$numeroexamenes = mysql_num_rows($res2);

			$consultaexamen="SELECT distinct E.NOMBRE FROM EXAMEN E, ASIGNATURA A WHERE E.IDASIGNATURA=A.IDASIGNATURA AND E.IDASIGNATURA='$row[$i]'";
			$res3=mysql_query($consultaexamen) or die('Error: '.mysql_error()."<BR>Query: $consultaexamen");

	   		echo "<br><li><a href='inicio.php?menu=examenes&amp;enlace=preguntasexamenrapido&amp;var=".nl2br($row[$i])."'>".nl2br($nombreasignatura[0])." ($numeroexamenes)"."</a><br>\n";

			while ($asignaturaexamen = mysql_fetch_row($res3)){
				for ($j = 0; $j < $this->numcampos(); $j++){
					echo "$asignaturaexamen[$j]\n";
				}
			echo "<br>\n";
			}

	   }
	}
	echo "</ul>\n";

}

function verconsultacheckboxexamenrapido() {
	while ($row = mysql_fetch_row($this->Consulta_ID)) {

		for ($i = 0; $i < $this->numcampos(); $i++){
	   		$consultanombreexamen="SELECT NOMBRE FROM EXAMEN WHERE IDEXAMEN='$row[$i]'";
			$res=mysql_query($consultanombreexamen) or die('Error: '.mysql_error()."<BR>Query: $consultanombreexamen");
			$nombreexamen = mysql_fetch_row($res);
			$consultaidpregunta="SELECT IDPREGUNTA FROM PREGUNTA WHERE IDEXAMEN='$row[$i]'";
			$res2=mysql_query($consultaidpregunta) or die('Error: '.mysql_error()."<BR>Query: $consultaidpregunta");
			$consultanombrepregunta="SELECT distinct P.TITULO FROM PREGUNTA P, EXAMEN E WHERE E.IDEXAMEN=P.IDEXAMEN AND E.IDEXAMEN='$row[$i]'";
			$res3=mysql_query($consultanombrepregunta) or die('Error: '.mysql_error()."<BR>Query: $consultanombrepregunta");

			echo "<b><br><li>$nombreexamen[0]<br></b>\n";

	   		while ($preguntaexamen = mysql_fetch_row($res3) and $idpreguntaexamen = mysql_fetch_row($res2) ){
				for ($j = 0; $j < $this->numcampos(); $j++){
					$preguntatitulo=$preguntaexamen[$j];
					$idpreguntatitulo=$idpreguntaexamen[$j];
					$nombre="checkbox$idpreguntatitulo";
					echo "<input type='checkbox' name=$nombre value='$idpreguntatitulo'>".$preguntatitulo."\n";
				}
			echo "<br>\n";
			}
	   }

	}
}



function vertablalinkasignaturainicioonline() {

       echo "<ul>\n";
	while ($row = mysql_fetch_row($this->Consulta_ID)) {
	   for ($i = 0; $i < $this->numcampos(); $i++){
	   		$consultanombreasignatura="SELECT NOMBRE FROM ASIGNATURA WHERE IDASIGNATURA='$row[$i]'";
			$consultanumeroexamenes="SELECT IDEXAMEN FROM EXAMEN WHERE IDASIGNATURA='$row[$i]'";
			$res=mysql_query($consultanombreasignatura) or die('Error: '.mysql_error()."<BR>Query: $consultanombreasignatura");
			$nombreasignatura = mysql_fetch_row($res);
			
			$res2=mysql_query($consultanumeroexamenes) or die('Error: '.mysql_error()."<BR>Query: $consultanumeroexamenes");
			$numeroexamenes = mysql_num_rows($res2);

			$consultaexamen="SELECT distinct E.NOMBRE FROM EXAMEN E, ASIGNATURA A WHERE E.IDASIGNATURA=A.IDASIGNATURA AND E.IDASIGNATURA='$row[$i]'";
			$res3=mysql_query($consultaexamen) or die('Error: '.mysql_error()."<BR>Query: $consultaexamen");

	   		echo "<br><li><a href='inicio.php?menu=examenes&amp;enlace=verexamenonline&amp;var=".nl2br($row[$i])."'>".nl2br($nombreasignatura[0])." ($numeroexamenes)"."</a><br>\n";

			while ($asignaturaexamen = mysql_fetch_row($res3)){
				for ($j = 0; $j < $this->numcampos(); $j++){
					echo "$asignaturaexamen[$j]\n";
				}
			echo "<br>\n";
			}

	   }
	}
	echo "</ul>\n";

}


function vertablalinkexamenonline() {
        echo "<ul>\n";
        // mostrarmos los registros
        while ($row = mysql_fetch_row($this->Consulta_ID)) {
           for ($i = 0; $i < $this->numcampos(); $i++){
                        $consultanombreexamen="SELECT NOMBRE FROM EXAMEN WHERE IDEXAMEN='$row[$i]'";
                        $res=mysql_query($consultanombreexamen) or die('Error: '.mysql_error()."<BR>Query: $consultanombreexamen");
                        $nombreexamen = mysql_fetch_row($res);
			echo "<li><a href='inicio.php?menu=examenes&amp;enlace=visualizarexamen&amp;var=".nl2br($row[$i])."'>".nl2br($nombreexamen[0])."</a> &nbsp; &nbsp; &nbsp; &nbsp;<a href='iniciopdf.php?menu=examenes&amp;enlace=examenpdfonline&amp;var=".nl2br($row[$i])."'><IMG SRC='imagenes/pdf.png' NAME='Generar Examen PDF' ALIGN=MIDDLE BORDER=0 alt=''></a>\n";
                    echo "<br><br>";
           }
        }
        echo "</ul>\n";
}


function vertablalinkexameninicio() {
	echo "<ol>\n";
	// mostrarmos los registros
	while ($row = mysql_fetch_row($this->Consulta_ID)) {
	   for ($i = 0; $i < $this->numcampos(); $i++){
	   	   	$consultanombreexamen="SELECT NOMBRE FROM EXAMEN WHERE IDEXAMEN='$row[$i]'";
			$res=mysql_query($consultanombreexamen) or die('Error: '.mysql_error()."<BR>Query: $consultanombreexamen");
			$nombreexamen = mysql_fetch_row($res);
	   		echo "<li><a href='inicio.php?menu=examenes&amp;enlace=visualizarexamen&amp;var=".nl2br($row[$i])."'>".nl2br($nombreexamen[0])."</a> &nbsp; &nbsp; &nbsp; &nbsp;<a href='iniciopdf.php?menu=examenes&amp;enlace=visualizarexamenpdf&amp;var=".nl2br($row[$i])."'><IMG SRC='imagenes/pdf.png' NAME='Generar Examen PDF' ALIGN=MIDDLE BORDER=0 alt=''></a> &nbsp; &nbsp; &nbsp; &nbsp;<a href='iniciopdf.php?menu=examenes&amp;enlace=guardarexamenpdf&amp;var=".nl2br($row[$i])."'><IMG SRC='imagenes/guardar.png' NAME='Guardar examen en BD' ALIGN=MIDDLE BORDER=0 alt=''></a>\n";
	   	    echo "<br><br>";
	   }
	}
	echo "</ol>\n";
}

function vertablalinkexamenorden() {
	echo "<ol>\n";
	// mostrarmos los registros
	while ($row = mysql_fetch_row($this->Consulta_ID)) {
	   for ($i = 0; $i < $this->numcampos(); $i++){
	   	   	$consultanombreexamen="SELECT NOMBRE FROM EXAMEN WHERE IDEXAMEN='$row[$i]'";
			$res=mysql_query($consultanombreexamen) or die('Error: '.mysql_error()."<BR>Query: $consultanombreexamen");
			$nombreexamen = mysql_fetch_row($res);
	   		echo "<li><a href='inicio.php?menu=examenes&amp;enlace=ordenpreguntasexamen2&amp;var=".nl2br($row[$i])."'>".nl2br($nombreexamen[0])."</a>\n";
	   	    echo "<br><br>";
	   }
	}
	echo "</ol>\n";
}


function vertablalinkexamenrespuesta() {
	
	echo "<ol>\n";
	// mostrarmos los registros
	while ($row = mysql_fetch_row($this->Consulta_ID)) {
	   for ($i = 0; $i < $this->numcampos(); $i++){
	   	   	$consultanombreexamen="SELECT NOMBRE FROM EXAMEN WHERE IDEXAMEN='$row[$i]'";
			$res=mysql_query($consultanombreexamen) or die('Error: '.mysql_error()."<BR>Query: $consultanombreexamen");
			$nombreexamen = mysql_fetch_row($res);
	   		echo "<li><b>".nl2br($nombreexamen[0])."</b> &nbsp; &nbsp; &nbsp; &nbsp;<a href='iniciopdf.php?menu=examenes&amp;enlace=respuestaexamenpdf&amp;var=".nl2br($row[$i])."'><IMG SRC='imagenes/pdf.png' NAME='Generar respuesta PDF' ALIGN=MIDDLE BORDER=0 alt=''></a>\n";
	   	    echo "<br><br>";
	   }
	}
	echo "</ol>\n";
}


function vertablalinkexamenpdf() {

	echo "<ol>\n";
	// mostrarmos los registros
	while ($row = mysql_fetch_row($this->Consulta_ID)) {
	   for ($i = 0; $i < $this->numcampos(); $i++){
		$consultanombreexamenpdf="SELECT NOMBRE FROM PDF WHERE IDPDF='$row[$i]'";
		$res=mysql_query($consultanombreexamenpdf) or die('Error: '.mysql_error()."<BR>Query: $consultanombreexamenpdf");
		$nombreexamenpdf = mysql_fetch_row($res);
	   	echo "<li><a href='iniciopdf.php?menu=examenes&amp;enlace=verexamenpdf2&amp;var=".nl2br($row[$i])."'>".nl2br($nombreexamenpdf[0])."</a>\n";
	   }
	}
	echo "</ol>\n";
}



function verconsultabox() {   
	while ($row = mysql_fetch_row($this->Consulta_ID)) {
		echo "<Option>".$row[0]."</Option>\n";
	}
}


function verconsultaradio() {
	while ($row = mysql_fetch_row($this->Consulta_ID)) {
		echo "<input type='radio' name=nombre value='$row[0]'>".$row[0]."<BR>\n";
	}
}


function verconsultatabla() {
    echo "<table class='formato' border=0 CELLPADDING=10 CELLSPACING=0>\n";
    echo "<font size=2>\n";
    // mostramos los nombres de los campos
    for ($i = 0; $i < $this->numcampos(); $i++){
	     echo "<td class='formatotd'><center><b>".$this->nombrecampo($i)."</b></center></td>\n";
    }
    echo "</tr>\n";
    // mostrarmos los registros
    while ($row = mysql_fetch_row($this->Consulta_ID)) {
	     echo "<tr> \n";
	     for ($i = 0; $i < $this->numcampos(); $i++){
		      echo "<td class='formatotd'><center>".$row[$i]."</center></td>\n";
	     }
	     echo "</tr>\n";
     }
	 echo "</table>";
}



function verconsultaexamen() {    
 
    while ($row = mysql_fetch_row($this->Consulta_ID)) {
	     for ($i = 0; $i < $this->numcampos(); $i++){
			  echo "<b>".$this->nombrecampo($i).": </b>";
			  echo " ".nl2br($row[$i])."";
			  echo "<br>";
	     }
     }
}

function verpreguntas() { 
	//Guardamos el titulo de las preguntas para luego procesarlas en un array
	$query="SELECT P.TITULO FROM PREGUNTA P, EXAMEN E WHERE E.NOMBRE='$examen' AND E.IDEXAMEN=P.IDEXAMEN";
	$resultadotitulopreguntas=mysql_query($query) or die('Error: '.mysql_error()."<BR>Query: $query");

	//Guardamos el texto de las preguntas para luego procesarlas en un array
	$query="SELECT P.TEXTO FROM PREGUNTA P, EXAMEN E WHERE E.NOMBRE='$examen' AND E.IDEXAMEN=P.IDEXAMEN";
	$resultadopreguntas=mysql_query($query) or die('Error: '.mysql_error()."<BR>Query: $query");

	$i=1;
    echo "<font size=3>\n";
    while ($preguntas = mysql_fetch_row($resultadopreguntas) and $titulopreguntas = mysql_fetch_row($resultadotitulopreguntas)) {
		$titulopregunta=$titulopreguntas[0];
		echo "<b>Pregunta $i.- $titulopregunta</b><br>";
		echo " ".nl2br($row[0])."";
		echo "<br>";
		$i++;
     }
}


function updateexamen() {
require "CONFIG/configuracion.php";

    $reg=array("IDEXAMEN: ", "IDASIGNATURA: ", "EXAMEN: ", "COMENTARIOS: ", "FECHA: ", "MODELO: ", "EVALUACION: ", );
    $m=0;
    while ($row = mysql_fetch_row($this->Consulta_ID)) {
		for ($i = 0; $i < $this->numcampos(); $i++){
			$reg[$m]=$row[$i];
			$m++;
		}
	}

	echo "<FORM METHOD='post' ACTION='inicio.php?menu=examenes&amp;enlace=updateexamen'>";
	
	echo "<b>$langexamenasignatura:</b> ";
	echo "<select name='asignatura'>";

	$consultanombreasignatura="SELECT NOMBRE FROM ASIGNATURA WHERE IDASIGNATURA='$reg[1]'";
	$res=mysql_query($consultanombreasignatura) or die('Error: '.mysql_error()."<BR>Query: $consultanombreasignatura");
	$nombreasignatura = mysql_fetch_row($res);
			
	$consultaasignatura="SELECT NOMBRE FROM ASIGNATURA ORDER BY NOMBRE";
	$res=mysql_query($consultaasignatura) or die('Error: '.mysql_error()."<BR>Query: $consultaasignatura");
		while ($rowasignatura = mysql_fetch_row($res)) {
		if ($rowasignatura[0]==$nombreasignatura[0]) {
			echo "<Option selected='selected'>".$rowasignatura[0]."</Option>\n";
		}
		else{
			echo "<Option>".$rowasignatura[0]."</Option>\n";
		}				
	}

	echo "</select><br><br>";

	echo "<input type=hidden name='idexamen' value='$reg[0]'<br>";
	echo "<input type=hidden name='idasignatura' value='$reg[1]'<br>";
	echo "<b>$langexamennombre2:</b> <input type='text' name='nombre' value='$reg[2]' size=55><br><br>";
	echo "<b>$langexamencomentario:</b> <input type='text' name='comentario' value='$reg[3]' size=55><br><br>";
	echo "<b>$langexamenfecha (dd/mm/yyyy):</b> <input type='text' name='fecha' value='$reg[4]' size=25><br><br>";
	echo "<b>$langexamenmodelo:</b> <input type='text' name='modelo' value='$reg[5]' size=15><br><br>";
	echo "<b>$langexamenevaluacion:</b> <input type='text' name='evaluacion' value='$reg[6]' size=15><br><br>";
	echo "<b>$langexamencodigobarras:</b> <input type='text' name='codigobarras' value='$reg[7]' size=15><b>$langexamencodigobarras2</b><br><br>";
	echo "<BR><BR>";
	echo "<center>";
	echo "<input type='submit' class='button' value='$langexamenmodificaboton2 $reg[2]'>";
	echo "</center>";
	echo "</FORM> ";

		    
}


function ordenpreguntasexamen() {
require "CONFIG/configuracion.php";


// Recogemos en un array los idpregunta pasados
    	$reg=array( "IDPREGUNTA: " );
    	$m=0;
    	while ($row = mysql_fetch_row($this->Consulta_ID)) {
		for ($i = 0; $i < $this->numcampos(); $i++){
			$reg[$m]=$row[$i];
			$m++;
		}
	}

echo "<table border=0 CELLPADDING=6 CELLSPACING=0>\n";
	for ($i = 0; $i < $m; $i++){
		$consultanombrepregunta="SELECT TITULO FROM PREGUNTA WHERE IDPREGUNTA='$reg[$i]'";
		$res=mysql_query($consultanombrepregunta) or die('Error: '.mysql_error()."<BR>Query: $consultanombrepregunta");
		$nombrepregunta = mysql_fetch_row($res);
		$consultaordenpregunta="SELECT IDORDER FROM PREGUNTA WHERE IDPREGUNTA='$reg[$i]'";
		$res2=mysql_query($consultaordenpregunta) or die('Error: '.mysql_error()."<BR>Query: $consultaordenpregunta");
		$ordenpregunta = mysql_fetch_row($res2);

		echo "<FORM METHOD='post' ACTION='inicio.php?menu=examenes&amp;enlace=updateordenpreguntasexamen'>";

		echo "<tr>\n";
		echo "<td><input type=hidden name='idpregunta' value='$reg[$i]'> </td>";
		echo "<td> <b>$nombrepregunta[0]: </b> </td><td><input type='text' name='orden' value='$ordenpregunta[0]' size=3> &nbsp; &nbsp; &nbsp; <input type='submit' value='$langexamenorden3' class='button'></td>";
		echo "</tr>\n";


		echo "</FORM> ";
	}
echo "</table>";

}


} //fin de la Clse DB_mysql
?>
