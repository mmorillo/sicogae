<?php
/************************************************************************
* Software: ELSAM                                                       *
* Version:  0.92                                                         *
* Date:     2006-08-06                                                  *
* Author:   Miguel Morillo Iruela                                       *
* License:  GPL                                                         *
* This program is distributed under the terms and conditions of the GPL *
* See the LICENSE files for details                                     *
************************************************************************/

/* Autenticacion */

require 'CHECK/check_login.php';

require 'CHECK/chequealogin.php';

// login OK

?>


<?
require ("funciones-examenes.inc.php");
$idexamen = $_GET["var"]; 

echo "<br><br>";
echo "<center>";
echo "<FONT size=5>$langexamentitulo2</FONT>";
echo "<hr><br>";
echo "</center>";

echo "<br>";
$miconexion = new DB_mysql ;
$miconexion->conectar($BaseDatosNombre, $BaseDatosServidor, $BaseDatosUsuario, $BaseDatosClave);
$miconexion->consulta("SELECT NOMBRE AS '$langexamennombre',FECHA AS '$langexamenfecha',MODELO AS '$langexamenmodelo',EVALUACION AS '$langexamenevaluacion' FROM EXAMEN WHERE IDEXAMEN='$idexamen'");
$miconexion->verconsultaexamen();

echo "<br>";
$miconexion = new DB_mysql ;
$miconexion->conectar($BaseDatosNombre, $BaseDatosServidor, $BaseDatosUsuario, $BaseDatosClave);
$miconexion->consulta("SELECT COMENTARIO AS '$langexamencomentario' FROM EXAMEN WHERE IDEXAMEN='$idexamen'");
$miconexion->verconsultaexamen();

echo "<br>";
$miconexion = new DB_mysql ;
$miconexion->conectar($BaseDatosNombre, $BaseDatosServidor, $BaseDatosUsuario, $BaseDatosClave);
$miconexion->consulta("SELECT A.NOMBRE AS '$langexamenasignatura', C.NOMBRE AS '$langexamencurso' FROM EXAMEN E, ASIGNATURA A, CURSO C WHERE E.IDEXAMEN='$idexamen' AND E.IDASIGNATURA=A.IDASIGNATURA AND A.IDCURSO=C.IDCURSO");
$miconexion->verconsultaexamen();

echo "<br>";
echo "<center>";
echo "<u><FONT size=5>$langexamenpregunta:</FONT></u>";
echo "</center>";
echo "<br><br>";

//Guardamos el titulo de las preguntas para luego procesarlas en un array
$miconexion = new DB_mysql ;
$miconexion->conectar($BaseDatosNombre, $BaseDatosServidor, $BaseDatosUsuario, $BaseDatosClave);
$miconexion->consulta("SELECT P.TITULO FROM PREGUNTA P, EXAMEN E WHERE E.IDEXAMEN='$idexamen' AND E.IDEXAMEN=P.IDEXAMEN ORDER BY P.IDORDER, P.IDPREGUNTA");
if ($miconexion->Errno>0 )
{
	echo("<IMG SRC='imagenes/peligro.png' NAME='Peligro' ALIGN=MIDDLE BORDER=0><br><br>");
	echo("$langerrorexamen1");echo("<br><br>");
	die("Error: $miconexion->Error");
}

$resultadotitulopreguntas=$miconexion->Consulta_ID;

//Guardamos el texto de las preguntas para luego procesarlas en un array
$miconexion = new DB_mysql ;
$miconexion->conectar($BaseDatosNombre, $BaseDatosServidor, $BaseDatosUsuario, $BaseDatosClave);
$miconexion->consulta("SELECT P.TEXTO FROM PREGUNTA P, EXAMEN E WHERE E.IDEXAMEN='$idexamen' AND E.IDEXAMEN=P.IDEXAMEN ORDER BY P.IDORDER, P.IDPREGUNTA");
if ($miconexion->Errno>0 )
{
	echo("<IMG SRC='imagenes/peligro.png' NAME='Peligro' ALIGN=MIDDLE BORDER=0><br><br>");
	echo("$langerrorexamen1");echo("<br><br>");
	die("Error: $miconexion->Error");
}

$resultadopreguntas=$miconexion->Consulta_ID;

   $i=1;
   //echo "<font size=3>\n";
   while ($preguntas = mysql_fetch_row($resultadopreguntas) and $titulopreguntas = mysql_fetch_row($resultadotitulopreguntas)) {
	$titulopregunta=$titulopreguntas[0];
	echo "<b>Pregunta $i.- $titulopregunta</b><br>";
	$pregunta=$preguntas[0];
	echo "".nl2br($pregunta)."";
	echo "<br><br>";
	$i++;
   }
 

echo "<br>";
echo "<center>";
echo "<u><FONT size=5>$langexamensolucion:</FONT></u>";
echo "</center>";
echo "<br><br>";

//Guardamos el texto de las soluciones para luego procesarlas en un array
$miconexion = new DB_mysql ;
$miconexion->conectar($BaseDatosNombre, $BaseDatosServidor, $BaseDatosUsuario, $BaseDatosClave);
$miconexion->consulta("SELECT P.SOLUCION FROM PREGUNTA P, EXAMEN E WHERE E.IDEXAMEN='$idexamen' AND E.IDEXAMEN=P.IDEXAMEN ORDER BY P.IDORDER, P.IDPREGUNTA");
if ($miconexion->Errno>0 )
{
	echo("<IMG SRC='imagenes/peligro.png' NAME='Peligro' ALIGN=MIDDLE BORDER=0><br><br>");
	echo("$langerrorexamen1");echo("<br><br>");
	die("Error: $miconexion->Error");
}

$resultadosolucion=$miconexion->Consulta_ID;


    $i=1;
    //echo "<font size=3>\n";
    while ($soluciones = mysql_fetch_row($resultadosolucion)) {
		$solucion=$soluciones[0];
		echo "<b>$langexamensolucion1 $i.-</b><br>";
		echo "".nl2br($solucion)."";
		echo "<br><br>";
		$i++;
     }

?>

<?php
/* Pie */
require_once "pie.php";
?>
