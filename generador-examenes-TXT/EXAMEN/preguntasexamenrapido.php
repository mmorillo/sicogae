<?php
/************************************************************************
* Software: ELSAM                                                       *
* Version:  0.9                                                         *
* Date:     2006-08-06                                                  *
* Author:   Miguel Morillo Iruela                                       *
* License:  GPL                                                         *
* This program is distributed under the terms and conditions of the GPL *
* See the LICENSE files for details                                     *
************************************************************************/

/* Autenticacion */

require 'CHECK/check_login.php';

require 'CHECK/chequealogin.php';

// login OK

?>

<BR><BR>

<CENTER>
<FONT size=5><?php echo "$langexamenrapidopreguntas"?></FONT>
<HR><BR>
</CENTER>

<FORM METHOD='post' ACTION="iniciopdf.php?menu=examenes&amp;enlace=examenpdfrapido">

<b><?php echo "$langexamencurso"?>:</b> <input type='text' name='curso' value='' size=20>&nbsp;&nbsp;&nbsp;
<b><?php echo "$langexamengrupo"?>:</b> <input type='text' name='grupo' value='' size=20><br><br>
<b><?php echo "$langexamenasignatura"?>:</b> <input type='text' name='asignatura' value='' size=30>&nbsp;&nbsp;&nbsp;
<b><?php echo "$langexamencomentario"?>:</b> <input type='text' name='comentario' value='' size=20><br><br>
<b><?php echo "$langexamenfecha"?> (dd/mm/yyyy):</b> <input type='text' name='date' value='<? echo(date("d/m/Y")); ?>' size=15>&nbsp;&nbsp;&nbsp;
<b><?php echo "$langexamenevaluacion"?>:</b> <input type='text' name='evaluacion' value='' size=15><br><br>
<b><?php echo "$langexamenmodelo"?>:</b> <input type='text' name='modelo' value='' size=10>&nbsp;&nbsp;&nbsp;
<b><?php echo "$langexamencodigobarras"?>:</b> <input type='text' name='codigobarras' value='' size=10>
<b><?php echo "$langexamencodigobarras2"?></b><br><br><br>

<CENTER>
<FONT size=4><?php echo "$langexamenrapidopreguntas2"?></FONT><br><br>
</CENTER>

<?php
require ("funciones-examenes.inc.php");

$idasignatura = $_GET["var"];

$miconexion = new DB_mysql ;
$miconexion->conectar($BaseDatosNombre, $BaseDatosServidor, $BaseDatosUsuario, $BaseDatosClave);
$miconexion->consulta("SELECT distinct E.IDEXAMEN FROM EXAMEN E, ASIGNATURA A, PREGUNTA P WHERE P.IDEXAMEN=E.IDEXAMEN AND E.IDASIGNATURA='$idasignatura' AND E.IDASIGNATURA=A.IDASIGNATURA ORDER BY E.NOMBRE");
$miconexion->verconsultacheckboxexamenrapido();

?>

<BR><BR>
<center>
<input type='submit' class='button' value="<?php echo "$langexamenrapidoboton"?>">
</center>
</FORM>

<?php
/* Pie */
require_once "pie.php";
?>
