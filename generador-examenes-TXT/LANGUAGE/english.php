<?php
/************************************************************************
* Software: ELSAM                                                       *
* Version:  0.92                                                         *
* Date:     2006-08-06                                                  *
* Author:   Miguel Morillo Iruela                                       *
* License:  GPL                                                         *
* This program is distributed under the terms and conditions of the GPL *
* See the LICENSE files for details                                     *
************************************************************************/

// Menu Inicio

$langinicio = "Return to the main menu";
$langlogout = "Logout and exit";
$langpreferencias = "Add, delete and edit user data <br> add, change and erase logos";
$langcursos = "Check in detail the data stored<br> of the courses<br> inserts, updates and delete courses.";
$langasignaturas = "Check in detail the data stored<br> of the subjects<br> inserts, updates and delete subjects.";
$langexamenes = "Check in detail the data stored<br> of the exams<br> inserts, updates and delete exams. <br> You can also store and display your exams <br> pdf (online or stored)";
$langpreguntas = "Check in detail the data stored<br> of the questions<br> inserts, updates and delete questions.";

$langmenuinicio = "Home";
$langmenulogout = "Exit";
$langmenupreferencias = "Preferences";
$langmenucursos = "Courses";
$langmenuasignaturas = "Subjects";
$langmenuexamenes = "Exams";
$langmenupreguntas = "Questions";
$langmenucalendario = "Calendar of examinations";

// Menu principal
$langmenuacceso = "Users access";
$langmenudescargas = "Download";
$langmenupantallas = "Screenshots";
$langmenudoc = "Documentation";
$langmenuemail = "E-Mail";

$langacceso = "Access subscribers <br> Login access";
$langdescargas = "Download the application of <br> the official repositories.";
$langpantallas = "Screenshots of<br> different modules of the application.";
$langdoc = "Installation documentation<br> and user manual.";
$langemail = "Write us!";


//Index
$langaccesousuarios="Access subscribers";
$langlogin1="Enter user name and password";
$langusuario1="User: ";
$langpass1="Password: ";

// Calendario
$langcalendario="Dates of examinations scheduled";

// Preferencias
$langpreferencias="Preferences";
$langprefusuario="Users";
$langprefdatosusuario ="Data users";
$langprefnuevosusuarios ="Register new users";
$langprefactusuarios ="Update data users";
$langprefborrarusuarios ="Delete users";
$langpreflogo ="Logos";
$langprefnuevologo ="Up / add school logo";
$langprefcambiarlogo ="Change / display school logo";
$langprefborrarlogo ="Delete school logo";
$langprefverlogo ="Existing Logos";
$langprefverlogodefecto ="Default school Logo, will be used to generate exams";
$langprefelegirlogo ="Push to choose this image as a logo:";
$langprefusuarios ="Subscribers in the system";
$langprefusuarios1 ="Users";
$langprefusuario ="User";
$langpreffecharegistro ="Record date";
$langprefdireccion ="Address";
$langprefultimoacceso ="Last access";
$langprefusuariosadmin ="Administrator users";
$langprefregistro ="User registrant correctly";
$langprefregistro2 ="Thank you, user added to the database, and can access the system.";
$langprefregistrousuario ="Registration of new users";
$langprefregistroobligatorio ="Required *";
$langprefregusuario ="User *:";
$langprefregpassword ="Password *:";
$langprefregpassword2 ="Confirm Password *:";
$langprefregpassword3 ="New Password *:";
$langprefregemail ="E-Mail *:";
$langprefregwebsite ="Website:";
$langprefregdireccion ="Address:";
$langprefregadmin ="¿Administrator?";
$langprefregboton ="Register user";
$langprefregactualiza ="User updated";
$langprefregactualiza1 ="Updated data subscribers";
$langprefregactualizaboton ="Update user";
$langprefborrarusuariook ="has been deleted user correctly: ";
$langprefborrarusuarioboton ="Delte user";
$langprefborrausuario1 ="The next user will be deleted system:";
$langprefborrausuario ="Select a user to delete";
$langprefsubirlogo ="The images should not exceed 200 kb. Only can upload files in jpg format. Maximum 5 logos";
$langprefsubirlogo1 ="We recommend upload pictures with logos of the following sizes:";
$langprefsubirlogo2 ="Width: 670 píxeles - Height: 850 píxeles";
$langpreflogo ="Existing Logos";
$langpreflogoborrar ="Push to delete this logo:";
$langpreflogoborrar1 ="The file";
$langpreflogoborrar2 ="has been deleted";
$langpreflogo2="The logo has been updated";


//Curso
$langcursoconsulta ="View course";
$langcursoactualiza ="Update course";
$langcursoinserta ="Insert course";
$langcursoborra ="Delete course";
$langcursotitulo ="Listing courses introduced into the system";
$langcursotitulo1 ="Detailed information about the course";
$langcursotitulo2 ="Course";
$langcursocomentario ="Coment:";
$langcursoasignatura ="Subjects of the course:";
$langcursotitulo3 ="Select a course to modify their data";
$langcursoactualizaboton ="Update course";
$langcursotitulo4 ="Amendment to the data in the form of the course";
$langcursook ="There has been ongoing with the amended success";
$langcursoactualizaboton ="Press to modify the current data";
$langcursotitulo5 ="Enter the data to include a course in the database";
$langcursotitulo6 ="Name Course:";
$langcursoinsertaboton ="Insert course";
$langcursoinsertaok ="The course has been inserted into the system correctly";
$langcursoborrar ="Choose a course to delete";
$langcursoborrarboton ="Delete course";
$langcursoborrar2 ="The next course will be deleted system:";
$langcursoborrarok ="The course has been deleted correctly";


//Asignatura
$langasignaturaconsulta ="See subject";
$langasignaturaactualiza ="Update subject";
$langasignaturainserta ="Insert subject";
$langasignaturaborra ="Delete subject";
$langasignaturatitulo1 ="List of subjects stored into the system";
$langasignaturatitulo2 ="Detailed information about the subject";
$langasignatura1 ="Subject";
$langasignaturagrupo ="Group";
$langasignaturacomentario ="Commentary";
$langasignaturacurso ="Course";
$langasignaturaexamenes ="Examinations of the subject:";
$langasignaturamodifica ="Choose a subject to modify their data";
$langasignaturamodificaboton ="Modify a subject";
$langasignaturamodifica1 ="Amendment to the data subject in the form";
$langasignaturamodificaok ="It has changed the subject correctly";
$langasignaturamodificaboton2 ="Press to modify the data subject";
$langasignaturaborra1 ="Choose a subject to delete";
$langasignaturaborraboton ="Delete subject";
$langasignaturaborra2 ="You must choose a subject in order to delete it.";
$langasignaturaborra3 ="The next course will be erased from the system:";
$langasignaturaborraok ="has been deleted properly the subject";
$langasignaturainsertar ="Enter the data to include a subject in the database";
$langasignaturainsertaboton ="Insert subject";
$langasignaturainsertaok ="The subject has been inserted into the system correctly";


//Examenes
$langexamenconsulta ="Consult examination";
$langexamenactualiza ="Update examination";
$langexameninserta ="Insert examination";
$langexamenborra ="Delete examination";
$langexamenconsultabd ="Consult exams stored on BD";
$langexamencalificacion ="Rating";
$langexamenborrabd ="Delete exams stored on BD";
$langexamentitulo1 ="Listing examinations introduced into the system";
$langexamentitulo2 ="Detailed information about the examination";
$langexamennombre ="Name examination";
$langexamennombre2 ="Examination";
$langexamenfecha ="Date";
$langexamenmodelo ="Model";
$langexamenevaluacion ="assessment";
$langexamencomentario ="Commentary";
$langexamenasignatura ="Subject";
$langexamencurso ="Course";
$langexamenpregunta ="Questions";
$langexamensolucion ="Solutions";
$langexamensolucion1 ="Solution to the question";
$langexamenpdf ="Listing exams stored in database";
$langexamenactualiza1 ="Select a examination to modify their data";
$langexamenactualizaboton ="Update examination";
$langexamenactualiza2 ="Amendment to the data in the form of consideration";
$langexamenmodificaok ="It has changed the review correctly";
$langexamenmodificaboton2 ="Press to modify data review";
$langexameninserta1 ="Enter the data to include an examination into the database";
$langexameninsertaboton ="Insert examination";
$langexameninsertaok2 ="has been inserted into consideration the system correctly";
$langexamenborrar1 ="Select a test to delete";
$langexamenborrarboton ="Delete examination";
$langexamenborraok ="has been deleted consideration";
$langexamenborrar2 ="The next review will be deleted system:";
$langexamencodigobarras ="Values for the generation bar code";
$langexamencodigobarras2 ="(If you can not introduce any value use the current date)";
$langexamenasignatura2 ="Subject:";
$langexamenrespuestas="Create answer sheet";
$langexamenrespuestas2 ="Press to create your answer sheet";
$langexamenrespuestas3 ="Answer sheet";
$langexamenorden1= "Change order of questions";
$langexamenorden2= "Change the Order of the questions displayed in the examination";
$langexamenorden3= "Press to modify the order question";
$langexamenasignaturalista ="Select the subject to select then the examination";
$langexamengrupo ="Group";
$langexamenrapidoboton ="Press to create the examination";
$langexamenrapidomenu="Create examination quick";
$langexamenrapidopreguntas="Fill in the data and select the questions you want to create the review";
$langexamenrapidopreguntas2="Questions stored, to create blank examination do not selections any question";
$langexamenonlinemenu="Create writable examination";
$langexamenonlineexamenes="Listing exams stored in database, Select a examination to create a writable version";
$langexamenonlineexamenes2="It is required a client that support Javascript (Ej: Acrobat reader v.8)";
$langexamenonlinetitulo="Care For Environment, reduces your consumption of paper";

//Preguntas
$langpreguntaconsulta ="See question";
$langpreguntaactualiza ="Update question";
$langpreguntainserta ="Insert question";
$langpreguntaborra ="Delete question";
$langpreguntatitulo1 ="Listing questions introduced into the system";
$langpreguntatitulo2 ="Detailed information about the question";
$langpreguntatitulo ="Title";
$langpreguntafecha ="Date";
$langpreguntapuntuacion ="Score";
$langpreguntacodigobarras ="Values for the generation bar code";
$langpreguntacodigobarras2 ="(If you can not introduce any value use the current date)";
$langpreguntacomentario ="Commentary";
$langpreguntapregunta ="Question";
$langpreguntasolucion ="Solution";
$langpreguntatitulo3 ="Enter the data to include a question in the database";
$langpreguntatitulo4 ="Title question:";
$langpreguntatexto ="Text";
$langpreguntainsertaboton ="Insert question";
$langpreguntainsertaok ="The question has been inserted into the system correctly";
$langpreguntaactualiza1 ="Select a question to modify their data";
$langpreguntaactualizaboton ="Update question";
$langpreguntaactualiza2 ="Amendment to the data in question form";
$langpreguntaactualizaok ="It has changed the question correctly";
$langpreguntaexamen ="Examination:";
$langpreguntaexamen2 ="Examination asigned:";
$langpreguntaactualizaok2 ="Press to modify data question:";
$langpreguntaborrar1 ="Select a question to delete";
$langpreguntaborraboton ="Delete question";
$langpreguntaborraok ="The next question will be erased from the system:";
$langpreguntaborraok2 ="Has been deleted the question";
$langpreguntaasignaturalista ="Select the examination to select then the question";

//PDF
$langpdfpregunta ="Question:";
$langpdffecha ="Date:";
$langpdfmodelo ="Model:";
$langpdfevaluacion ="Assessment:";
$langpdfpregunta2 ="Question -";
$langpdfpuntos ="points";
$langpdfsolucion ="Solution:";
$langpdfasignatura ="Subject:";
$langpdfcurso ="Course:";
$langpdfgrupo ="Group:";
$langpdfcomentario ="Comments:";
$langpdfalumno ="Student:";
$langpdfpreguntas ="QUESTIONS:";
$langpdfrespuestas ="RESPONSES:";
$langpdfpregunta3 ="Question";
$langpdfguardaok ="It has saved the examination";
$langpdfguardaok2 ="in Database";


//Graficas Flash
$langflashpreguntas="Questions";
$langflashexamenes="Examination";
$langflashexamenesalmacenados="Examinations stored";
$langflashnumeropreguntas="Number of questions and examinations";
$langflashcursos="Courses";
$langflashasignaturas="Subjects";
$langflashnumerocursos="Number of courses and subjects";
$langflashnumeroexamenes="Number of exams created in";
$langflashcreacionexamenes="Creating exams";
$langflashenero="January";
$langflashfebrero="February";
$langflashmarzo="March";
$langflashabril="April";
$langflashmayo="May";
$langflashjunio="June";
$langflashjulio="July";
$langflashagosto="August";
$langflashsep="September";
$langflashoct="October";
$langflashnov="November";
$langflashdic="December";
$langflashfechaexamenes="Creation Date examinations and questions";
$langflashnumeroexamenestotal="Total number of examinations and questions";
$langflashnumeroasignaturas="Number of subjects per course";
$langflashnumeroexamenesasig="Number of reviews by subject";


// Error
$langerrorcampoobligatorio ="You must complete all the required fields";
$langerrorusuario ="The user: ";
$langerrorexiste ="exists,there you have to choose someone else";
$langerrorpasswords ="Passwords do not match";
$langerroremail ="Email invalid";
$langerrorusuario1 ="You have to be a normal user or administrator";
$langerrorborrausuario ="You must choose a user to delete it.";
$langerrorborrarusuario2 ="There was an error to erase the user";
$langerrorlogo ="You can not upload more than 5 logos";
$langerrorlogo1 ="Your file exceeds the permitted limit";
$langerrorlogo2 ="The file has been sent successfully";
$langerrorlogo3 ="The file is not an image jpg";
$langerrorlogo4 ="You must pass a file to upload";
$langerrorlogo5 ="An error occurred when deleting the file";
$langerrorcurso1 ="An error occurred when inserting the course";
$langerrorcurso2 ="You have to fill the field course correctly. There must be an empty field";
$langerrorcurso3 ="An error occurred when inserting the course";
$langerrorcurso4 ="You must choose one course in order to delete it.";
$langerrorcurso5 ="There was an error to delete the course";
$langerrorcurso6 ="The course you want to delete is being used by a subject";
$langerrorcurso7 ="Failed to delete the course";
$langerrorasignatura1 ="You have to fill in the subject field correctly.";
$langerrorasignatura2 ="An error occurred when updating the subject";
$langerrorasignatura3 ="The course you want to upgrade must be assigned to a course";
$langerrorasignatura4 ="Failed to update the subject";
$langerrorasignatura5 ="An error occurred when updating the subject";
$langerrorasignatura6 ="There was an error to remove the subject";
$langerrorasignatura7 ="The course you want to delete has assigned an examination, to delete the subject should erase examinations allocated to this subject";
$langerrorasignatura8 ="Failed to delete the subject";
$langerrorasignatura9 ="You must fill in all fields correctly. You have to create course before assigning a subject";
$langerrorasignatura10 ="An error occurred when inserting the subject";
$langerrorasignatura11 ="The subject you want to insert should be assigned to a course";
$langerrorasignatura12 ="Failed to update the subject";
$langerrorasignatura13 ="You must fill in all fields correctly.";
$langerrorasignatura14 ="An error occurred when inserting the subject";
$langerrorexamen1 ="Error";
$langerrorexamen2 ="An error occurred when tried to display the PDF";
$langerrorexamen3 ="You have to fill the field examination correctly.";
$langerrorexamen4 ="An error occurred when inserting the examination";
$langerrorexamen5 ="You must fill in all fields correctly, there should be a subject to assign an examination";
$langerrorexamen6 ="An error occurred, there must be a subject to assign  an examination";
$langerrorexamen7 ="You must fill in all fields correctly. The examination should not be an empty field";
$langerrorexamen8 ="An error occurred when inserting the examination";
$langerrorexamen9 ="There was an error to delete the examination";
$langerrorexamen10 ="You must choose an examination in order to delete it.";
$langerrorexamen11 ="The examination you want to delete has questions associated. To delete an examination should not have associated questions";
$langerrorexamen12 ="You must choose at least a question";
$langerrorpregunta1 ="You must fill in all fields correctly. You have to create an examination before assigning a question";
$langerrorpregunta2 ="An error occurred when inserting the question, you must create an examination before assigning a question";
$langerrorpregunta3 ="You must fill in all fields correctly. The question should not be an empty field";
$langerrorpregunta4 ="An error occurred when inserting the question";
$langerrorpregunta5 ="There was an error to change the question";
$langerrorpregunta6 ="You have to fill in the field question correctly.";
$langerrorpregunta7 ="You have a choice to delete a question.";
$langerrorpregunta8 ="There was an error to remove the question";
$langerrorpdf ="Failed to insert the examination into BD:";
?>
