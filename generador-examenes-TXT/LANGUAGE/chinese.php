<?php
/************************************************************************
* Software: ELSAM                                                       *
* Version:  0.92                                                         *
* Date:     2006-08-06                                                  *
* Author:   Miguel Morillo Iruela                                       *
* License:  GPL                                                         *
* This program is distributed under the terms and conditions of the GPL *
* See the LICENSE files for details                                     *
************************************************************************/

// Menu Inicio

$langinicio = "Return to the main menu";
$langlogout = "Logout and exit";
$langpreferencias = "Add, delete and edit user data <br> add, change and erase logos";
$langcursos = "Check in detail the data stored<br> of the courses<br> inserts, updates and delete courses.";
$langasignaturas = "Check in detail the data stored<br> of the subjects<br> inserts, updates and delete subjects.";
$langexamenes = "Check in detail the data stored<br> of the exams<br> inserts, updates and delete exams. <br> You can also store and display your exams <br> pdf (online or stored)";
$langpreguntas = "Check in detail the data stored<br> of the questions<br> inserts, updates and delete questions.";

$langmenuinicio = "Home";
$langmenulogout = "Exit";
$langmenupreferencias = "Preferences";
$langmenucursos = "Courses";
$langmenuasignaturas = "Subjects";
$langmenuexamenes = "Exams";
$langmenupreguntas = "Questions";

// Menu principal
$langmenuacceso = "华语/華語";
$langmenudescargas = "Download";
$langmenupantallas = "Screenshots";
$langmenudoc = "Documentation";
$langmenuemail = "E-Mail";

$langacceso = "Access subscribers <br> 华语/華語";
$langdescargas = "Download the application of <br> the official repositories.";
$langpantallas = "Screenshots of<br> different modules of the application.";
$langdoc = "Installation documentation<br> and user manual.";
$langemail = "Write us!";


//Index
$langaccesousuarios="Acceso de usuarios registrados";
$langlogin1="Introduce usuario y contraseña";
$langusuario1="Usuario: ";
$langpass1="Contraseña: ";


// Preferencias
$langpreferencias="Preferencias";
$langprefusuario="Usuarios";
$langprefdatosusuario ="Datos usuarios";
$langprefnuevosusuarios ="Registro nuevos usuarios";
$langprefactusuarios ="Actualizar datos usuarios";
$langprefborrarusuarios ="Borrar usuarios";
$langpreflogo ="Logos";
$langprefnuevologo ="Subir/añadir logo colegio";
$langprefcambiarlogo ="Cambiar/visualizar logo colegio";
$langprefborrarlogo ="Borrar logo colegio";
$langprefverlogo ="Logos existentes en el sistema";
$langprefverlogodefecto ="Logo del colegio por defecto, será usado para generar los exámenes";
$langprefelegirlogo ="Pulsar para elegir esta imagen como logo:";
$langprefusuarios ="Usuarios registrados en el sistema";
$langprefusuarios1 ="Usuarios";
$langprefusuario ="Usuario";
$langpreffecharegistro ="Fecha registro";
$langprefdireccion ="Dirección";
$langprefultimoacceso ="Ultimo acceso";
$langprefusuariosadmin ="Usuarios administradores";
$langprefregistro ="Usuario registrado correctamente";
$langprefregistro2 ="Gracias, usuario añadido a la base de datos, ya puede acceder al sistema.";
$langprefregistrousuario ="Registro de nuevos usuarios";
$langprefregistroobligatorio ="Campos obligatorios *";
$langprefregusuario ="Usuario *:";
$langprefregpassword ="Password *:";
$langprefregpassword2 ="Confirmar Password *:";
$langprefregpassword3 ="Nueva Password *:";
$langprefregemail ="E-Mail *:";
$langprefregwebsite ="Website:";
$langprefregdireccion ="Dirección:";
$langprefregadmin ="¿Administrador?";
$langprefregboton ="Registrar usuario";
$langprefregactualiza ="Usuario actualizado correctamente";
$langprefregactualiza1 ="Actualización datos usuarios registrados";
$langprefregactualizaboton ="Actualizar usuario";
$langprefborrarusuariook ="Se ha borrado correctamente el usuario: ";
$langprefborrarusuarioboton ="Borrar usuario";
$langprefborrausuario1 ="El siguiente usuario sera borrado del sistema:";
$langprefborrausuario ="Seleccione un usuario a borrar";
$langprefsubirlogo ="Las imagenes no deben exceder los 200 kb.Solo se pueden subir archivos en formato jpg. Maximo 5 logos";
$langprefsubirlogo1 ="Recomendamos subir logos de imagenes con el siguiente tamaño:";
$langprefsubirlogo2 ="Anchura: 670 píxeles - Altura: 850 píxeles";
$langpreflogo ="Logos existentes en el sistema";
$langpreflogoborrar ="Pulsa para borrar este logo:";
$langpreflogoborrar1 ="El archivo";
$langpreflogoborrar2 =" ha sido borrado correctamente";
$langpreflogo2="El logo ha sido actualizado correctamente";


//Curso
$langcursoconsulta ="Consultar curso";
$langcursoactualiza ="Actualizar curso";
$langcursoinserta ="Insertar curso";
$langcursoborra ="Borrar curso";
$langcursotitulo ="Listado de cursos introducidos en el sistema";
$langcursotitulo1 ="Información detallada del curso";
$langcursotitulo2 ="Curso";
$langcursocomentario ="Comentario:";
$langcursoasignatura ="Asignaturas del curso:";
$langcursotitulo3 ="Seleccione un curso para modificar sus datos";
$langcursoactualizaboton ="Modificar Curso";
$langcursotitulo4 ="Modifica los datos del curso en el formulario";
$langcursook ="Se ha realizado la modificacion curso con exito";
$langcursoactualizaboton ="Pulse para modificar datos del curso";
$langcursotitulo5 ="Introduce los datos para incluir un curso en la base de datos";
$langcursotitulo6 ="Nombre Curso:";
$langcursoinsertaboton ="Insertar curso";
$langcursoinsertaok ="Se ha insertado el curso en el sistema correctamente";
$langcursoborrar ="Seleccione un curso a borrar";
$langcursoborrarboton ="Borrar curso";
$langcursoborrar2 ="El siguiente curso sera borrado del sistema:";
$langcursoborrarok ="Se ha borrado correctamente el curso";


//Asignatura
$langasignaturaconsulta ="Consultar asignatura";
$langasignaturaactualiza ="Actualizar asignatura";
$langasignaturainserta ="Insertar asignatura";
$langasignaturaborra ="Borrar asignatura";
$langasignaturatitulo1 ="Listado de asignaturas introducidas en el sistema";
$langasignaturatitulo2 ="Información detallada de la asignatura";
$langasignatura1 ="Asignatura";
$langasignaturagrupo ="Grupo";
$langasignaturacomentario ="Comentario";
$langasignaturacurso ="Curso";
$langasignaturaexamenes ="Examenes de la asignatura:";
$langasignaturamodifica ="Seleccione una asignatura para modificar sus datos";
$langasignaturamodificaboton ="Modificar Asignatura";
$langasignaturamodifica1 ="Modifica los datos de la asignatura en el formulario";
$langasignaturamodificaok ="Se ha modificado la asignatura correctamente";
$langasignaturamodificaboton2 ="Pulse para modificar datos de la asignatura";
$langasignaturaborra1 ="Seleccione una asignatura para borrar";
$langasignaturaborraboton ="Borrar asignatura";
$langasignaturaborra2 ="Debes un elegir una asignatura para poder borrarla.";
$langasignaturaborra3 ="La siguiente asignatura sera borrada del sistema:";
$langasignaturaborraok ="Se ha borrado correctamente la asignatura";
$langasignaturainsertar ="Introduce los datos para incluir una asignatura en la base de datos";
$langasignaturainsertaboton ="Insertar asignatura";
$langasignaturainsertaok ="Se ha insertado la asignatura en el sistema correctamente";


//Examenes
$langexamenconsulta ="Consultar examen";
$langexamenactualiza ="Actualizar examen";
$langexameninserta ="Insertar examen";
$langexamenborra ="Borrar examen";
$langexamenconsultabd ="Consultar exámenes guardados en BD";
$langexamencalificacion ="Calificación";
$langexamenborrabd ="Borrar exámenes guardados en BD";
$langexamentitulo1 ="Listado de exámenes introducidos en el sistema";
$langexamentitulo2 ="Información detallada del examen";
$langexamennombre ="Nombre examen";
$langexamennombre2 ="Examen";
$langexamenfecha ="Fecha";
$langexamenmodelo ="Modelo";
$langexamenevaluacion ="Evaluación";
$langexamencomentario ="Comentario";
$langexamenasignatura ="Asignatura";
$langexamencurso ="Curso";
$langexamenpregunta ="Preguntas";
$langexamensolucion ="Soluciones";
$langexamensolucion1 ="Solución a la pregunta";
$langexamenpdf ="Listado de exámenes guardados en Base de datos";
$langexamenactualiza1 ="Seleccione un examen para modificar sus datos";
$langexamenactualizaboton ="Modificar Examen";
$langexamenactualiza2 ="Modifica los datos del examen en el formulario";
$langexamenmodificaok ="Se ha modificado del examen correctamente";
$langexamenmodificaboton2 ="Pulse para modificar datos del examen";
$langexameninserta1 ="Introduce los datos para incluir un examen en la base de datos";
$langexameninsertaboton ="Insertar examen";
$langexameninsertaok2 ="Se ha insertado el examen en el sistema correctamente";
$langexamenborrar1 ="Seleccione un examen para borrar";
$langexamenborrarboton ="Borrar examen";
$langexamenborraok ="Se ha borrado correctamente el examen";
$langexamenborrar2 ="El siguiente examen sera borrado del sistema:";
$langexamencodigobarras ="Valores para la generación codigo barras";
$langexamencodigobarras2 ="(Sino se introduce ningun valor se usara la fecha actual)";
$langexamenasignatura2 ="Asignatura:";
$langexamencodigobarras ="Valores para la generación codigo barras";
$langexamencodigobarras2 ="(Sino se introduce ningun valor se usara la fecha actual)";
$langexamenrespuestas="Crear hoja de respuestas";
$langexamenrespuestas2 ="Pulse en el examen deseado para crear su hoja de respuestas";
$langexamenrespuestas3 ="Hoja de respuestas";

//Preguntas
$langpreguntaconsulta ="Consultar pregunta";
$langpreguntaactualiza ="Actualizar pregunta";
$langpreguntainserta ="Insertar pregunta";
$langpreguntaborra ="Borrar pregunta";
$langpreguntatitulo1 ="Listado de preguntas introducidas en el sistema";
$langpreguntatitulo2 ="Información detallada de la pregunta";
$langpreguntatitulo ="Titulo";
$langpreguntafecha ="Fecha";
$langpreguntapuntuacion ="Puntuación";
$langpreguntacodigobarras ="Valores para la generación codigo barras";
$langpreguntacodigobarras2 ="(Sino se introduce ningun valor se usara la fecha actual)";
$langpreguntacomentario ="Comentario";
$langpreguntapregunta ="Pregunta";
$langpreguntasolucion ="Solución";
$langpreguntatitulo3 ="Introduce los datos para incluir una pregunta en la base de datos";
$langpreguntatitulo4 ="Titulo pregunta:";
$langpreguntatexto ="Texto";
$langpreguntainsertaboton ="Insertar pregunta";
$langpreguntainsertaok ="Se ha insertado la pregunta en el sistema correctamente";
$langpreguntaactualiza1 ="Seleccione una pregunta para modificar sus datos";
$langpreguntaactualizaboton ="Modificar pregunta";
$langpreguntaactualiza2 ="Modifica los datos de la pregunta en el formulario";
$langpreguntaactualizaok ="Se ha modificado la pregunta correctamente";
$langpreguntaexamen ="Examen:";
$langpreguntaexamen2 ="Examen asignado:";
$langpreguntaactualizaok2 ="Pulse para modificar datos de la pregunta:";
$langpreguntaborrar1 ="Seleccione una pregunta para borrar";
$langpreguntaborraboton ="Borrar pregunta";
$langpreguntaborraok ="La siguiente pregunta sera borrada del sistema:";
$langpreguntaborraok2 ="Se ha borrado correctamente la pregunta";

//PDF
$langpdfpregunta ="Pregunta:";
$langpdffecha ="Fecha:";
$langpdfmodelo ="Modelo:";
$langpdfevaluacion ="Evaluación:";
$langpdfpregunta2 ="Pregunta -";
$langpdfpuntos ="puntos";
$langpdfsolucion ="Solución:";
$langpdfasignatura ="Asignatura:";
$langpdfcurso ="Curso:";
$langpdfgrupo ="Grupo:";
$langpdfcomentario ="Comentario:";
$langpdfalumno ="Alumno/a:";
$langpdfpreguntas ="PREGUNTAS:";
$langpdfrespuestas ="RESPUESTAS:";
$langpdfpregunta3 ="Pregunta";
$langpdfguardaok ="Se ha guardado correctamente el examen";
$langpdfguardaok2 ="en Base de datos";


//Graficas Flash
$langflashpreguntas="Preguntas";
$langflashexamenes="Exámenes";
$langflashexamenesalmacenados="Exámenes almacenados";
$langflashnumeropreguntas="Número de preguntas y exámenes";
$langflashcursos="Cursos";
$langflashasignaturas="Asignaturas";
$langflashnumerocursos="Número de cursos y asignaturas";
$langflashnumeroexamenes="Número de exámenes creados en el";
$langflashcreacionexamenes="Creación de examenes";
$langflashenero="Enero";
$langflashfebrero="Febrero";
$langflashmarzo="Marzo";
$langflashabril="Abril";
$langflashmayo="Mayo";
$langflashjunio="Junio";
$langflashjulio="Julio";
$langflashagosto="Agosto";
$langflashsep="Septiembre";
$langflashoct="Octubre";
$langflashnov="Noviembre";
$langflashdic="Diciembre";
$langflashfechaexamenes="Fecha de creación de exámenes y preguntas";
$langflashnumeroexamenestotal="Número total de exámenes y preguntas";
$langflashnumeroasignaturas="Número de asignaturas por curso";
$langflashnumeroexamenesasig="Número de exámenes por asignatura";


// Error
$langerrorcampoobligatorio ="Debes rellenar todos los campos obligatorios";
$langerrorusuario ="El usuario: ";
$langerrorexiste ="existe, debes elegir otro usuario";
$langerrorpasswords ="Las contraseñas no coinciden";
$langerroremail ="Correo electronico invalido";
$langerrorusuario1 ="Debes de ser un usuario normal o admnistrador";
$langerrorborrausuario ="Debes un elegir un usuario para poder borrarlo.";
$langerrorborrarusuario2 ="Se ha producido un error al borrar el usuario";
$langerrorlogo ="No es posible subir mas de 5 logos";
$langerrorlogo1 ="Tu archivo excede al limite permitido";
$langerrorlogo2 ="El archivo ha sido enviado correctamente";
$langerrorlogo3 ="El archivo no es una imagen jpg";
$langerrorlogo4 ="Debes pasar un fichero para poder subirlo";
$langerrorlogo5 ="Se ha producido un error al borrar el archivo";
$langerrorcurso1 ="Se ha producido un error al insertar el curso";
$langerrorcurso2 ="Debes rellenar el campo curso correctamente. No debe ser un campo vacio";
$langerrorcurso3 ="Se ha producido un error al insertar el curso";
$langerrorcurso4 ="Debes un elegir un curso para poder borrarlo.";
$langerrorcurso5 ="Se ha producido un error al borrar el curso";
$langerrorcurso6 ="El curso que desea borrar esta siendo usado por una asignatura";
$langerrorcurso7 ="Error al borrar el curso";
$langerrorasignatura1 ="Debes rellenar el campo asignatura correctamente.";
$langerrorasignatura2 ="Se ha producido un error al actualizar la asignatura";
$langerrorasignatura3 ="La asignatura que desea actualizar debe estar asignada a un curso";
$langerrorasignatura4 ="Error al actualizar la asignatura";
$langerrorasignatura5 ="Se ha producido un error al actualizar la asignatura";
$langerrorasignatura6 ="Se ha producido un error al borrar la asignatura";
$langerrorasignatura7 ="La asignatura que deseea borrar tiene asignado algun examen, para borrar la asignatura debe borrar los exámenes asignados a esta asignatura";
$langerrorasignatura8 ="Error al borrar la asignatura";
$langerrorasignatura9 ="Debes rellenar todos los campos correctamente. Debes crear curso antes de asignarle una asignatura";
$langerrorasignatura10 ="Se ha producido un error al insertar la asignatura";
$langerrorasignatura11 ="La asignatura que desea insertar debe estar asignada a un curso";
$langerrorasignatura12 ="Error al actualizar la asignatura";
$langerrorasignatura13 ="Debes rellenar todos los campos correctamente.";
$langerrorasignatura14 ="Se ha producido un error al insertar la asignatura";
$langerrorexamen1 ="Se ha producido un error";
$langerrorexamen2 ="Se ha producido un error visualizar el examen en PDF";
$langerrorexamen3 ="Debes rellenar el campo examen correctamente.";
$langerrorexamen4 ="Se ha producido un error al insertar el examen";
$langerrorexamen5 ="Debes rellenar todos los campos correctamente, debe existir una asignatura para poder asignarle un examen";
$langerrorexamen6 ="Se ha producido un error, debe existir una asignatura para poder asignarle un examen";
$langerrorexamen7 ="Debes rellenar todos los campos correctamente. El examen no debe ser un campo vacio";
$langerrorexamen8 ="Se ha producido un error al insertar el examen";
$langerrorexamen9 ="Se ha producido un error al borrar el examen";
$langerrorexamen10 ="Debes un elegir un examen para poder borrarlo.";
$langerrorexamen11 ="El examen que deseea borrar tiene preguntas asociadas. Para borrar un examen no debe tener preguntas asociadas";
$langerrorpregunta1 ="Debes rellenar todos los campos correctamente. Debes crear un examen antes de asignarle una pregunta";
$langerrorpregunta2 ="Se ha producido un error al insertar la pregunta, debes crear un examen antes de asignarle una pregunta";
$langerrorpregunta3 ="Debes rellenar todos los campos correctamente. La pregunta no debe ser un campo vacio";
$langerrorpregunta4 ="Se ha producido un error al insertar la pregunta";
$langerrorpregunta5 ="Se ha producido un error al modificar la pregunta";
$langerrorpregunta6 ="Debes rellenar el campo pregunta correctamente.";
$langerrorpregunta7 ="Debes un elegir una pregunta para poder borrarla.";
$langerrorpregunta8 ="Se ha producido un error al borrar la pregunta";
$langerrorpdf ="Error al insertar el examen en BD:";
?>
