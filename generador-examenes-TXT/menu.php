<?php
/************************************************************************
* Software: ELSAM                                                       *
* Version:  0.92                                                         *
* Date:     2006-08-06                                                  *
* Author:   Miguel Morillo Iruela                                       *
* License:  GPL                                                         *
* This program is distributed under the terms and conditions of the GPL *
* See the LICENSE files for details                                     *
************************************************************************/

require_once "CONFIG/configuracion.php";

require "CHECK/check_login.php";

require 'CHECK/chequealogin.php';

// login OK

?>



<center>
<table style="text-align: center; width: 100%;"  border="0" cellpadding="1" cellspacing="1">
  <tbody>
    <tr>
      <td style='border: solid' bgcolor='A0B8C8'>
      <A href="<?php echo "$pathweb"?>/inicio.php?menu=inicio" onmouseover="Tip('<?php echo "$langinicio"?>', PADDING, 5, OPACITY , 85, BGCOLOR, '#F73B3E')"><IMG SRC="<?php echo "$pathweb"?>/imagenes/inicio.png" NAME="Inicio" ALIGN=MIDDLE BORDER=0 ALT="Inicio"><BR><FONT face="sans-serif, Arial, Helvetica, Geneva" size=2><b><?php echo "$langmenuinicio"?></b></FONT></A><BR>
      <A href="<?php echo "$pathweb"?>/logout.php" onmouseover="Tip('<?php echo "$langlogout"?>', PADDING, 5, OPACITY , 85, BGCOLOR, '#EDEDED')"><IMG SRC="<?php echo "$pathweb"?>/imagenes/logout.png" NAME="Logout" ALIGN=MIDDLE BORDER=0 ALT="Salir"><BR><FONT face="sans-serif, Arial, Helvetica, Geneva" size=2><b><?php echo "$langmenulogout"?></b></FONT></A><BR>
      <A href="<?php echo "$pathweb"?>/inicio.php?menu=preferencias&amp;enlace=menu" onmouseover="Tip('<?php echo "$langpreferencias"?>', PADDING, 5, OPACITY , 90, BGCOLOR, '#99BFF7')"><IMG SRC="<?php echo "$pathweb"?>/imagenes/panel-control.png" NAME="Panel de control" ALIGN=MIDDLE BORDER=0 ALT="Preferencias"><BR><FONT face="sans-serif, Arial, Helvetica, Geneva" size=2><b><?php echo "$langmenupreferencias"?></b></FONT></A><BR>
      <A href="<?php echo "$pathweb"?>/inicio.php?menu=calendario&amp;enlace=ver" onmouseover="Tip('<?php echo "$langcalendario"?>', PADDING, 5, OPACITY , 90, BGCOLOR, '#b58b8b')"><IMG SRC="<?php echo "$pathweb"?>/imagenes/calendario.png" NAME="Calendario" ALIGN=MIDDLE BORDER=0 ALT="Calendario"><BR><FONT face="sans-serif, Arial, Helvetica, Geneva" size=2><b><?php echo "$langmenucalendario"?></b></FONT></A><HR><BR>
      <A href="<?php echo "$pathweb"?>/inicio.php?menu=cursos&amp;enlace=menu" onmouseover="Tip('<?php echo "$langcursos"?>', PADDING, 5, OPACITY , 85, BGCOLOR, '#FEE751')"><IMG SRC="<?php echo "$pathweb"?>/imagenes/cursos.png" NAME="Curso" ALIGN=MIDDLE BORDER=0 ALT="Cursos"><BR><FONT face="sans-serif, Arial, Helvetica, Geneva" size=2><b><?php echo "$langmenucursos"?></b></FONT></a><BR><BR>
      <A href="<?php echo "$pathweb"?>/inicio.php?menu=asignaturas&amp;enlace=menu" onmouseover="Tip('<?php echo "$langasignaturas"?>', PADDING, 5, OPACITY , 85, BGCOLOR, '#F07BF4')"><IMG SRC="<?php echo "$pathweb"?>/imagenes/asignaturas.png" NAME="Asignaturas" ALIGN=MIDDLE BORDER=0 ALT="Asignaturas"><BR><FONT face="sans-serif, Arial, Helvetica, Geneva" size=2><b><?php echo "$langmenuasignaturas"?></b></FONT></A><BR><BR>
      <A href="<?php echo "$pathweb"?>/inicio.php?menu=examenes&amp;enlace=menu" onmouseover="Tip('<?php echo "$langexamenes"?>', PADDING, 5, OPACITY , 85, BGCOLOR, '#F4686B')"><IMG SRC="<?php echo "$pathweb"?>/imagenes/examenes.png" NAME="Examenes" ALIGN=MIDDLE BORDER=0 ALT="Examenes"><BR><FONT face="sans-serif, Arial, Helvetica, Geneva" size=2><b><?php echo "$langmenuexamenes"?></b></FONT></A><BR><BR>
      <A href="<?php echo "$pathweb"?>/inicio.php?menu=preguntas&amp;enlace=menu" onmouseover="Tip('<?php echo "$langpreguntas"?>', PADDING, 5, OPACITY , 85, BGCOLOR, '#FF8A15')"><IMG SRC="<?php echo "$pathweb"?>/imagenes/preguntas.png" NAME="Preguntas" ALIGN=MIDDLE BORDER=0 ALT="Preguntas"><BR><FONT face="sans-serif, Arial, Helvetica, Geneva" size=2><b><?php echo "$langmenupreguntas"?></b></FONT></A><BR><BR>
	  </td>
    </tr>
  </tbody>
</table>
</center>
