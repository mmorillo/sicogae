<?php
/************************************************************************
* Software: ELSAM                                                       *
* Version:  0.92                                                         *
* Date:     2006-08-06                                                  *
* Author:   Miguel Morillo Iruela                                       *
* License:  GPL                                                         *
* This program is distributed under the terms and conditions of the GPL *
* See the LICENSE files for details                                     *
************************************************************************/

require "CONFIG/configuracion.php";

?>

<table style="text-align: center; width: 100%;"  border="0" cellpadding="1" cellspacing="1">
  <tbody>
    <tr>
      <td style='border: solid' bgcolor='#C0B8C8'><A href="<?php echo "$pathweb"?>/index.php" onmouseover="Tip('<?php echo "$langacceso"?>', PADDING, 5, OPACITY , 80)"><IMG SRC="<?php echo "$pathweb"?>/imagenes/inicio.png" NAME="Login" ALIGN=MIDDLE BORDER=0 ALT="Login"><BR><FONT face="sans-serif, Arial, Helvetica, Geneva" size=2><b><?php echo "$langmenuacceso"?></b></FONT></A><BR><BR>
      <A href="http://forjamari.linex.org/projects/sicogae/" onmouseover="Tip('<?php echo "$langdescargas"?>', PADDING, 5, OPACITY , 80, BGCOLOR, '#FFDE4A')"><IMG SRC="<?php echo "$pathweb"?>/imagenes/tar.png" NAME="tar" ALIGN=MIDDLE BORDER=0 ALT="Descarga"><BR><FONT face="sans-serif, Arial, Helvetica, Geneva" size=2><b><?php echo "$langmenudescargas"?></b></FONT></A><BR><BR>
      <A href="<?php echo "$pathweb"?>/inicioout.php?menu=img&amp;enlace=ver" onmouseover="Tip('<?php echo "$langpantallas"?>', PADDING, 5, OPACITY , 80, BGCOLOR, '#61CC23')"><IMG SRC="<?php echo "$pathweb"?>/imagenes/cambio-logo.png" NAME="tar" ALIGN=MIDDLE BORDER=0 ALT="Pantallas"><BR><FONT face="sans-serif, Arial, Helvetica, Geneva" size=2><b><?php echo "$langmenupantallas"?></b></FONT></A><BR><BR>
      <A href="<?php echo "$pathweb"?>/inicioout.php?menu=doc&amp;enlace=ver" onmouseover="Tip('<?php echo "$langdoc"?>', PADDING, 5, OPACITY , 80, BGCOLOR, '#92A3C3')"><IMG SRC="<?php echo "$pathweb"?>/imagenes/documentacion.png" NAME="Documentacion" ALIGN=MIDDLE BORDER=0 ALT="Documentacion"><BR><FONT face="sans-serif, Arial, Helvetica, Geneva" size=2><b><?php echo "$langmenudoc"?></b></FONT></A><BR><BR><HR><BR><BR>
      <A href="mailto:elsam.sicogae@gmail.com?subject='SICOGAE'" onmouseover="Tip('<?php echo "$langemail"?>', PADDING, 5, OPACITY , 80, BGCOLOR, '#EDEDED')"><IMG SRC="<?php echo "$pathweb"?>/imagenes/correo.png" NAME="correo electronico" ALIGN=MIDDLE BORDER=0 ALT="Correo"><BR><FONT face="sans-serif, Arial, Helvetica, Geneva" size=2><b><?php echo "$langmenuemail"?></b></FONT></a><BR><BR>
     </td>
    </tr>
  </tbody>
</table>


<br><br>

<!--
<center>
<p>
    <a href="http://validator.w3.org/check?uri=referer"><img
        src="http://www.w3.org/Icons/valid-html401" STYLE="border: none;"
        alt="Valid HTML 4.01 Transitional" height="31" width="88"></a>
  </p>
</center>
-->

