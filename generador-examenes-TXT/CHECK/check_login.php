<?php
/************************************************************************
* Software: ELSAM                                                       *
* Version:  0.92                                                         *
* Date:     2006-08-06                                                  *
* Author:   Miguel Morillo Iruela                                       *
* License:  GPL                                                         *
* This program is distributed under the terms and conditions of the GPL *
* See the LICENSE files for details                                     *
************************************************************************/

require 'SQL/db_connect.php';

    /* script de chequeo, incluido en el fichero db_connect.php. */
	
	/* comenzamos una sesion */
    session_start();       
	
	/* Chequeamos si el usuario y password existen en BD, si no existen ponemos la variable logged_in a 0 y el devuelta */	
    if (!isset($_SESSION['username']) || !isset($_SESSION['password']))
    {
        $logged_in = 0;
        return;
    }
    else
    {
        if(!get_magic_quotes_gpc()) // This gets the current active config of magic quotes. GPC means GET/POST/Cookie.
        {
            $_SESSION['username'] = addslashes($_SESSION['username']);    //addslashes a la session del usuario antes de la query
        }
        $pass = "SELECT password FROM USUARIOS WHERE username = '".$_SESSION['username']."'";
        $result = mysql_query($pass);
        $num_row = mysql_fetch_row($result);
        If (!($num_row))
        {
            $logged_in = 0;
            unset($_SESSION['username']);
            unset($_SESSION['password']);
        /* Matamos sesiones incorrectas */  
        }
        $db_pass = mysql_fetch_array($result);
        /* obtenemos la password encriptada */
        $db_pass['password'] = stripslashes($num_row[0]);
        $_SESSION['password'] = stripslashes($_SESSION['password']);
        
        if($_SESSION['password'] == $db_pass['password'])
        {
        /* usuario y password correcto */ 
            $logged_in = 1;
			
			//COMPROBAMOS SI EL USUARIO ES ADMINISTRADOR
                $adm = "SELECT admin FROM USUARIOS WHERE username = '".$_SESSION['username']."'";
                $resultadm = mysql_query($adm);
                $array_db_adm= mysql_fetch_array($resultadm);
                $db_adm = ($array_db_adm["admin"]);
                $admin = $db_adm;
        }
        else
        {
            $logged_in = 0;

            unset($_SESSION['username']);
            unset($_SESSION['password']);
			/* Matamos sesiones incorrectas */            
        }
    }
    
	/* Limpiamos */
    unset($db_pass['password']);
    $_SESSION['username'] = stripslashes($_SESSION['username']);
?>
