<?php
/************************************************************************
* Software: ELSAM                                                       *
* Version:  0.92                                                         *
* Date:     2006-08-06                                                  *
* Author:   Miguel Morillo Iruela                                       *
* License:  GPL                                                         *
* This program is distributed under the terms and conditions of the GPL *
* See the LICENSE files for details                                     *
************************************************************************/

    /* Conexion a BD */
    require 'SQL/db_connect.php';
    require 'CHECK/check_login.php';
	
    if($logged_in == 1)
    {
		echo("<IMG SRC='imagenes/peligro.png' NAME='Peligro' ALIGN=MIDDLE BORDER=0><br><br>");
       	die('Estas ya logeado en el sistema,<b> '.$_SESSION['username'].'</b>. Vuelve a <a href = "inicio.php">ELSAM</a>.');
    }
	
	/* Chequeo si el formulario ha sido enviado */
    if (isset($_POST['submit']))
    { 
        /* Chequeamos si se ha enviado el usario y password */
        if(!$_POST['uname'] | !$_POST['passwd'])
        {
			echo("<IMG SRC='imagenes/peligro.png' NAME='Peligro' ALIGN=MIDDLE BORDER=0><br><br>");
            die('Tiene que rellenar todos los campos');
        }
		
        /* autenticacion */
        if (!get_magic_quotes_gpc())
        {
            $_POST['uname'] = addslashes($_POST['uname']);
        }
		
   		/* Verificamos si existe el usuario y password en BD */
		//$check = "SELECT username, password FROM USUARIOS WHERE username = '".$_POST['uname']."' AND password = '".md5($_POST['passwd'])."'";
		$check = "SELECT username, password FROM USUARIOS WHERE username = '".$_POST['uname']."'";
        $result = mysql_query($check);
        $num_rows = mysql_num_rows($result);
		
		/* El usuario no existe en BD */
        if (!($num_rows))
        {
			echo("<IMG SRC='imagenes/peligro.png' NAME='Peligro' ALIGN=MIDDLE BORDER=0><br><br>");
        	die('<p>Lo sentimos no esta autorizado, debe autenticarse para acceder, usuario no existe <p> <a href="index.php">Login</a>');
        }

     	/* Recogemos el usuario y password de la BD */
		$query = mysql_query("SELECT username, password FROM USUARIOS WHERE username = '".$_POST['uname']."' AND password = '".md5($_POST['passwd'])."'");

        //definimos un array
        $array = mysql_fetch_Array($query);

        //realizamos un array de los campos que contienen el usuario y la contraseña
        $arrayusuario = ($array["username"]);
        $arraypassword = ($array["password"]);
        $usuario2 = $_POST["uname"];
        $pwd2 = md5($_POST["passwd"]);
				
        /* Verificacion del usuario y password */
		if ($usuario2!="$arrayusuario" && $pwd2!="$arraypassword"){
			echo("<IMG SRC='imagenes/peligro.png' NAME='Peligro' ALIGN=MIDDLE BORDER=0><br><br>");
            die('Password incorrecta --> <a href="index.php">Login</a>');
        }
		
        /* Si existe el usuario y password registramos la sesion y actualizamos la fecha del ultimo login */
        $date = date('d - m - Y');
        $update_login = mysql_query("UPDATE USUARIOS SET last_login = '$date' WHERE username = '".$_POST['uname']."'");
		
        $_POST['uname'] = stripslashes($_POST['uname']);
        $_SESSION['username'] = $_POST['uname'];
        $_SESSION['password'] = md5($_POST['passwd']);
		
/* Se inserta la cookie despues del login */
if (isset($_POST['remember_me'])) {
  $time_expire = time()+31536000;
  setcookie("uname", $_POST['uname'], $time_expire);
  setcookie("passwd", md5($_POST['passwd']), $time_expire);
}

?>
<?php

/* Chequeamos si somos administradores para enviarnos a algun otro sitio*/

if($admin==1){
header ("Location: inicio.php?menu=inicio");
}
else{
header ("Location: inicio.php?menu=inicio");
}

?>
<?php
    }
	
	/* Si el formulario no ha sido enviado, sacamos el box de inicio */
    else
    { 


include("head.php");

echo "<table border='0' cellpadding='6' cellspacing='6' style='border-collapse: separate' width='100%'>";
echo "<tr>";
echo "<td width='16%' valign='top' style='border: 0px'>";

include("menuinicio.php");

echo "</td>";
echo "<td width='82%' valign='top' style='border: hidden'>";
//echo "<td width='82%' valign='top' style='border: solid'>";

echo "<br><br>";
echo "<center><FONT size=5 color='#990000'>$langaccesousuarios</FONT></center>";
echo "<br><br>";
?>

<center>

<form action="<?php echo $_SERVER['PHP_SELF']?>" method="post" >



<table  border="0" cellpadding="0" cellspacing="0">
    <tr>
            <td width="12"><img src="imagenes/top_left.gif" width="12" height="13" border="0" alt=""></td>
            <td style="background-image: url('imagenes/top_top.gif'); "><img src="imagenes/spacer.gif" border="0" width="1" height="1" alt=""></td>
            <td width="12"><img src="imagenes/top_right.gif" width="12" height="13" border="0" alt=""></td>
    </tr>
    <tr>
            <td width="12" style="background-image: url('imagenes/left.gif'); "><img src="imagenes/spacer.gif" border="0" width="1" height="1" alt=""></td>
            <td style="color:#666666;" >

<table cellpadding="0" cellspacing="0" border="0">
<tr>
<td><img src="imagenes/spacer.gif" border="0" width="20" height="20" alt=""></td>
<td><img src="imagenes/spacer.gif" border="0" width="1" height="1" alt=""></td>
<td><img src="imagenes/spacer.gif" border="0" width="60" height="1" alt=""></td>
</tr>

  		<tr>
    			<td colspan="2" align="center"><b><font color="#0066ff"><img src="imagenes/spacer.gif" border="0" width="50" height="1" alt=""><?php echo "$langlogin1"?></font></b></td>
  		</tr>
  		<tr>
    			<td><img src="imagenes/spacer.gif" border="0" width="1" height="30" alt=""></td>
  		</tr>
  		<tr>
    			<td align="right"><img src="imagenes/spacer.gif" border="0" width="50" height="1" alt=""><?php echo "$langusuario1"?></td>
    			<td ><input type="text" size="16" maxlength="40" name="uname" value=""></td>
		</tr>
		<tr>
    			<td align="right"><img src="imagenes/spacer.gif" border="0" width="50" height="1" alt=""><?php echo "$langpass1"?></td>
    			<td><input type="password" size="16" maxlength="40" name="passwd" value=""></td>
		</tr>
  		<tr>
    			<td><img src="imagenes/spacer.gif" border="0" width="1" height="20" alt=""></td>
  		</tr>
  		<tr>
    			<td colspan="2" align="center"><img src="imagenes/spacer.gif" border="0" width="65" height="1" alt=""><input type="hidden" name="submit" value="Login"><input onmouseover='this.src="imagenes/login_off.png"' onmouseout='this.src="imagenes/login.png"' type="image" src="imagenes/login.png" name="submit" value="Login" ></td>

  		</tr>


<tr>
<td><img src="imagenes/spacer.gif" border="0" width="20" height="20" alt=""></td>
<td><img src="imagenes/spacer.gif" border="0" width="1" height="1" alt=""></td>
<td><img src="imagenes/spacer.gif" border="0" width="60" height="1" alt=""></td>
</tr>

</table>

</td>
            <td width="12" style="background-image: url('imagenes/right.gif'); "><img src="imagenes/spacer.gif" border="0" width="1" height="1" alt=""></td>
    </tr>
    <tr>
            <td width="12"><img src="imagenes/bottom_left.gif" width="12" height="12" border="0" alt=""></td>
            <td style="background-image: url('imagenes/down.gif'); "><img src="imagenes/spacer.gif" border="0" width="1" height="1" alt=""></td>
            <td width="12"><img src="imagenes/bottom_right.gif" width="12" height="12" border="0" alt=""></td>
    </tr>
    </table>


</form>

</center>

<br>


<br>

<?php
}

/* Pie */
require_once "pie.php";
?>
