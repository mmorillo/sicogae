<?php
/************************************************************************
* Software: ELSAM                                                       *
* Version:  0.92                                                         *
* Date:     2006-08-06                                                  *
* Author:   Miguel Morillo Iruela                                       *
* License:  GPL                                                         *
* This program is distributed under the terms and conditions of the GPL *
* See the LICENSE files for details                                     *
************************************************************************/

// login OK

include_once ( '../CONFIG/configuracion.php' );


$db = mysql_connect("$BaseDatosServidor", "$BaseDatosUsuario","$BaseDatosClave") or die("Imposible conectar a la base de datos");

mysql_select_db("$BaseDatosNombre", $db) or die("Imposible consultar la base de datos");



//Creacion arrays para los datos
$data = array();
$labels= array();


$year=date("Y");


// Realizamos la consulta
$examenes = mysql_query("select count(E.NOMBRE) from EXAMEN E",$db) or die("Sentencia SQL incorrecta");

// Itera sobre el resultado de la consulta para obtener los campos que necesitamos para generar la gr�fica
while( $row = mysql_fetch_array($examenes) ){
   $numtotal[] =$row[0];  
}

// Realizamos la consulta
$examenesenero = mysql_query("select count(E.NOMBRE) from EXAMEN E where E.FECHA like ('%/01/$year%')",$db) or die("Sentencia SQL incorrecta");
$examenesfebrero = mysql_query("select count(E.NOMBRE) from EXAMEN E where E.FECHA like ('%/02/$year%')",$db) or die("Sentencia SQL incorrecta");
$examenesmarzo = mysql_query("select count(E.NOMBRE) from EXAMEN E where E.FECHA like ('%/03/$year%')",$db) or die("Sentencia SQL incorrecta");
$examenesabril = mysql_query("select count(E.NOMBRE) from EXAMEN E where E.FECHA like ('%/04/$year%')",$db) or die("Sentencia SQL incorrecta");
$examenesmayo = mysql_query("select count(E.NOMBRE) from EXAMEN E where E.FECHA like ('%/05/$year%')",$db) or die("Sentencia SQL incorrecta");
$examenesjunio = mysql_query("select count(E.NOMBRE) from EXAMEN E where E.FECHA like ('%/06/$year%')",$db) or die("Sentencia SQL incorrecta");
$examenesjulio = mysql_query("select count(E.NOMBRE) from EXAMEN E where E.FECHA like ('%/07/$year%')",$db) or die("Sentencia SQL incorrecta");
$examenesagosto = mysql_query("select count(E.NOMBRE) from EXAMEN E where E.FECHA like ('%/08/$year%')",$db) or die("Sentencia SQL incorrecta");
$examenesseptiembre = mysql_query("select count(E.NOMBRE) from EXAMEN E where E.FECHA like ('%/09/$year%')",$db) or die("Sentencia SQL incorrecta");
$examenesoctubre = mysql_query("select count(E.NOMBRE) from EXAMEN E where E.FECHA like ('%/10/$year%')",$db) or die("Sentencia SQL incorrecta");
$examenesnoviembre = mysql_query("select count(E.NOMBRE) from EXAMEN E where E.FECHA like ('%/11/$year%')",$db) or die("Sentencia SQL incorrecta");
$examenesdiciembre = mysql_query("select count(E.NOMBRE) from EXAMEN E where E.FECHA like ('%/12/$year%')",$db) or die("Sentencia SQL incorrecta");

// Itera sobre el resultado de la consulta para obtener los campos que necesitamos para generar la gr�fica
while( $row = mysql_fetch_array($examenesenero) ){
   $enero[] =$row[0]; // Valores de cada fila que se insertan en el array de datos que le pasaremos a Open Flash Chart
}

while( $row = mysql_fetch_array($examenesfebrero) ){
   $febrero[] =$row[0]; // Valores de cada fila que se insertan en el array de datos que le pasaremos a Open Flash Chart
}

while( $row = mysql_fetch_array($examenesmarzo) ){
   $marzo[] =$row[0]; // Valores de cada fila que se insertan en el array de datos que le pasaremos a Open Flash Chart
}

while( $row = mysql_fetch_array($examenesabril) ){
   $abril[] =$row[0]; // Valores de cada fila que se insertan en el array de datos que le pasaremos a Open Flash Chart
}

while( $row = mysql_fetch_array($examenesmayo) ){
   $mayo[] =$row[0]; // Valores de cada fila que se insertan en el array de datos que le pasaremos a Open Flash Chart
}

while( $row = mysql_fetch_array($examenesjunio) ){
   $junio[] =$row[0]; // Valores de cada fila que se insertan en el array de datos que le pasaremos a Open Flash Chart
}

while( $row = mysql_fetch_array($examenesjulio) ){
   $julio[] =$row[0]; // Valores de cada fila que se insertan en el array de datos que le pasaremos a Open Flash Chart
}
while( $row = mysql_fetch_array($examenesagosto) ){
   $agosto[] =$row[0]; // Valores de cada fila que se insertan en el array de datos que le pasaremos a Open Flash Chart
}

while( $row = mysql_fetch_array($examenesseptiembre) ){
   $septiembre[] =$row[0]; // Valores de cada fila que se insertan en el array de datos que le pasaremos a Open Flash Chart
}

while( $row = mysql_fetch_array($examenesoctubre) ){
   $octubre[] =$row[0]; // Valores de cada fila que se insertan en el array de datos que le pasaremos a Open Flash Chart
}

while( $row = mysql_fetch_array($examenesnoviembre) ){
   $noviembre[] =$row[0]; // Valores de cada fila que se insertan en el array de datos que le pasaremos a Open Flash Chart
}

while( $row = mysql_fetch_array($examenesdiciembre) ){
   $diciembre[] =$row[0]; // Valores de cada fila que se insertan en el array de datos que le pasaremos a Open Flash Chart
}





mysql_close($db); // Cerramos la conexi�n con la base de datos

// Recogemos datos
include_once( 'open-flash-chart.php' );

// generate some random data
srand((double)microtime()*1000000);

$bar = new bar_sketch( 55, 6, '#d070ac', '#000000' );


$bar->key( "$langflashnumeroexamenes $year ", 10 );

//for( $i=0; $i<12; $i++ ) $bar->data[] = rand(2,9);

$bar->data[0] = $enero[0];
$bar->data[1] = $febrero[0];
$bar->data[2] = $marzo[0];
$bar->data[3] = $abril[0];
$bar->data[4] = $mayo[0];
$bar->data[5] = $junio[0];
$bar->data[6] = $julio[0];
$bar->data[7] = $agosto[0];
$bar->data[8] = $septiembre[0];
$bar->data[9] = $octubre[0];
$bar->data[10] = $noviembre[0];
$bar->data[11] = $diciembre[0];



$g = new graph();
$g->title( "$langflashcreacionexamenes $year", '{font-size:16px; color: #ffffff; margin:10px; background-color: #d070ac; padding: 5px 15px 5px 15px;}' );
$g->bg_colour = '#FDFDFD';


//
// add the bar object to the graph
//
$g->data_sets[] = $bar;

$g->x_axis_colour( '#e0e0e0', '#e0e0e0' );
$g->set_x_tick_size( 9 );
$g->y_axis_colour( '#e0e0e0', '#e0e0e0' );

$g->set_x_labels( array( "$langflashenero","$langflashfebrero","$langflashmarzo","$langflashabril","$langflashmayo","$langflashjunio","$langflashjulio","$langflashagosto","$langflashsep","$langflashoct", "$langflashnov", "$langflashdic" ) );
$g->set_x_label_style( 11, '#303030', 2 );
$g->set_y_label_style( 11, '#303030', 2 );

//$g->set_y_max( 10 );
$numeromax = max($enero[0], $febrero[0], $marzo[0], $abril[0], $mayo[0], $junio[0], $julio[0], $agosto[0], $septiembre[0], $octubre[0], $noviembre[0], $diciembre[0]);

$g->set_y_max( $numeromax + 1 ); // Para poner el numero maximo alcanzable en el grafico



$g->y_label_steps( 5 );
echo $g->render();

?>