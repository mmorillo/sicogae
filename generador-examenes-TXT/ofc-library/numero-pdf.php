<?php 
/************************************************************************
* Software: ELSAM                                                       *
* Version:  0.92                                                         *
* Date:     2006-08-06                                                  *
* Author:   Miguel Morillo Iruela                                       *
* License:  GPL                                                         *
* This program is distributed under the terms and conditions of the GPL *
* See the LICENSE files for details                                     *
************************************************************************/

// login OK

include_once ( '../CONFIG/configuracion.php' );

$db = mysql_connect("$BaseDatosServidor", "$BaseDatosUsuario","$BaseDatosClave") or die("Imposible conectar a la base de datos");

mysql_select_db("$BaseDatosNombre", $db) or die("Imposible consultar la base de datos");

// Realizamos la primera consulta
$preguntas = mysql_query("SELECT count(TITULO) FROM PREGUNTA",$db) or die("Sentencia SQL incorrecta");
$examenes = mysql_query("SELECT count(NOMBRE) FROM EXAMEN",$db) or die("Sentencia SQL incorrecta");
$pdf = mysql_query("SELECT count(IDPDF) FROM PDF",$db) or die("Sentencia SQL incorrecta");

//Creacion arrays para los datos
$data = array();
$labels= array("$langflashpreguntas","$langflashexamenes","$langflashexamenesalmacenados");


// Itera sobre el resultado de la consulta para obtener los campos que necesitamos para generar la gr�fica
while( $row = mysql_fetch_array($preguntas) ){
   $data[] =$row[0]; // Valores de cada fila que se insertan en el array de datos que le pasaremos a Open Flash Chart
}

// Itera sobre el resultado de la consulta para obtener los campos que necesitamos para generar la gr�fica
while( $row = mysql_fetch_array($examenes) ){
   $data[] =$row[0]; // Valores de cada fila que se insertan en el array de datos que le pasaremos a Open Flash Chart
}

// Itera sobre el resultado de la consulta para obtener los campos que necesitamos para generar la gr�fica
while( $row = mysql_fetch_array($pdf) ){
   $data[] =$row[0]; // Valores de cada fila que se insertan en el array de datos que le pasaremos a Open Flash Chart
}

mysql_close($db); // Cerramos la conexi�n con la base de datos


// Creamos el objeto Gr�fica
include_once( 'open-flash-chart.php' );
$g = new graph();

// Gr�fica con 60% de transparencia
$g->pie(60,'#505050','{font-size: 12px; color: #404040;');

// Le pasamos 2 arrays, uno con los datos y otro con las etiquetas
$g->pie_values( $data, $labels );

// Se asigna los colores para los datos
$g->pie_slice_colours(array('#d01f3c','#356aa0','#C79810') );
$g->set_tool_tip( '#val#' );
$g->title( "$langflashnumeropreguntas", '{font-size:18px; color:#d01f3c}' );

$g->bg_colour = '#FDFDFD';

echo $g->render();
?>
