<?

/* Autenticacion */

require 'CHECK/check_login_pdf.php';

require 'CHECK/chequealogin.php';

require('SQL/db_connect-pdf.php');

// Include de la Libreria TCPDF
require_once('config/lang/eng.php');
require_once('tcpdf.php');

// Incluimos la Clase class.crearpdf.php
include('class.crearexamenpdf.php');


/* 
Aqui recogemos los datos de la Base de Datos para mostrarlos
*/
$idexamen = $_GET["var"]; 
$query="SELECT NOMBRE FROM EXAMEN WHERE IDEXAMEN='$idexamen'";
$res=mysql_query($query) or die('Error: '.mysql_error()."<BR>Query: $query");
$resultado = mysql_fetch_row($res);
//$examen = utf8_decode($resultado[0]);
$examen = $resultado[0];

$query="SELECT A.NOMBRE AS ASIGNATURA FROM EXAMEN E, ASIGNATURA A WHERE E.IDEXAMEN='$idexamen' AND E.IDASIGNATURA=A.IDASIGNATURA";
$res=mysql_query($query) or die('Error: '.mysql_error()."<BR>Query: $query");
$resultado = mysql_fetch_row($res);
//$asignatura = utf8_decode($resultado[0]);
$asignatura = $resultado[0];

$query="SELECT A.GRUPO AS GRUPO FROM EXAMEN E, ASIGNATURA A WHERE E.IDEXAMEN='$idexamen' AND E.IDASIGNATURA=A.IDASIGNATURA";
$res=mysql_query($query) or die('Error: '.mysql_error()."<BR>Query: $query");
$resultado = mysql_fetch_row($res);
//$grupo = utf8_decode($resultado[0]);
$grupo = $resultado[0];

$query="select FECHA from EXAMEN where IDEXAMEN='$idexamen'";
$res=mysql_query($query) or die('Error: '.mysql_error()."<BR>Query: $query");
$resultado = mysql_fetch_row($res);
//$fecha = utf8_decode($resultado[0]);
$fecha = $resultado[0];

$query="select MODELO from EXAMEN where IDEXAMEN='$idexamen'";
$res=mysql_query($query) or die('Error: '.mysql_error()."<BR>Query: $query");
$resultado = mysql_fetch_row($res);
//$modelo = utf8_decode($resultado[0]);
$modelo = $resultado[0];

$query="select EVALUACION from EXAMEN where IDEXAMEN='$idexamen'";
$res=mysql_query($query) or die('Error: '.mysql_error()."<BR>Query: $query");
$resultado = mysql_fetch_row($res);
//$evaluacion = utf8_decode($resultado[0]);
$evaluacion = $resultado[0];

$query="select COMENTARIO from EXAMEN where IDEXAMEN='$idexamen'";
$res=mysql_query($query) or die('Error: '.mysql_error()."<BR>Query: $query");
$resultado = mysql_fetch_row($res);
//$comentario = utf8_decode($resultado[0]);
$comentario = $resultado[0];

$query="SELECT C.NOMBRE AS CURSO FROM EXAMEN E, ASIGNATURA A, CURSO C WHERE E.IDEXAMEN='$idexamen' AND E.IDASIGNATURA=A.IDASIGNATURA AND A.IDCURSO=C.IDCURSO";
$res=mysql_query($query) or die('Error: '.mysql_error()."<BR>Query: $query");
$resultado = mysql_fetch_row($res);
//$curso = utf8_decode($resultado[0]);
$curso = $resultado[0];

$query="SELECT count(P.TITULO) AS PREGUNTAS FROM PREGUNTA P, EXAMEN E WHERE E.IDEXAMEN='$idexamen' AND E.IDEXAMEN=P.IDEXAMEN";
$res=mysql_query($query) or die('Error: '.mysql_error()."<BR>Query: $query");
$resultado = mysql_fetch_row($res);
//$numeropreguntas = utf8_decode($resultado[0]);
$numeropreguntas = $resultado[0];


$query="select CODIGOBARRAS from EXAMEN where IDEXAMEN='$idexamen'";
$res=mysql_query($query) or die('Error: '.mysql_error()."<BR>Query: $query");
$resultado = mysql_fetch_row($res);
//$codigobarras= utf8_decode($resultado[0]);
$codigobarras = $resultado[0];


//Guardamos el titulo de las preguntas para luego procesarlas en un array
$query="SELECT P.TITULO FROM PREGUNTA P, EXAMEN E WHERE E.IDEXAMEN='$idexamen' AND E.IDEXAMEN=P.IDEXAMEN";
$resultadotitulopreguntas=mysql_query($query) or die('Error: '.mysql_error()."<BR>Query: $query");

//Guardamos el texto de las preguntas para luego procesarlas en un array
$query="SELECT P.TEXTO FROM PREGUNTA P, EXAMEN E WHERE E.IDEXAMEN='$idexamen' AND E.IDEXAMEN=P.IDEXAMEN";
$resultadopreguntas=mysql_query($query) or die('Error: '.mysql_error()."<BR>Query: $query");

//Guardamos imagen de las preguntas para luego procesarlas en un array
$query="SELECT P.IMAGEN FROM PREGUNTA P, EXAMEN E WHERE E.IDEXAMEN='$idexamen' AND E.IDEXAMEN=P.IDEXAMEN";
$resultadoimagenes=mysql_query($query) or die('Error: '.mysql_error()."<BR>Query: $query");

/* 
Fin SQL
*/

// Usamos La Clase class.crearexamenpdf.php
$pdf = new crearpdf("respuesta.pdf",1);
$pdf->respuestaexamen($examen,$asignatura,$grupo,$fecha,$modelo,$evaluacion,$comentario,$curso,$numeropreguntas,$codigobarras,$resultadotitulopreguntas,$resultadopreguntas,$resultadoimagenes);
?>