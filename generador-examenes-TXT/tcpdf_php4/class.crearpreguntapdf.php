<?
/* Autenticacion */
/*
require 'CHECK/check_login_pdf.php';

require 'CHECK/chequealogin.php';

require('SQL/db_connect-pdf.php');
*/


class crearpdf
{
	var $pdf;	
	var $nombre_archivo;
	var $descargar = FALSE;
	var $patch = K_PATH_MAIN; // Patch Default del TCPDF
	
	// Funciones
	
	// Constructor
	function crearpdf($nombre,$descargar)
	{	
	 	// Instanciamos el Objeto
		$this->pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true);
		// Damos nombre al Archivo
		$this->nombre_archivo=$nombre;
		/*  $descargar TRUE Descargar Archivo
			True  Descargar Archivo
			False Crear Archivo
		*/
		if ($descargar)
		{
			$this->descargar=TRUE;
		}
		else
		{
			$this->descargar=FALSE;
		}
					
	}
	
	// Cambiar Ruta del Archivo
	function ruta($ruta)
	{	
	 	if ( is_dir($ruta) )
	 	{
			$this->patch=$ruta;
			return TRUE;	
		}
		else
		{
			return FALSE;
		}
	}
	
	// Crear el Archivo PDF
	function pregunta($pregunta,$titulo,$fecha,$modelo,$evaluacion,$texto,$solucion,$imagen,$puntuacion,$codigobarras)
	{

	// Para poder cargar los ficheros de idiomas y variables
	require 'CONFIG/configuracion.php';


		// $string debe ser un String
		if ( is_string ($texto) )
		{
			// Info del Documento, estos valores los podemos cambiar en config/tcpdf_config.php
			$this->pdf->SetCreator(PDF_CREATOR);
			$this->pdf->SetAuthor(PDF_AUTHOR);
				$doc_title="$langpreguntapregunta $titulo";
			$this->pdf->SetTitle($doc_title);
			$this->pdf->SetSubject($doc_subject);
			$this->pdf->SetKeywords($doc_keywords);
			//$this->pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);
			//$this->pdf->SetHeaderData( PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);
			// Seteamos Margenes
			$this->pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
			$this->pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
			$this->pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
			$this->pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
			$this->pdf->setImageScale(PDF_IMAGE_SCALE_RATIO); 
			$this->pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
			$this->pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
			$this->pdf->setLanguageArray($l);

				$this->pdf->setPrintHeader(false);

			// Inciciamos Documento 
			$this->pdf->AliasNbPages();
			$this->pdf->AddPage();


	/* Comienzo de la recuperacion de los datos pasados para formar el archivo PDF */


				//Logo
				$this->pdf->Image("$path/imagenes/logo.jpg",21,17,30);
				$this->pdf->Cell(40,46,"",1);
				
				//Primera linea gris
				$this->pdf->SetX(56);
				$this->pdf->SetFillColor(223);
				$this->pdf->Cell(130,3,"",1,0,L,1);
				$this->pdf->Ln();
					
				//Pregunta
				$this->pdf->SetX(56);
				$this->pdf->SetFont('freesans','B',11);
				$this->pdf->Cell(27,10,"$langpdfpregunta",1,0,R,1);
				$this->pdf->SetFont('freesans','',10);
				//$this->pdf->Cell(103,10,"$titulo",1,0,J,0);
				$this->pdf->Cell(103,10,"$titulo",1,0,L,0);
				$this->pdf->Ln();
				
				//Fecha
				$this->pdf->SetX(56);
				$this->pdf->SetFont('freesans','B',11);
				$this->pdf->Cell(27,10,"$langpdffecha",1,0,R,1);
				$this->pdf->SetFont('freesans','',10);
				$this->pdf->Cell(103,10,"$fecha",1,0,L,0);
				$this->pdf->Ln();
				
				
				//Modelo
				$this->pdf->SetX(56);
				$this->pdf->SetFont('freesans','B',11);
				$this->pdf->Cell(27,10,"$langpdfmodelo",1,0,R,1);
				$this->pdf->SetFont('freesans','',10);
				$this->pdf->Cell(103,10,"$modelo",1,0,L,0);
				$this->pdf->Ln();
				
				//Evaluacion
				$this->pdf->SetX(56);
				$this->pdf->SetFont('freesans','B',11);
				$langpdfevaluacionutf8=utf8_decode($langpdfevaluacion);
				//$this->pdf->Cell(27,10,"$langpdfevaluacionutf8",1,0,R,1);
				$this->pdf->Cell(27,10,"$langpdfevaluacion",1,0,R,1);
				$this->pdf->SetFont('freesans','',10);
				$this->pdf->Cell(103,10,"$evaluacion",1,0,L,0);
				$this->pdf->Ln();
				
				//Segunda linea gris
				$this->pdf->SetFillColor(223);
				$this->pdf->Cell(168,3,"",1,0,L,1);
				$this->pdf->Ln();
				
				
				//Texto pregunta
				$this->pdf->Ln();
				$this->pdf->SetFont('freesans','B',11);
				//$this->pdf->Cell(170,10,"$langpdfpregunta2 $titulo ($puntuacion $langpdfpuntos):",0,0,L,0);
				$this->pdf->WriteHTML("$langpdfpregunta2 $titulo ($puntuacion $langpdfpuntos):", true, 0, true, 0);
				//$this->pdf->Ln();
				$this->pdf->SetFont('freesans','',10);
				$this->pdf->MultiCell(0,5,"",0,L);
				//$this->pdf->Cell(170,80,"$texto",1,0,L,1);
				//$this->pdf->WriteHTML($texto, true, 0);
				//$this->pdf->Ln();
				
				/*
				if ($solucion !=''){
				//Solucion pregunta
				$this->pdf->Ln(5);
				$this->pdf->SetFont('freesans','B',11);
				//$langpdfsolucionutf8=utf8_decode($langpdfsolucion);
				$this->pdf->Cell(170,10,"$langpdfsolucion",0,0,L,0);
				$this->pdf->Ln();
				$this->pdf->SetFont('freesans','',10);
				//$this->pdf->MultiCell(0,5,$solucion,0,L);
				//$this->pdf->WriteHTML($solucion, true, 0);
				$this->pdf->Ln();
				}
*/


                        // Codigo de Barra para el Pie de pagina
                        if ($codigobarras==null){
                                $this->pdf->SetBarcode(date("Y-m-d H:i:s", time()));
                        }
                        else {
                                $this->pdf->SetBarcode("$codigobarras");
                        }


			// Escribimos el archivo
			$string=$texto;
			//$this->pdf->Ln();
			$this->pdf->MultiCell(0,5,"$texto",0,L);
			//$this->pdf->WriteHTML($texto, true, 0);
			//$this->pdf->WriteHTML($solucion, true, 0);
			//$this->pdf->WriteHTML($string, true, 0);


			// � Creamos o Descargamos ?
			if ($this->descargar)
			{
				$this->pdf->Output($this->nombre_archivo);
			}
			else
			{
				$this->pdf->Output($this->nombre_archivo,$this->$patch);
			}
		
			
		}
	}
	
	
	
	
}

?>
