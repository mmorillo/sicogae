<?php
/************************************************************************
* Software: ELSAM                                                       *
* Version:  0.92                                                         *
* Date:     2006-08-06                                                  *
* Author:   Miguel Morillo Iruela                                       *
* License:  GPL                                                         *
* This program is distributed under the terms and conditions of the GPL *
* See the LICENSE files for details                                     *
************************************************************************/

require_once ("CONFIG/configuracion.php");

$db_user = $BaseDatosUsuario;
$db_pass = $BaseDatosClave;
$db_host = $BaseDatosServidor;
$db_name = $BaseDatosNombre;

mysql_connect("$db_host","$db_user","$db_pass") or die ("No es posible la conexion con MySQL");
mysql_select_db("$db_name");

?>
