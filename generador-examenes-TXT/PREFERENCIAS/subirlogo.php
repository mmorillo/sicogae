<?php
/************************************************************************
* Software: ELSAM                                                       *
* Version:  0.92                                                         *
* Date:     2006-08-06                                                  *
* Author:   Miguel Morillo Iruela                                       *
* License:  GPL                                                         *
* This program is distributed under the terms and conditions of the GPL *
* See the LICENSE files for details                                     *
************************************************************************/

/* Autenticacion */

require 'CHECK/check_login.php';

require 'CHECK/chequealoginadmin.php';

// login OK

?>

<center>
	<br><br>

    <p align="center"><?php echo "$langprefsubirlogo"?><br><br></p>
    <form enctype="multipart/form-data" method="post" action="inicio.php?menu=preferencias&amp;enlace=uploader">
    <div align="center">
    <input type="file" name="archivo" >
    <br><br><br>
    <input type="submit" class="button">
    </div>
    </form>
	
	<br><br><br>
	<FONT size=3><?php echo "$langprefsubirlogo1"?></FONT>
	<br>
	<FONT size=3><?php echo "$langprefsubirlogo2"?></FONT>
	<br>
</center>

<?php
/* Pie */
require_once "pie.php";
?>
