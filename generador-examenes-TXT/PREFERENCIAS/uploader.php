<?php
/************************************************************************
* Software: ELSAM                                                       *
* Version:  0.92                                                         *
* Date:     2006-08-06                                                  *
* Author:   Miguel Morillo Iruela                                       *
* License:  GPL                                                         *
* This program is distributed under the terms and conditions of the GPL *
* See the LICENSE files for details                                     *
************************************************************************/

/* Autenticacion */

require 'CHECK/check_login.php';

require 'CHECK/chequealoginadmin.php';

// login OK

?>

<br><br><br>

<?php

    //$pag=$_SERVER[’PHP_SELF’]; // el nombre y ruta de esta misma página, es para limpiar el codigo y no tener tanto echo por ahi ;
	//$pag="subirlogo.php";
    $max = 200000; //en bytes tamaño de imagen
    $tama = $_FILES["archivo"]["size"];
	$nombrearchivo=$_FILES["archivo"]["name"];
	
    if ($_FILES["archivo"]) {
    if (!is_dir("logo/")) { //si no existe el directorio
    mkdir("logo/", 0777); //lo creamos
    chmod("logo/", 0777); //damos permisos
    }

	
    //contamos el numero de fotos que hay y lo almacenamos en la variable $fotos
    $fotos = 0;
    $dir = opendir("logo/"); //
    while ($file = readdir($dir)) {
    if ($file != "." && $file != "..") {
    $fotos++;
	if ($fotos > 4) {
    	echo("<IMG SRC='imagenes/peligro.png' NAME='Peligro' ALIGN=MIDDLE BORDER=0><br><br>");
		die ("$langerrorlogo");
    	exit;
    	}
    }
    }

    closedir($dir);

    $separado = explode(".", $nombrearchivo); 
    $ext = strtolower($separado[count($separado)-1]); //extension
    if ($tama > $max) {
    echo("<IMG SRC='imagenes/peligro.png' NAME='Peligro' ALIGN=MIDDLE BORDER=0><br><br>");
	die ("$langerrorlogo1 $max");
    exit;
    }
	
	
    if ($ext == "jpg"){
    $fotos++; //para empezar a nombrarlas por el 1
    $nombre = "banner".$fotos."-".$nombrearchivo;
	
	//echo "<br>mueve $nombrearchivo a ../logo con nombre: $nombre<br><br>";
		
    //move_uploaded_file($nombrearchivo, "../logo/".$nombre);
	copy($HTTP_POST_FILES['archivo']['tmp_name'], "logo/$nombrearchivo");
	chmod("logo/$nombrearchivo", 0777);
	die ("$langerrorlogo2");
    exit;

    } else {
    echo("<IMG SRC='imagenes/peligro.png' NAME='Peligro' ALIGN=MIDDLE BORDER=0><br><br>");
	die ("$langerrorlogo3");
    exit;
    }
    }
	echo("<IMG SRC='imagenes/peligro.png' NAME='Peligro' ALIGN=MIDDLE BORDER=0><br><br>");
	die ("$langerrorlogo4");


	
 ?>

<?php
/* Pie */
require_once "pie.php";
?>
