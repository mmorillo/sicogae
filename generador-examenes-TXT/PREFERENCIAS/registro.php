<?php
/************************************************************************
* Software: ELSAM                                                       *
* Version:  0.92                                                         *
* Date:     2006-08-06                                                  *
* Author:   Miguel Morillo Iruela                                       *
* License:  GPL                                                         *
* This program is distributed under the terms and conditions of the GPL *
* See the LICENSE files for details                                     *
************************************************************************/

/* Autenticacion */

require 'CHECK/check_login.php';

require 'CHECK/chequealoginadmin.php';

// Login OK

?>


<?php

 if (isset($_POST['submit'])) // verificacion que el formulario ha sido enviado
 
  /* Chequeo que los campos obligatorios han sido introducidos */  
  {
  if (!$_POST['uname'] | !$_POST['passwd'] | !$_POST['passwd_again'] |!$_POST['email'])
  {
  	echo("<IMG SRC='imagenes/peligro.png' NAME='Peligro' ALIGN=MIDDLE BORDER=0><br><br>");
   	die("$langerrorcampoobligatorio");
  }

  if (!get_magic_quotes_gpc())
  {
   $_POST['uname'] = addslashes($_POST['uname']);
  }
  
  /* Chequeo que el usuario no existe */
  $name_check = "SELECT username FROM USUARIOS WHERE username = '".$_POST['uname']."'"; // check if username exists in database.

  if (!($name_check)) print mysql_error();
  $result = mysql_query($name_check);
  $num_rows = mysql_num_rows($result);

  if ($num_rows != 0)

  {
  	echo("<IMG SRC='imagenes/peligro.png' NAME='Peligro' ALIGN=MIDDLE BORDER=0><br><br>");
   	die(" $langerrorusuario <strong>".$_POST['uname']."</strong> $langerrorexiste");
  }

  /* Chequeo que las passwords coinciden */

  if ($_POST['passwd'] != $_POST['passwd_again'])
  {
  	echo("<IMG SRC='imagenes/peligro.png' NAME='Peligro' ALIGN=MIDDLE BORDER=0><br><br>");
   	die("$langerrorpasswords");
  }

  /* Chequeo formato email*/

  if (!preg_match("/.*@.*..*/", $_POST['email']) | preg_match("/(<|>)/",$_POST['email']))
  {
  	echo("<IMG SRC='imagenes/peligro.png' NAME='Peligro' ALIGN=MIDDLE BORDER=0><br><br>");
   	die("$langerroremail");
  }

  /* no  username, website, location, password */

  $_POST['uname'] = strip_tags($_POST['uname']);
  $_POST['passwd'] = strip_tags($_POST['passwd']);
  $_POST['website'] = strip_tags($_POST['website']);
  $_POST['location'] = strip_tags($_POST['location']);

  /* Chequeo si el usuario es o no admin */

  if ($_POST['admin'] != 0 & $_POST['admin'] != 1)
  {
  	echo("<IMG SRC='imagenes/peligro.png' NAME='Peligro' ALIGN=MIDDLE BORDER=0><br><br>");
   	die("$langerrorusuario1");
  }

  /* Chequeo del resto de la informacion, solo se verifica el formato */

  if ($_POST['website'] != '' & !preg_match("/^(http|ftp):\/\//",$_POST['website']))
  {
   $_POST['website'] = 'http://'.$_POST['website'];
  }

  /* Se encripta la password con md5 y se inserta en BD */

  $_POST['passwd'] = md5($_POST['passwd']);

  if (!get_magic_quotes_gpc())
  {
   $_POST[passwd] = $_POST[passwd];
   $_POST[email] = $_POST[email];
   $_POST[website] = $_POST[website];
   $_POST[location] = $_POST[location];
  }

  $regdate = date('d - m - Y');

  $insert = "INSERT INTO USUARIOS (
   username,
   password,
   regdate,
   email,
   website,
   location,
   admin,
   last_login)
   VALUES (
   '".$_POST['uname']."',
   '".$_POST['passwd']."',
   '$regdate',
   '".$_POST['email']."',
   '".$_POST['website']."',
   '".$_POST['location']."',
   '".$_POST['admin']."',
   'Never')";

  $add_member = mysql_query($insert);

  if (!($add_member)) print mysql_error();

?>


<br><br><br>

<FONT size=5><?php echo "$langprefregistro"?> </FONT>
<br>


<?php

 }
 else
 { // Si no se ha enviado el formulario se inicia el registro:

?>


<br>
<center>
<FONT size=5><?php echo "$langprefregistrousuario"?></FONT>
<hr>
<br>
<FONT size=3><?php echo "$langprefregistroobligatorio"?></FONT>
<br>
<br>
<br>
</center>

<form action="<?php echo $_SERVER['PHP_SELF']; ?>?menu=preferencias&amp;enlace=registronuevousuario" method="post" name="register"> 

<table  class="formato" align="center" border="0" cellspacing="0" cellpadding="3">
	<tr>
		<td class="formatotdizqtop"><?php echo "$langprefregusuario"?></td>
		<td class="formatotdtop"><input type="text" name="uname" maxlength="40" class="input"></td>
	</tr>
	<tr>
		<td class="formatotdizqtop"><?php echo "$langprefregpassword"?></td>
		<td class="formatotdtop"> <input type="password" name="passwd" maxlength="50" class="input"> </td>
	</tr>
	<tr>
		<td class="formatotdizqtop"><?php echo "$langprefregpassword2"?></td>
		<td class="formatotdtop"> <input type="password" name="passwd_again" maxlength="50" class="input"> </td>
	</tr> 
	<tr>
		<td class="formatotdizqtop"><?php echo "$langprefregemail"?></td>
		<td class="formatotdtop"> <input type="text" name="email" maxlength="100" class="input"> </td>
	</tr>
	<tr>
		<td class="formatotdizqtop"><?php echo "$langprefregwebsite"?></td>
		<td class="formatotdtop"> <input type="text" name="website" maxlength="150"> </td>
	</tr> 
	<tr>
		<td class="formatotdizqtop"><?php echo "$langprefregdireccion"?></td>
		<td class="formatotdtop"> <input type="text" name="location" maxlength="150"> </td>
	</tr> 
	<tr>
		<td class="formatotdizqbottom"><?php echo "$langprefregadmin"?></td>
		<td class="formatotdbottom"> <select name="admin"> <option value="0" selected="selected">No</option> <option value="1">Si</option></select> </td>
	</tr>
	<tr>
		<td colspan="2" align="center"> <br> <input type="submit" name="submit" class="button" value="<?php echo "$langprefregboton"?>"> </td>
	</tr>
</table> 

</form>


<?php

} // Fin del registro usuarios

?>

<?php
/* Pie */
require_once "pie.php";
?>
