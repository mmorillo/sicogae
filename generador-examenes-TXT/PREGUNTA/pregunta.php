<?php
/************************************************************************
* Software: ELSAM                                                       *
* Version:  0.92                                                         *
* Date:     2006-08-06                                                  *
* Author:   Miguel Morillo Iruela                                       *
* License:  GPL                                                         *
* This program is distributed under the terms and conditions of the GPL *
* See the LICENSE files for details                                     *
************************************************************************/

/* Autenticacion */

require 'CHECK/check_login.php';

require 'CHECK/chequealogin.php';

// login OK

?>

<BR><BR><BR><BR>


<table style="text-align: center; width: 100%;" border="0" cellpadding="0" cellspacing="0">
  <tbody>
    <tr>
      <td align="center" valign="middle"><a href="inicio.php?menu=preguntas&amp;enlace=verpreguntaasignatura"><IMG SRC="imagenes/consultar.png" NAME="Consultar" ALIGN=MIDDLE BORDER=0 alt=""></a></td>
      <td align="center" valign="middle"><a href="inicio.php?menu=preguntas&amp;enlace=actualizarpreguntaasignatura"><IMG SRC="imagenes/actualizar.png" NAME="Actualizar" ALIGN=MIDDLE BORDER=0 alt=""></a></td>
      <td align="center" valign="middle"><a href="inicio.php?menu=preguntas&amp;enlace=insertapregunta"><IMG SRC="imagenes/insertar.png" NAME="Insertar" ALIGN=MIDDLE BORDER=0 alt=""></a></td>
      <td align="center" valign="middle"><a href="inicio.php?menu=preguntas&amp;enlace=borrarpreguntaasignatura"><IMG SRC="imagenes/borrar.png" NAME="Borrar" ALIGN=MIDDLE BORDER=0 alt=""></a></td>
    </tr>
    <tr>
      <td align="center" valign="middle"><a href="inicio.php?menu=preguntas&amp;enlace=verpreguntaasignatura"><FONT face="sans-serif, Arial, Helvetica, Geneva" size=3><b><?php echo "$langpreguntaconsulta"?></b></FONT></a></td>
      <td align="center" valign="middle"><a href="inicio.php?menu=preguntas&amp;enlace=actualizarpreguntaasignatura"><FONT face="sans-serif, Arial, Helvetica, Geneva" size=3><b><?php echo "$langpreguntaactualiza"?></b></FONT></a></td>
      <td align="center" valign="middle"><a href="inicio.php?menu=preguntas&amp;enlace=insertapregunta"><FONT face="sans-serif, Arial, Helvetica, Geneva" size=3><b><?php echo "$langpreguntainserta"?></b></FONT></a></td>
      <td align="center" valign="middle"><a href="inicio.php?menu=preguntas&amp;enlace=borrarpreguntaasignatura"><FONT face="sans-serif, Arial, Helvetica, Geneva" size=3><b><?php echo "$langpreguntaborra"?></b></FONT></a></td>
    </tr>
  </tbody>
</table>

<?php
/* Pie */
require_once "pie.php";
?>
