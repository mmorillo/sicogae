<?php
/************************************************************************
* Software: ELSAM                                                       *
* Version:  0.92                                                         *
* Date:     2006-08-06                                                  *
* Author:   Miguel Morillo Iruela                                       *
* License:  GPL                                                         *
* This program is distributed under the terms and conditions of the GPL *
* See the LICENSE files for details                                     *
************************************************************************/

/* Autenticacion */

require 'CHECK/check_login.php';

require 'CHECK/chequealogin.php';

// login OK

?>

<BR><BR>

<center>
<FONT size=5><?php echo "$langpreguntaborrar1"?></FONT>
</center>
<HR><BR><BR>


<FORM ACTION='inicio.php?menu=preguntas&amp;enlace=borrarpregunta2' method='post'>
<?php
require ("funciones-preguntas.inc.php");

$idexamen = $_GET["var"];

$miconexion = new DB_mysql ;
$miconexion->conectar($BaseDatosNombre, $BaseDatosServidor, $BaseDatosUsuario, $BaseDatosClave);
$miconexion->consulta("SELECT P.TITULO FROM PREGUNTA P, EXAMEN E WHERE E.IDEXAMEN=P.IDEXAMEN AND E.IDEXAMEN='$idexamen' ORDER BY P.TITULO");
$miconexion->verconsultaradio();

?>
<BR><BR>
<INPUT TYPE=SUBMIT class='button' VALUE=" <?php echo "$langpreguntaborraboton"?> ">
</FORM>

<?php
/* Pie */
require_once "pie.php";
?>
