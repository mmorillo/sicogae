<?php
/************************************************************************
* Software: ELSAM                                                       *
* Version:  0.92                                                         *
* Date:     2006-08-06                                                  *
* Author:   Miguel Morillo Iruela                                       *
* License:  GPL                                                         *
* This program is distributed under the terms and conditions of the GPL *
* See the LICENSE files for details                                     *
************************************************************************/

/* Autenticacion */

require 'CHECK/check_login.php';

require 'CHECK/chequealogin.php';

// login OK

//include_once("fckeditor/fckeditor.php") ;

?>

<BR><BR>

<center>
<FONT size=5><?php echo "$langpreguntatitulo3"?></FONT>
<HR><BR><BR>
</center>

<FORM METHOD="post" ACTION="inicio.php?menu=preguntas&amp;enlace=formularioinsertapregunta">

<b><?php echo "$langpreguntaexamen2"?></b>
 
<select name="examen">
<?php
require ("funciones-preguntas.inc.php");
$miconexion = new DB_mysql ;
$miconexion->conectar($BaseDatosNombre, $BaseDatosServidor, $BaseDatosUsuario, $BaseDatosClave);
$miconexion->consulta("SELECT NOMBRE FROM EXAMEN ORDER BY NOMBRE");
$miconexion->verconsultaboxasignatura();
?>

</select> 


<br><br>

<b><?php echo "$langpreguntatitulo4"?></b> <input type='text' name='pregunta' value='' size=60><br><br>
<b><?php echo "$langpreguntatexto"?>:</b>  <BR> <textarea rows=12 cols=80 name='texto' id="textarea1"></textarea> 
<?php

?>

<br><br>
<b><?php echo "$langpreguntasolucion"?>:</b>  <BR><textarea rows=12 cols=80 name='solucion'></textarea> 
<?php

?>
<br><br>
<b><?php echo "$langpreguntacomentario"?>:</b> <input type='text' name='comentario' value='' size=50><br><br>
<b><?php echo "$langpreguntafecha"?> (dd/mm/yyyy):</b> <input type='text' name='date' value='<? echo(date("d/m/Y")); ?>' size=12><br><br>
<b><?php echo "$langpreguntapuntuacion"?>:</b> <input type='text' name='puntuacion' value='' size=10><br><br>
<b><?php echo "$langpreguntacodigobarras"?>:</b> <input type='text' name='codigobarras' value='' size=10>
<b><?php echo "$langpreguntacodigobarras2"?></b><br><br>
<BR><BR>
<center>
<input type="submit" class="button" value="<?php echo "$langpreguntainsertaboton"?>">
</center>

</FORM>

<BR>



<?php
/* Pie */
require_once "pie.php";
?>
