<?php
/************************************************************************
* Software: ELSAM                                                       *
* Version:  0.92                                                         *
* Date:     2006-08-06                                                  *
* Author:   Miguel Morillo Iruela                                       *
* License:  GPL                                                         *
* This program is distributed under the terms and conditions of the GPL *
* See the LICENSE files for details                                     *
************************************************************************/

/* Autenticacion */

require 'CHECK/check_login.php';

require 'CHECK/chequealogin.php';

// login OK

?>

<BR><BR>

<CENTER>
<FONT size=5><?php echo "$langpreguntaasignaturalista"?></FONT>
<HR><BR>
</CENTER>

<?php
require ("funciones-preguntas.inc.php");
$miconexion = new DB_mysql ;
$miconexion->conectar($BaseDatosNombre, $BaseDatosServidor, $BaseDatosUsuario, $BaseDatosClave);
$miconexion->consulta("SELECT distinct A.IDASIGNATURA FROM ASIGNATURA A, EXAMEN E WHERE E.IDASIGNATURA=A.IDASIGNATURA ORDER BY GRUPO");
$miconexion->vertablalinkexameninicio3();

?>


<?php
/* Pie */
require_once "pie.php";
?>
