<?php
/************************************************************************
* Software: ELSAM                                                       *
* Version:  0.92                                                         *
* Date:     2006-08-06                                                  *
* Author:   Miguel Morillo Iruela                                       *
* License:  GPL                                                         *
* This program is distributed under the terms and conditions of the GPL *
* See the LICENSE files for details                                     *
************************************************************************/

/* Autenticacion */

require 'CHECK/check_login.php';

require 'CHECK/chequealogin.php';

// login OK

?>

<?
require ("funciones-preguntas.inc.php");


if ($_POST[nombre]=='' )
{
	echo("<IMG SRC='imagenes/peligro.png' NAME='Peligro' ALIGN=MIDDLE BORDER=0 alt=''><br><br>");
	die("$langerrorpregunta7");
}


$miconexion = new DB_mysql ;
$miconexion->conectar($BaseDatosNombre, $BaseDatosServidor, $BaseDatosUsuario, $BaseDatosClave);
$miconexion->consulta("SELECT E.IDEXAMEN FROM EXAMEN E, PREGUNTA P WHERE P.IDEXAMEN=E.IDEXAMEN AND P.TITULO='".$_POST['nombre']."'");
if ($miconexion->Errno>0 )
{
	echo("<IMG SRC='imagenes/peligro.png' NAME='Peligro' ALIGN=MIDDLE BORDER=0 alt=''><br><br>");
	echo("$langerrorpregunta8");echo("<br><br>");
	die("Error: $miconexion->Error");
}

$idexamen=$miconexion->verconsulta0();

echo "<center>";
echo "<FONT size=5>$langpreguntaborraok ".$_POST['nombre']."</FONT>";
echo "<br><br><br>";

$miconexion = new DB_mysql ;
$miconexion->conectar($BaseDatosNombre, $BaseDatosServidor, $BaseDatosUsuario, $BaseDatosClave);
$miconexion->consulta("SELECT TITULO,SOLUCION,TEXTO,COMENTARIO,FECHA FROM PREGUNTA WHERE TITULO='".$_POST['nombre']."'");
$miconexion->verconsultatabla();

echo "</center>";
echo "<br><br><br>";

echo "<FORM METHOD='post' ACTION='inicio.php?menu=preguntas&amp;enlace=borrarpregunta3'>";
echo "<input type=hidden name='pregunta' value='".$_POST['nombre']."'>";

echo "<center>";
echo "<input type='submit' class='button' value='$langpreguntaborraboton $_POST[nombre]'>";
echo "</center>";

?>

<?php
/* Pie */
require_once "pie.php";
?>
