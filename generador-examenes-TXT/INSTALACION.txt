###########################
#       Instalación       #
###########################


- Una vez descargada la aplicación deberemos descomprimirla y copiarla en algun directorio visible desde nuestro servidor web.

$ tar -xzvf generador-examenes.tar.gz
$ mv generador-examenes/ /var/www/



- Crearemos una base de datos en mysql y cargaremos el fichero que nos creara las tablas necesarias y los datos iniciales necesarios para el primer uso de la aplicación:

# mysqladmin create nombre_bd  # Crea la base de datos
# cat examenes.sql | mysql --host=servidor --user=usuario --password=contraseña nombre_bd --default-character-set=utf8


- A continuacion deberemos dar permisos de escritura a los directorios logo e imagenes, asi como de lectura al archivo de configuracion, bastara con dar permisos 0744 si conocemos el usuario usado por el servidor web, ej: apache, sino podemos dar permisos totales pero no es recomendable:

$ cd generador-examenes/
$ chmod 0777 logo/
$ chmod 0777 imagenes/
$ chmod 0777 imagenes/logo.jpg
$ chmod 0777 upload/



- Editaremos el archivo de configuracion introduciendo los valores correspondientes a nuestra configuracion:


$BaseDatosServidor      = "localhost";
$BaseDatosNombre        = "examenes";
$BaseDatosUsuario       = "usuario";
$BaseDatosClave         = "password";

$tituloIndex="Generador examenes ELSAM - Sistema de control, gestion y almacenaje de examenes";
$tituloContacto="";
$PieDePaginaindex="";
$PieDePaginaContacto="elsam.sicogae@gmail.com";
$pathweb="http://www.tuservidorweb.com/generador-examenes";


En funcion de la versión que tengamos intalada de PHP deberemos realizar un enlace a la version correcta:
PHP5: ln -s tcpdf_php5 tcpdf

PHP4: ln -s tcpdf_php4 tcpdf

Si estamos realizando la instalacion en Windows deberemos copiar el directorio con la version adecuada con el nombre tcpdf



