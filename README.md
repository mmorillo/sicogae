# Bienvenido

Bienvenido a Sicogae! Versión 0.92

ELSAM, examenes es una aplicación web (PHP - mysql) que genera automaticamente examenes en PDF con preguntas almacenadas en Base de datos. Permite guardar y gestionar los examenes y preguntas de los cursos y asignaturas del centro. 

Algunas funcionalidades de tcpdf están obsoletas a partir de PHP 5.2.

# Pruébalo

[ELSAM](http://demoxhtml.generadordeexamenes.miguelines.es/)

# Descarga

[Descargar](https://bitbucket.org/mmorillo/sicogae/get/master.zip )

# Actualiza

Bájate el código y actualizado, esta licenciado bajo GNU GENERAL PUBLIC LICENSE Version 2

```
$ git clone https://bitbucket.org/mmorillo/sicogae.git
```

¡Suerte!
